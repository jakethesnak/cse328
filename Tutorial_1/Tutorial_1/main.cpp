#include "Dependencies\glew\glew.h"
#include "Dependencies\freeglut\freeglut.h"
#include "Dependencies\SOIL.h"
#include <iostream>
#include <algorithm>
#include <vector>
#include <cstdint>
#include <list>
#include <string>
#include <sstream>
#include <thread>
#include <mutex>
#include <random>
#include <assert.h>
#include <iostream>
#include <fstream>
#include <Windows.h>
#include <commdlg.h>

class Pixel {
public:
	GLfloat red;
	GLfloat green;
	GLfloat blue;
	GLfloat dred = -1;
	GLfloat dgreen = -1;
	GLfloat dblue = -1;
	GLfloat pred = -1;
	GLfloat pgreen = -1;
	GLfloat pblue = -1;
	GLfloat volume = 0;
	GLfloat pvolume = 0;
	GLfloat alpha = 0;
	bool line = false;
	bool drawn = false;
	bool pline = false;
	bool selected = true;
	bool isSelected = false;
	Pixel(GLfloat r, GLfloat g, GLfloat b) {
		red = r;
		green = g;
		blue = b;
	}
	void setVolume(GLfloat v) {
		volume = v;
	}
	void setLine(bool l) {
		line = l;
	}
};

class Point {
public:
	GLint x;
	GLint y;
	Point(GLint newX, GLint newY) {
		x = newX;
		y = newY;
	}
};

class Brush {
public:
	GLfloat sred = 1;
	GLfloat sgreen = 0;
	GLfloat sblue = 0;
	int volume = 0;
	int rvolume = 50;
	GLfloat rred = 1;
	GLfloat rgreen = 0;
	GLfloat rblue = 0;
	bool s = false;
	Brush() {

	}
	Brush(GLfloat r, GLfloat g, GLfloat b) {
		sred = r;
		sgreen = g;
		sblue = b;
	}
};

class Box {
public:
	Point one = Point(-1, -1);
	Point two = Point(-1, -1);
	Box(Point o, Point t) {
		one = o;
		two = t;
	}
};

void initMenu();
void loadFile();
void saveFile();
void loadData(OPENFILENAME ofn);
void saveData(OPENFILENAME ofn);
void resetLineCheck(GLint x0, GLint y0);
Pixel blendCanvas(Pixel canvas, GLint x, GLint y, int i, int j);
void displayWells();
void blendBrush(Pixel canvas, GLint x, GLint y, int i, int j);
long double opticaldepth(float r, float g, float b);
void drawPixel(GLint x, GLint y);
void drawLineMidpoint(GLint x0, GLint y0, GLint x1, GLint y1, GLint spec);
void drawLine(GLint x0, GLint y0, GLint x1, GLint y1);
void displayCanvas();
void mouseInput(GLint button, GLint action, GLint xMouse, GLint yMouse);
void mouseMotion(GLint xMouse, GLint yMouse);
void initArray();
void reshape(GLint newWidth, GLint newHeight);
void init();
void drawPixelPaletteWell(GLint x, GLint y, GLfloat r, GLfloat g, GLfloat b);
void initPalette();
void menu(int num);
void initMenu();
void drying(int n);
void polygonFil();
void polygonFlat();
void polygonEraser();
void polygonRound();
Point pointFromEndOfLine(Point start, Point end, double distance);

//GLfloat red = 0.5, green = 0.25, blue = 0, aplha = 0, , brushVolume = 255
GLfloat R = 1.5, theta = 0;
GLint currXMouse = 500, currYMouse = 250, prevXMouse = -1, prevYMouse = -1;
int counter = 0, winWidth = 1200, winHeight = 650, T = 0, backwinWidth = winWidth, 
	backwinHeight = winHeight, brushWidth = 20, barX = winWidth / 2;
unsigned char* brushTextureData, *canvasTextureData;
std::vector<std::vector<Pixel>> pixels;
static bool isCanvas = true, isDrawing = false, first = false, firstBrush = true,
boolFilLeft = false, boolfilRightDown = false, boolFilUp = true, boolfilLeftDown = false,
fil = true, flat = false, boolFlaLeft = false, boolflaRightUp = false, boolFlaUp = true,
	boolflaLeftUp = false, single = false, sketch = false, eraser = false, redoBackground = false,
	selection = false, colorSelection = false, colorPicker = false, undoSelect = false, 
	replacement = true, load = false, bmp = false, full = false, crossbar = false;
static std::mutex dryLock;
std::vector<Point> dryingList;
static int window, palette = -1, brushMenu_id = 0, mainMenu_id = 0, modeMenu_id, toolMenu_id, 
	fileMenu_id, clearMenu_id;

Brush brush;
Point select1 = Point(-1, -1), select2 = Point(-1, -1);
Pixel backgroundC = Pixel(231.0 / 255.0, 238.0 / 255.0, 69.0 / 255.0);
GLuint canvasTexture = -1, brushTexture, newBrushTexture, shader, fbo, rbo, quadVAO, 
	quadVBO, tcanvasTexture, quadricList, canvasBackgroundTexture, fbo2, rbo2;
GLint newBrushWidth = 4, newBrushHeight = 4, centerX = 3, centerY = 3;
const float DEG2RAD = 3.14159 / 180;
std::vector<std::vector<Brush>> newBrush, filLeft, filUp, filLeftDown, filRightDown, cirBrush, 
	flaLeft, flaUp, flaLeftUp, flaRightUp, eraBrush;
std::vector<Box> boxes;
double ox = 0, oy = 0, oz = 2.000001, sx = 0, sy = 0, sz = 0, tx = 0, ty = 1, tz = 0;
std::vector<Point> polygon, polygon2, polygon3;

/**
* Shader helper function to read in data from a file. 
*/
std::string readFile(const char *filePath) {
	std::string content;
	std::ifstream fileStream;
	fileStream.open(filePath, std::ios::in);
	if (!fileStream.is_open()) {
		std::cerr << "Could not read file " << filePath << ". File does not exist." << std::endl;
		return "";
	}
	std::string line = "";
	while (!fileStream.eof()) {
		std::getline(fileStream, line);
		content.append(line + "\n");
	}
	fileStream.close();
	return content;
}

/**
* Loads shaders into opengl.
*/
GLuint LoadShader(const char *vertex_path, const char *fragment_path) {
	GLuint vertShader = glCreateShader(GL_VERTEX_SHADER);
	GLuint fragShader = glCreateShader(GL_FRAGMENT_SHADER);
	// Read shaders
	std::string vertShaderStr = readFile(vertex_path);
	std::string fragShaderStr = readFile(fragment_path);
	const char *vertShaderSrc = vertShaderStr.c_str();
	const char *fragShaderSrc = fragShaderStr.c_str();

	GLint result = GL_FALSE;
	int logLength;

	// Compile vertex shader
	std::cout << "Compiling vertex shader." << std::endl;
	glShaderSource(vertShader, 1, &vertShaderSrc, NULL);
	glCompileShader(vertShader);

	// Check vertex shader
	glGetShaderiv(vertShader, GL_COMPILE_STATUS, &result);
	glGetShaderiv(vertShader, GL_INFO_LOG_LENGTH, &logLength);
	std::vector<GLchar> vertShaderError((logLength > 1) ? logLength : 1);
	glGetShaderInfoLog(vertShader, logLength, NULL, &vertShaderError[0]);
	std::cout << &vertShaderError[0] << std::endl;
	
	// Compile fragment shader
	std::cout << "Compiling fragment shader." << std::endl;
	glShaderSource(fragShader, 1, &fragShaderSrc, NULL);
	glCompileShader(fragShader);

	// Check fragment shader
	glGetShaderiv(fragShader, GL_COMPILE_STATUS, &result);
	glGetShaderiv(fragShader, GL_INFO_LOG_LENGTH, &logLength);
	std::vector<GLchar> fragShaderError((logLength > 1) ? logLength : 1);
	glGetShaderInfoLog(fragShader, logLength, NULL, &fragShaderError[0]);
	std::cout << &fragShaderError[0] << std::endl;

	std::cout << "Linking program" << std::endl;
	GLuint program = glCreateProgram();
	glAttachShader(program, vertShader);

	//Bind loactions to program
	glBindAttribLocation(program, 0, "position");
	glBindAttribLocation(program, 1, "texCoords");

	glAttachShader(program, fragShader);
	glLinkProgram(program);
	glGetProgramiv(program, GL_LINK_STATUS, &result);
	glGetProgramiv(program, GL_INFO_LOG_LENGTH, &logLength);
	std::vector<char> programError((logLength > 1) ? logLength : 1);
	glGetProgramInfoLog(program, logLength, NULL, &programError[0]);
	std::cout << &programError[0] << std::endl;
	glDeleteShader(vertShader);
	glDeleteShader(fragShader);
	return program;
}

void createCanvasTexture() {
	
	//glReadPixels(0, 0, winWidth, winHeight, GL_RGBA, GL_UNSIGNED_BYTE, canvasTextureData);
	//glGenTextures(1, &canvasTexture);
	//glBindTexture(GL_TEXTURE_2D, canvasTexture);
	//glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, winWidth, winHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, canvasTextureData);
	glGenFramebuffers(1, &fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, fbo);
	glGenTextures(1, &tcanvasTexture);
	glBindTexture(GL_TEXTURE_2D, tcanvasTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, winWidth, winHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glBindTexture(GL_TEXTURE_2D, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tcanvasTexture, 0);
	//glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_TEXTURE_2D, canvasTexture, 0);
	glGenRenderbuffers(1, &rbo);
	glBindRenderbuffer(GL_RENDERBUFFER, rbo);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, winWidth, winHeight);
	glBindRenderbuffer(GL_RENDERBUFFER, 0);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rbo);
	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		std::cout << "ERROR::FRAMEBUFFER:: Framebuffer is not complete!";
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glGenFramebuffers(1, &fbo2);
	glBindFramebuffer(GL_FRAMEBUFFER, fbo2);

	glGenTextures(1, &canvasBackgroundTexture);
	glBindTexture(GL_TEXTURE_2D, canvasBackgroundTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, winWidth, winHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glBindTexture(GL_TEXTURE_2D, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, canvasBackgroundTexture, 0);
	//glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_TEXTURE_2D, canvasTexture, 0);
	glGenRenderbuffers(1, &rbo2);
	glBindRenderbuffer(GL_RENDERBUFFER, rbo2);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, winWidth, winHeight);
	glBindRenderbuffer(GL_RENDERBUFFER, 0);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, rbo2);
	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		std::cout << "ERROR::FRAMEBUFFER:: Framebuffer is not complete!";
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	GLfloat quadVertices[] = {
		-1.0f,  1.0f,  0.0f, 1.0f,
		-1.0f, -1.0f,  0.0f, 0.0f,
		1.0f, -1.0f,  1.0f, 0.0f,
		-1.0f,  1.0f,  0.0f, 1.0f,
		1.0f, -1.0f,  1.0f, 0.0f,
		1.0f,  1.0f,  1.0f, 1.0f
	};
	glGenVertexArrays(1, &quadVAO);
	glGenBuffers(1, &quadVBO);
	glBindVertexArray(quadVAO);
	glBindBuffer(GL_ARRAY_BUFFER, quadVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), &quadVertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (GLvoid*)0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (GLvoid*)(2 * sizeof(GLfloat)));
	glBindVertexArray(0);
	//glGenerateMipmap(GL_TEXTURE_2D);
}

void subCanvasTexture() {
	int temp = currYMouse;
	//currYMouse -= winHeight;
	glBindTexture(GL_TEXTURE_2D, canvasTexture);
	if (currXMouse >= 100 && currXMouse <= winWidth - 100) {
		if (currYMouse >= 100 && currYMouse <= winHeight - 100) {
			unsigned char* canvasTextureDataSub = (unsigned char*)malloc(200 * 200 * 4);
			glReadPixels(currXMouse - 100, currYMouse - 100, 200, 200, GL_RGBA, GL_UNSIGNED_BYTE, canvasTextureDataSub);
			glTexSubImage2D(GL_TEXTURE_2D, 0, currXMouse - 100, currYMouse - 100, 200, 200, GL_RGBA, GL_UNSIGNED_BYTE, canvasTextureDataSub);
			//free(canvasTextureDataSub);
		}
		else if (currYMouse < 100) {
			unsigned char* canvasTextureDataSub = (unsigned char*)malloc(200 * 150 * 4);
			glReadPixels(currXMouse - 100, 0, 200, 150, GL_RGBA, GL_UNSIGNED_BYTE, canvasTextureDataSub);
			glTexSubImage2D(GL_TEXTURE_2D, 0, currXMouse - 100, 0, 200, 150, GL_RGBA, GL_UNSIGNED_BYTE, canvasTextureDataSub);
			//free(canvasTextureDataSub);
		}
		else {
			unsigned char* canvasTextureDataSub = (unsigned char*)malloc(200 * 150 * 4);
			glReadPixels(currXMouse - 100, winHeight - 150, 200, 150, GL_RGBA, GL_UNSIGNED_BYTE, canvasTextureDataSub);
			glTexSubImage2D(GL_TEXTURE_2D, 0, currXMouse - 100, winHeight - 150, 200, 150, GL_RGBA, GL_UNSIGNED_BYTE, canvasTextureDataSub);
			//free(canvasTextureDataSub);
		}
	} 
	else if (currXMouse < 100) {
		if (currYMouse >= 100 && currYMouse <= winHeight - 100) {
			unsigned char* canvasTextureDataSub = (unsigned char*)malloc(200 * 150 * 4);
			glReadPixels(0, currYMouse - 100, 150, 200, GL_RGBA, GL_UNSIGNED_BYTE, canvasTextureDataSub);
			glTexSubImage2D(GL_TEXTURE_2D, 0, 0, currYMouse - 100, 150, 200, GL_RGB, GL_UNSIGNED_BYTE, canvasTextureDataSub);
			//free(canvasTextureDataSub);
		}
		else if (currYMouse < 100) {
			unsigned char* canvasTextureDataSub = (unsigned char*)malloc(150 * 152 * 4);
			glReadPixels(0, 0, 150, 150, GL_RGBA, GL_UNSIGNED_BYTE, canvasTextureDataSub);
			glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, 150, 150, GL_RGBA, GL_UNSIGNED_BYTE, canvasTextureDataSub); 
			//free(canvasTextureDataSub);
		}
		else {
			unsigned char* canvasTextureDataSub = (unsigned char*)malloc(150 * 150 * 4);
			glReadPixels(0, winHeight - 150, 150, 150, GL_RGBA, GL_UNSIGNED_BYTE, canvasTextureDataSub);
			glTexSubImage2D(GL_TEXTURE_2D, 0, 0, winHeight - 150, 150, 150, GL_RGB, GL_UNSIGNED_BYTE, canvasTextureDataSub);
			//free(canvasTextureDataSub);
		}
	} 
	else {
		if (currYMouse >= 100 && currYMouse <= winHeight - 100) {
			unsigned char* canvasTextureDataSub = (unsigned char*)malloc(200 * 150 * 4);
			glReadPixels(winWidth - 150, currYMouse - 100, 150, 20, GL_RGBA, GL_UNSIGNED_BYTE, canvasTextureDataSub);
			glTexSubImage2D(GL_TEXTURE_2D, 0, winWidth - 150, currYMouse - 100, 150, 200, GL_RGBA, GL_UNSIGNED_BYTE, canvasTextureDataSub);
			//free(canvasTextureDataSub);
		}
		else if (currYMouse < 100) {
			unsigned char* canvasTextureDataSub = (unsigned char*)malloc(150 * 150 * 4);
			glReadPixels(winWidth - 150, 0, 150, 150, GL_RGBA, GL_UNSIGNED_BYTE, canvasTextureDataSub);
			glTexSubImage2D(GL_TEXTURE_2D, 0, winWidth - 150, 0, 150, 150, GL_RGBA, GL_UNSIGNED_BYTE, canvasTextureDataSub);
			//free(canvasTextureDataSub);
		}
		else {
			unsigned char* canvasTextureDataSub = (unsigned char*)malloc(150 * 150 * 4);
			glReadPixels(winWidth - 150, winHeight - 150, 150, 150, GL_RGBA, GL_UNSIGNED_BYTE, canvasTextureDataSub);
			glTexSubImage2D(GL_TEXTURE_2D, 0, winWidth - 150, winHeight - 150, 150, 150, GL_RGBA, GL_UNSIGNED_BYTE, canvasTextureDataSub);
			//free(canvasTextureDataSub);
		}
	}
	glBindTexture(GL_TEXTURE_2D, 0);
}

void computeCircleSelection(int k, int r, int h) {
	float x, y, rot;
	//glBegin(GL_TRIANGLE_FAN);
	//glColor3f(1, 0, 0);
	for (int i = 0; i < 180; i++) {
		x = r * cos(i) - h;
		y = r * sin(i) + k ;
		//circle[x + k][y - h] = true;
		//glVertex3f(x + k, y - h, 0);
		x = r * cos(i + 0.1) - h;
		y = r * sin(i + 0.1) + k;
		//circle[x + k][y - h] = true;
		//glVertex3f(x + k, y - h, 0);
	}
	//glEnd();
	double xaxis = newBrushWidth;
	double yaxis = newBrushHeight;
	double posX = newBrushWidth / 2;
	double posY = newBrushHeight / 2;
	double radius = newBrushWidth / 2;
	cirBrush.push_back(std::vector<Brush>());
	eraBrush.push_back(std::vector<Brush>());
	for (double x = 0; x <= xaxis; x++) {
		for (double y = 0; y <= yaxis; y++) {
			//using the formula for a cicrle
			double a = abs((posX - x) * (posX - x));
			double b = abs((posY - y) * (posY - y));
			double c = abs(a + b);
			double d = abs(radius * radius);
			// checking if the formula stands correct at each coordinate scanned
			double arbitraryNumber = 0.01;
			if (abs(c - d) < arbitraryNumber || c < d) {
				cirBrush[x].push_back(Brush());
				eraBrush[x].push_back(Brush(1, 1, 1));
				cirBrush[x][y].s = true;
				eraBrush[x][y].s = true;
			}
			else {
				cirBrush[x].push_back(Brush());
				eraBrush[x].push_back(Brush());
				cirBrush[x][y].s = false;
				eraBrush[x][y].s = false;
			}
		}
		eraBrush.push_back(std::vector<Brush>());
		cirBrush.push_back(std::vector<Brush>());
	}
	if (newBrushWidth != 5) {
		flaLeft.push_back(std::vector<Brush>());
		flaLeftUp.push_back(std::vector<Brush>());
		flaRightUp.push_back(std::vector<Brush>());
		flaUp.push_back(std::vector<Brush>());
		newBrushWidth = 9;
		newBrushHeight = 5;
		for (int x = 0; x < newBrushWidth; x++) {
			for (int y = 0; y < newBrushHeight; y++) {
				if (x == 0 || x == 8) {
					if (y == 2) {
						flaUp[x].push_back(Brush());
						flaUp[x][y].s = true;
					}
					else {
						flaUp[x].push_back(Brush());
						flaUp[x][y].s = false;
					}
				}
				else if (x == 1 || x == 7 || x == 2 || x == 6) {
					if (y == 1 || y == 2 || y == 3) {
						flaUp[x].push_back(Brush());
						flaUp[x][y].s = true;
					}
					else {
						flaUp[x].push_back(Brush());
						flaUp[x][y].s = false;
					}
				}
				else if (x == 3 || x == 4 || x == 5) {
					flaUp[x].push_back(Brush());
					flaUp[x][y].s = true;
				}
			}
			flaUp.push_back(std::vector<Brush>());
		}
		newBrushWidth = 7;
		newBrushHeight = 7;
		for (int x = 0; x < newBrushWidth; x++) {
			for (int y = 0; y < newBrushHeight; y++) {
				if (x == 0) {
					if (y == 0 || y == 1 || y == 2) {
						flaRightUp[x].push_back(Brush());
						flaRightUp[x][y].s = true;
					}
					else {
						flaRightUp[x].push_back(Brush());
						flaRightUp[x][y].s = false;
					}
				}
				else if (x == 1) {
					if (y == 0 || y == 1 || y == 2 || y == 3) {
						flaRightUp[x].push_back(Brush());
						flaRightUp[x][y].s = true;
					}
					else {
						flaRightUp[x].push_back(Brush());
						flaRightUp[x][y].s = false;
					}
				}
				else if (x == 2) {
					if (y == 1 || y == 2 || y == 3 || y == 4 || y == 0) {
						flaRightUp[x].push_back(Brush());
						flaRightUp[x][y].s = true;
					}
					else {
						flaRightUp[x].push_back(Brush());
						flaRightUp[x][y].s = false;
					}
				}
				else if (x == 3) {
					if (y == 1 || y == 2 || y == 3 || y == 4 || y == 5) {
						flaRightUp[x].push_back(Brush());
						flaRightUp[x][y].s = true;
					}
					else {
						flaRightUp[x].push_back(Brush());
						flaRightUp[x][y].s = false;
					}
				}
				else if (x == 4) {
					if (y == 2 || y == 3 || y == 4 || y == 5 || y == 6) {
						flaRightUp[x].push_back(Brush());
						flaRightUp[x][y].s = true;
					}
					else {
						flaRightUp[x].push_back(Brush());
						flaRightUp[x][y].s = false;
					}
				}
				else if (x == 5) {
					if (y == 3 || y == 4 || y == 5 || y == 6) {
						flaRightUp[x].push_back(Brush());
						flaRightUp[x][y].s = true;
					}
					else {
						flaRightUp[x].push_back(Brush());
						flaRightUp[x][y].s = false;
					}
				}
				else if (x == 6) {
					if (y == 4 || y == 5 || y == 6) {
						flaRightUp[x].push_back(Brush());
						flaRightUp[x][y].s = true;
					}
					else {
						flaRightUp[x].push_back(Brush());
						flaRightUp[x][y].s = false;
					}
				}
			}
			flaRightUp.push_back(std::vector<Brush>());
		}
		for (int x = 0; x < newBrushWidth; x++) {
			for (int y = 0; y < newBrushHeight; y++) {
				if (x == 0) {
					if (y == 6 || y == 5 || y == 4) {
						flaLeftUp[x].push_back(Brush());
						flaLeftUp[x][y].s = true;
					}
					else {
						flaLeftUp[x].push_back(Brush());
						flaLeftUp[x][y].s = false;
					}
				}
				else if (x == 1) {
					if (y == 6 || y == 5 || y == 4 || y == 3) {
						flaLeftUp[x].push_back(Brush());
						flaLeftUp[x][y].s = true;
					}
					else {
						flaLeftUp[x].push_back(Brush());
						flaLeftUp[x][y].s = false;
					}
				}
				else if (x == 2) {
					if (y == 2 || y == 3 || y == 4 || y == 5 || y == 6) {
						flaLeftUp[x].push_back(Brush());
						flaLeftUp[x][y].s = true;
					}
					else {
						flaLeftUp[x].push_back(Brush());
						flaLeftUp[x][y].s = false;
					}
				}
				else if (x == 3) {
					if (y == 1 || y == 2 || y == 3 || y == 4 || y == 5) {
						flaLeftUp[x].push_back(Brush());
						flaLeftUp[x][y].s = true;
					}
					else {
						flaLeftUp[x].push_back(Brush());
						flaLeftUp[x][y].s = false;
					}
				}
				else if (x == 4) {
					if (y == 2 || y == 3 || y == 4 || y == 1 || y == 0) {
						flaLeftUp[x].push_back(Brush());
						flaLeftUp[x][y].s = true;
					}
					else {
						flaLeftUp[x].push_back(Brush());
						flaLeftUp[x][y].s = false;
					}
				}
				else if (x == 5) {
					if (y == 3 || y == 2 || y == 1 || y == 0) {
						flaLeftUp[x].push_back(Brush());
						flaLeftUp[x][y].s = true;
					}
					else {
						flaLeftUp[x].push_back(Brush());
						flaLeftUp[x][y].s = false;
					}
				}
				else if (x == 6) {
					if (y == 0 || y == 1 || y == 2) {
						flaLeftUp[x].push_back(Brush());
						flaLeftUp[x][y].s = true;
					}
					else {
						flaLeftUp[x].push_back(Brush());
						flaLeftUp[x][y].s = false;
					}
				}
			}
			flaLeftUp.push_back(std::vector<Brush>());
		}
		newBrushWidth = 5;
		newBrushHeight = 9;
		for (int x = 0; x < newBrushWidth; x++) {
			for (int y = 0; y < newBrushHeight; y++) {
				if (x == 0 || x == 4) {
					if (y == 5 || y == 3 || y == 4) {
						flaLeft[x].push_back(Brush());
						flaLeft[x][y].s = true;
					}
					else {
						flaLeft[x].push_back(Brush());
						flaLeft[x][y].s = false;
					}
				}
				else if (x == 1 || x == 3) {
					if (y == 1 || y == 2 || y == 3 || y == 4 || y == 5 || y == 6 || y == 7) {
						flaLeft[x].push_back(Brush());
						flaLeft[x][y].s = true;
					}
					else {
						flaLeft[x].push_back(Brush());
						flaLeft[x][y].s = false;
					}
				}
				else if (x == 2) {
					flaLeft[x].push_back(Brush());
					flaLeft[x][y].s = true;
				}
			}
			flaLeft.push_back(std::vector<Brush>());
		}
		filLeft.push_back(std::vector<Brush>());
		filLeftDown.push_back(std::vector<Brush>());
		filRightDown.push_back(std::vector<Brush>());
		filUp.push_back(std::vector<Brush>());
		newBrushWidth = 7;
		newBrushHeight = 5;
		for (int x = 0; x < newBrushWidth; x++) {
			for (int y = 0; y < newBrushHeight; y++) {
				if (x == 0 || x == 6) {
					if (y == 2) {
						filLeft[x].push_back(Brush());
						filLeft[x][y].s = true;
					}
					else {
						filLeft[x].push_back(Brush());
						filLeft[x][y].s = false;
					}
				}
				else if (x == 1 || x == 5) {
					if (y == 1 || y == 2 || y == 3) {
						filLeft[x].push_back(Brush());
						filLeft[x][y].s = true;
					}
					else {
						filLeft[x].push_back(Brush());
						filLeft[x][y].s = false;
					}
				}
				else if (x == 2 || x == 3 || x == 4) {
					filLeft[x].push_back(Brush());
					filLeft[x][y].s = true;
				}
			}
			filLeft.push_back(std::vector<Brush>());
		}
		newBrushWidth = 6;
		newBrushHeight = 6;
		for (int x = 0; x < newBrushWidth; x++) {
			for (int y = 0; y < newBrushHeight; y++) {
				if (x == 0) {
					if (y == 4 || y == 5) {
						filRightDown[x].push_back(Brush());
						filRightDown[x][y].s = true;
					}
					else {
						filRightDown[x].push_back(Brush());
						filRightDown[x][y].s = false;
					}
				}
				else if (x == 1 || x == 2) {
					if (y == 1 || y == 2 || y == 3 || y == 4 || y == 5) {
						filRightDown[x].push_back(Brush());
						filRightDown[x][y].s = true;
					}
					else {
						filRightDown[x].push_back(Brush());
						filRightDown[x][y].s = false;
					}
				}
				else if (x == 3) {
					if (y == 1 || y == 2 || y == 3 || y == 4) {
						filRightDown[x].push_back(Brush());
						filRightDown[x][y].s = true;
					}
					else {
						filRightDown[x].push_back(Brush());
						filRightDown[x][y].s = false;
					}
				}
				else if (x == 4) {
					if (y == 1 || y == 2 || y == 3 || y == 4 || y == 0) {
						filRightDown[x].push_back(Brush());
						filRightDown[x][y].s = true;
					}
					else {
						filRightDown[x].push_back(Brush());
						filRightDown[x][y].s = false;
					}
				}
				else if (x == 5) {
					if (y == 0 || y == 1) {
						filRightDown[x].push_back(Brush());
						filRightDown[x][y].s = true;
					}
					else {
						filRightDown[x].push_back(Brush());
						filRightDown[x][y].s = false;
					}
				}
			}
			filRightDown.push_back(std::vector<Brush>());
		}
		for (int x = 0; x < newBrushWidth; x++) {
			for (int y = 0; y < newBrushHeight; y++) {
				if (x == 5) {
					if (y == 4 || y == 5) {
						filLeftDown[x].push_back(Brush());
						filLeftDown[x][y].s = true;
					}
					else {
						filLeftDown[x].push_back(Brush());
						filLeftDown[x][y].s = false;
					}
				}
				else if (x == 3 || x == 4) {
					if (y == 1 || y == 2 || y == 3 || y == 4 || y == 5) {
						filLeftDown[x].push_back(Brush());
						filLeftDown[x][y].s = true;
					}
					else {
						filLeftDown[x].push_back(Brush());
						filLeftDown[x][y].s = false;
					}
				}
				else if (x == 2) {
					if (y == 1 || y == 2 || y == 3 || y == 4) {
						filLeftDown[x].push_back(Brush());
						filLeftDown[x][y].s = true;
					}
					else {
						filLeftDown[x].push_back(Brush());
						filLeftDown[x][y].s = false;
					}
				}
				else if (x == 1) {
					if (y == 1 || y == 2 || y == 3 || y == 4 || y == 0) {
						filLeftDown[x].push_back(Brush());
						filLeftDown[x][y].s = true;
					}
					else {
						filLeftDown[x].push_back(Brush());
						filLeftDown[x][y].s = false;
					}
				}
				else if (x == 0) {
					if (y == 0 || y == 1) {
						filLeftDown[x].push_back(Brush());
						filLeftDown[x][y].s = true;
					}
					else {
						filLeftDown[x].push_back(Brush());
						filLeftDown[x][y].s = false;
					}
				}
			}
			filLeftDown.push_back(std::vector<Brush>());
		}
		newBrushWidth = 5;
		newBrushHeight = 7;
		centerX = 2;
		centerY = 3;
		for (int x = 0; x < newBrushWidth; x++) {
			for (int y = 0; y < newBrushHeight; y++) {
				if (x == 0 || x == 4) {
					if (y == 2 || y == 3 || y == 4) {
						filUp[x].push_back(Brush());
						filUp[x][y].s = true;
					}
					else {
						filUp[x].push_back(Brush());
						filUp[x][y].s = false;
					}
				}
				else if (x == 1 || x == 3) {
					if (y == 1 || y == 2 || y == 3 || y == 4 || y == 5) {
						filUp[x].push_back(Brush());
						filUp[x][y].s = true;
					}
					else {
						filUp[x].push_back(Brush());
						filUp[x][y].s = false;
					}
				}
				else if (x == 2) {
					filUp[x].push_back(Brush());
					filUp[x][y].s = true;
				}
			}
			filUp.push_back(std::vector<Brush>());
		}
		newBrush = filUp;
	}
}

void createBrushTexture() {
	//glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	computeCircleSelection(newBrushHeight + newBrushWidth, newBrushWidth, newBrushHeight);
	//brushTextureData = (unsigned char*)malloc(20 * 20 * 4);
	//glReadPixels(0, 0, 20, 20, GL_RGBA, GL_UNSIGNED_BYTE, brushTextureData);
	//glGenTextures(1, &brushTexture);
	//glBindTexture(GL_TEXTURE_2D, brushTexture);
	//glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 20, 20, 0, GL_RGBA, GL_UNSIGNED_BYTE, brushTextureData);
	//glGenerateMipmap(GL_TEXTURE_2D);
	//glBindTexture(GL_TEXTURE_2D, 0);
	//newBrushTexture = SOIL_load_OGL_texture("alphaBrush2.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, 0);
	if (0 == newBrushTexture) {
		printf("%s\n", SOIL_last_result());
	}
	//free(brushTextureData);
}

/**
* Calculates the optical thickness given a red, green, and blue.
*
* @param r
*			The red part of the pixel.
*
* @param g
*			The green part of the pixel.
*
* @param b
*			The blue part of the pixel.
*
* @return	The optical thickness.
*/
long double opticaldepth(float r, float g, float b) {
	float M = std::max(r, g);
	M = std::max(M, b);
	float m = std::min(r, g);
	m = std::min(m, b);
	float C = M - m;
	long double lamda = -1;
	if (C == 0) {
		return 1000000000;
	}
	else if (M == r) {
		lamda = fmod(((g - b) / C), 6);
	}
	else if (M == g) {
		lamda = ((b - r) / C) + 2;
	}
	else if (M == b) {
		lamda = ((r - g) / C) + 4;
	}
	if (lamda == 6) {
		lamda == 0;
	}
	std::cout.precision(30);
	//std::cout<<"first = "<< lamda << "\n";
	lamda = 4.0995422293303 * pow(lamda, 4) - 46.791974669307 * pow(lamda, 3) + 175.1741391393 * pow(lamda, 2) - 290.64049597981 * lamda + 743.12739053779;
	//std::cout << "second = " << lamda << "\n";
	long double eV = 1.6593563411319 * pow(lamda, 4) * pow(10, -11) - 5.3066879857218 * pow(lamda, 3) * pow(10, -8) + 6.532432401088 * pow(lamda, 2) * pow(10, -5) - 0.03878530941249 * lamda + 11.131353810033;
	//std::cout << "third = " << eV << "\n";
	long double joule = eV * 1.60218 * pow(10, -19);
	//std::cout << "fourth = " << joule << "\n";
	return log(joule) / (lamda);
}

/**
* Resets the line checker.
*
* @param x0
*			The initial x.
*
* @param y0
*			The initial y.
*/
void resetLineCheck(GLint x0, GLint y0) {
	if (isCanvas) {
		if (x0 >= 10 && x0 < winWidth - 10) {
			if (y0 >= 10 && y0 < winHeight - 10) {
				for (int x = x0 - 10; x < x0 + 10; x++) {
					for (int y = y0 - 10; y < y0 + 10; y++) {
						pixels[x][winHeight - y].setLine(false);
					}
				}
			}
			else if (y0 < 10) {
				for (int x = x0 - 10; x < x0 + 10; x++) {
					for (int y = 0; y < y0 + 10; y++) {
						pixels[x][winHeight - y].setLine(false);
					}
				}
			}
			else {
				for (int x = x0 - 10; x < x0 + 10; x++) {
					for (int y = winHeight - 10; y < winHeight; y++) {
						pixels[x][winHeight - y].setLine(false);
					}
				}
			}
		}
		else if (x0 < 10) {
			if (y0 >= 10 && y0 < winHeight - 10) {
				for (int x = 0; x < x0 + 10; x++) {
					for (int y = y0 - 10; y < y0 + 10; y++) {
						pixels[x][winHeight - y].setLine(false);
					}
				}
			}
			else if (y0 < 10) {
				for (int x = 0; x < x0 + 10; x++) {
					for (int y = 0; y < y0 + 10; y++) {
						pixels[x][winHeight - y].setLine(false);
					}
				}
			}
			else {
				for (int x = 0; x < x0 + 10; x++) {
					for (int y = winHeight - 10; y < winHeight; y++) {
						pixels[x][winHeight - y].setLine(false);
					}
				}
			}
		}
		else {
			if (y0 >= 10 && y0 < winHeight - 10) {
				for (int x = winWidth - 10; x < winWidth; x++) {
					for (int y = y0 - 10; y < y0 + 10; y++) {
						pixels[x][winHeight - y].setLine(false);
					}
				}
			}
			else if (y0 < 10) {
				for (int x = winWidth - 10; x < winWidth; x++) {
					for (int y = 0; y < y0 + 10; y++) {
						pixels[x][winHeight - y].setLine(false);
					}
				}
			}
			else {
				for (int x = winWidth - 10; x < winWidth; x++) {
					for (int y = winHeight - 10; y < winHeight; y++) {
						pixels[x][winHeight - y].setLine(false);
					}
				}
			}
		}
	}
	else {
		if (x0 >= 10 && x0 < winWidth - 10) {
			if (y0 >= 10 && y0 < winHeight - 10) {
				for (int x = x0 - 10; x < x0 + 10; x++) {
					for (int y = y0 - 10; y < y0 + 10; y++) {
						pixels[x][winHeight - y].pline = false;
					}
				}
			}
			else if (y0 < 10) {
				for (int x = x0 - 10; x < x0 + 10; x++) {
					for (int y = 0; y < y0 + 10; y++) {
						pixels[x][winHeight - y].pline = false;
					}
				}
			}
			else {
				for (int x = x0 - 10; x < x0 + 10; x++) {
					for (int y = winHeight - 10; y < winHeight; y++) {
						pixels[x][winHeight - y].pline = false;
					}
				}
			}
		}
		else if (x0 < 10) {
			if (y0 >= 10 && y0 < winHeight - 10) {
				for (int x = 0; x < x0 + 10; x++) {
					for (int y = y0 - 10; y < y0 + 10; y++) {
						pixels[x][winHeight - y].pline = false;
					}
				}
			}
			else if (y0 < 10) {
				for (int x = 0; x < x0 + 10; x++) {
					for (int y = 0; y < y0 + 10; y++) {
						pixels[x][winHeight - y].pline = false;
					}
				}
			}
			else {
				for (int x = 0; x < x0 + 10; x++) {
					for (int y = winHeight - 10; y < winHeight; y++) {
						pixels[x][winHeight - y].pline = false;
					}
				}
			}
		}
		else {
			if (y0 >= 10 && y0 < winHeight - 10) {
				for (int x = winWidth - 10; x < winWidth; x++) {
					for (int y = y0 - 10; y < y0 + 10; y++) {
						pixels[x][winHeight - y].pline = false;
					}
				}
			}
			else if (y0 < 10) {
				for (int x = winWidth - 10; x < winWidth; x++) {
					for (int y = 0; y < y0 + 10; y++) {
						pixels[x][winHeight - y].pline = false;
					}
				}
			}
			else {
				for (int x = winWidth - 10; x < winWidth; x++) {
					for (int y = winHeight - 10; y < winHeight; y++) {
						pixels[x][winHeight - y].pline = false;
					}
				}
			}
		}
	}
}

/**
* Blends the canvas with the brush.
*
* @param canvas
*			The pixel from the canvas to blend.
*
* @param x
*			The x coordinate of the pixel.
*
* @param y
*			The y coordinate of the pixel.
*
* @return	The new color for the screen
*/
Pixel blendCanvas(Pixel canvas, GLint x, GLint y, int i, int j) {
	GLfloat Vl = newBrush[i][j].volume * 1 * R - newBrush[i][j].volume, Vr = newBrush[i][j].volume - Vl, r = 0, g = 0, b = 0;
	if (isCanvas) {
		r = (pixels[x][winHeight - y].volume * canvas.red + Vl * newBrush[i][j].sred) / (pixels[x][winHeight - y].volume + Vl);
		g = (pixels[x][winHeight - y].volume * canvas.green + Vl * newBrush[i][j].sgreen) / (pixels[x][winHeight - y].volume + Vl);
		b = (pixels[x][winHeight - y].volume * canvas.blue + Vl * newBrush[i][j].sblue) / (pixels[x][winHeight - y].volume + Vl);
	}
	else {
		r = (pixels[x][winHeight - y].pvolume * canvas.pred + Vl * newBrush[i][j].sred) / (pixels[x][winHeight - y].pvolume + Vl);
		g = (pixels[x][winHeight - y].pvolume * canvas.pgreen + Vl * newBrush[i][j].sgreen) / (pixels[x][winHeight - y].pvolume + Vl);
		b = (pixels[x][winHeight - y].pvolume * canvas.pblue + Vl * newBrush[i][j].sblue) / (pixels[x][winHeight - y].pvolume + Vl);
	}
	return Pixel(r, g, b);
}

/**
* Blends the brush with the canvas and updates the volume.
*
* Blends the canvas with the brush.
*
* @param canvas
*			The pixel from the canvas to blend.
*
* @param x
*			The x coordinate of the pixel.
*
* @param y
*			The y coordinate of the pixel.
*/
void blendBrush(Pixel canvas, GLint x, GLint y, int i, int j) {
		GLfloat Vl = pixels[x][winHeight - y].volume * 1 * R - pixels[x][winHeight - y].volume;
		GLfloat Vr = pixels[x][winHeight - y].volume - Vl;
		GLfloat t = newBrush[i][j].volume * 1 * R - newBrush[i][j].volume;
		if (isCanvas) {
			newBrush[i][j].sred = (newBrush[i][j].volume * newBrush[i][j].sred + Vl * canvas.red) / (newBrush[i][j].volume + Vl);
			newBrush[i][j].sgreen = (newBrush[i][j].volume * newBrush[i][j].sgreen + Vl * canvas.green) / (newBrush[i][j].volume + Vl);
			newBrush[i][j].sblue = (newBrush[i][j].volume * newBrush[i][j].sblue + Vl * canvas.blue) / (newBrush[i][j].volume + Vl);
			pixels[x][winHeight - y].volume = Vr + t + Vl;
			newBrush[i][j].volume = newBrush[i][j].volume - t + Vl;
		}
		else {
			Vl = pixels[x][winHeight - y].pvolume * 1 * R - pixels[x][winHeight - y].pvolume;
			Vr = pixels[x][winHeight - y].pvolume - Vl;
			newBrush[i][j].rred = (newBrush[i][j].volume * newBrush[i][j].rred + Vl * canvas.pred) / (newBrush[i][j].volume + Vl);
			newBrush[i][j].rgreen = (newBrush[i][j].volume * newBrush[i][j].rgreen + Vl * canvas.pgreen) / (newBrush[i][j].volume + Vl);
			newBrush[i][j].rblue = (newBrush[i][j].volume * newBrush[i][j].rblue + Vl * canvas.pblue) / (newBrush[i][j].volume + Vl);
			newBrush[i][j].sred = newBrush[i][j].rred;
			newBrush[i][j].sgreen = newBrush[i][j].rgreen;
			newBrush[i][j].sblue = newBrush[i][j].rblue;
			pixels[x][winHeight - y].pvolume = Vr + t;
			newBrush[i][j].volume = newBrush[i][j].volume - t + Vl;
		}
}
int er = 0;
bool drawn = false;

void oldDrawPixel(int x, int y, int i, int j) {
	if (newBrush[i][j].sred != -1) {
		if (!eraser) {
			if (!sketch) {
				if ((x >= 0 && y >= 0) && (isCanvas && y < winHeight - brushWidth) && x < winWidth) {
					//printf("%d\t%d\n", brush.rvolume, brush.volume);
					//printf("%f\t%f\t%f\n", brush.rred, brush.rgreen, brush.rblue);
					Pixel p = pixels[x][winHeight - y];
					if (!pixels[x][winHeight - y].line) {
						if (p.red != -1) {
							if (pixels[x][winHeight - y].volume == 0) {
								glBegin(GL_POINTS); //begins gl loop
								double Ot = opticaldepth(newBrush[i][j].sred, newBrush[i][j].sgreen, newBrush[i][j].sblue);
								pixels[x][winHeight - y].volume++;
								double alpha = std::min(pixels[x][winHeight - y].volume * -Ot, 1.0);
								float r = alpha * newBrush[i][j].sred + (1 - alpha) * newBrush[i][j].sred;
								float g = alpha * newBrush[i][j].sgreen + (1 - alpha) * newBrush[i][j].sgreen;
								float b = alpha * newBrush[i][j].sblue + (1 - alpha) * newBrush[i][j].sblue;
								glColor3f(r, g, b);
								glVertex2i(x, winHeight - y);
								pixels[x][winHeight - y].red = newBrush[i][j].sred;
								pixels[x][winHeight - y].green = newBrush[i][j].sgreen;
								pixels[x][winHeight - y].blue = newBrush[i][j].sblue;
								pixels[x][winHeight - y].dred = newBrush[i][j].sred;
								pixels[x][winHeight - y].dgreen = newBrush[i][j].sgreen;
								pixels[x][winHeight - y].dblue = newBrush[i][j].sblue;
								glEnd(); // ends gl loop
							}
							else {
								glBegin(GL_POINTS); //begins gl loop
								Pixel newP = blendCanvas(p, x, y, i, j);
								blendBrush(p, x, y, i, j);
								double Ot = opticaldepth(newP.red, newP.green, newP.blue);
								double alpha = std::min(pixels[x][winHeight - y].volume * Ot * -1, 1.0);
								float r = alpha * newP.red + (1 - alpha) * pixels[x][winHeight - y].dred;
								float g = alpha * newP.green + (1 - alpha) * pixels[x][winHeight - y].dgreen;
								float b = alpha * newP.blue + (1 - alpha) * pixels[x][winHeight - y].dblue;
								glColor3f(r, g, b);
								glVertex2i(x, winHeight - y);
								pixels[x][winHeight - y].red = newP.red;
								pixels[x][winHeight - y].green = newP.green;
								pixels[x][winHeight - y].blue = newP.blue;
								glEnd(); // ends gl loop
								dryLock.lock();
								dryingList.push_back(Point(x, y));
								dryLock.unlock();
							}
						}
						else {
							glBegin(GL_POINTS); //begins gl loop
							double Ot = opticaldepth(newBrush[i][j].sred, newBrush[i][j].sgreen, newBrush[i][j].sblue);
							pixels[x][winHeight - y].volume++;
							double alpha = std::min(pixels[x][winHeight - y].volume * -Ot, 1.0);
							float r = alpha * newBrush[i][j].sred + (1 - alpha) * newBrush[i][j].sred;
							float g = alpha * newBrush[i][j].sgreen + (1 - alpha) * newBrush[i][j].sgreen;
							float b = alpha * newBrush[i][j].sblue + (1 - alpha) * newBrush[i][j].sblue;
							glColor3f(r, g, b);
							glVertex2i(x, winHeight - y);
							pixels[x][winHeight - y].red = newBrush[i][j].sred;
							pixels[x][winHeight - y].green = newBrush[i][j].sgreen;
							pixels[x][winHeight - y].blue = newBrush[i][j].sblue;
							pixels[x][winHeight - y].dred = newBrush[i][j].sred;
							pixels[x][winHeight - y].dgreen = newBrush[i][j].sgreen;
							pixels[x][winHeight - y].dblue = newBrush[i][j].sblue;
							glEnd(); // ends gl loop
						}
						if (newBrush[i][j].volume <= 0) {
							newBrush[i][j].volume = 1;
							newBrush[i][j].sred = newBrush[i][j].rred;
							newBrush[i][j].sgreen = newBrush[i][j].rgreen;
							newBrush[i][j].sblue = newBrush[i][j].rblue;
							newBrush[i][j].rvolume -= 1;
							if (newBrush[i][j].rvolume <= 0) {
								newBrush[i][j].sred = -1;
								newBrush[i][j].sgreen = -1;
								newBrush[i][j].sblue = -1;
							}
						}
					}
					pixels[x][winHeight - y].setLine(true);
				}
				else if ((x >= 0 && y >= 0) && (!isCanvas && y < winHeight - brushWidth) && x < winWidth) {
					//printf("(%d, %d) - ", x, y);
					//printf("V = %f, Line = %s\n", pixels[x][winHeight - y].pvolume, pixels[x][winHeight - y].pline ? "true" : "false");
					Pixel p = pixels[x][winHeight - y];
					if (!pixels[x][winHeight - y].pline) {
						if (p.pred != -1) {
							if (pixels[x][winHeight - y].pvolume == 0) {
								glBegin(GL_POINTS); //begins gl loop
								glColor3f(newBrush[i][j].sred, newBrush[i][j].sgreen, newBrush[i][j].sblue);
								glVertex2i(x, winHeight - y);
								pixels[x][winHeight - y].pred = newBrush[i][j].sred;
								pixels[x][winHeight - y].pgreen = newBrush[i][j].sgreen;
								pixels[x][winHeight - y].pblue = newBrush[i][j].sblue;
								pixels[x][winHeight - y].red = newBrush[i][j].sred;
								pixels[x][winHeight - y].green = newBrush[i][j].sgreen;
								pixels[x][winHeight - y].blue = newBrush[i][j].sblue;
								pixels[x][winHeight - y].pvolume++;
								//printf("here = (%f, %f, %f)\n", pixels[x][winHeight - y].pred, pixels[x][winHeight - y].pgreen, pixels[x][winHeight - y].pblue);
								glEnd(); // ends gl loop
							}
							else {
								glBegin(GL_POINTS); //begins gl loop
								Pixel newP = blendCanvas(p, x, y, i, j);
								blendBrush(p, x, y, i, j);
								glColor3f(newP.red, newP.green, newP.blue);
								glVertex2i(x, winHeight - y);
								pixels[x][winHeight - y].pred = newP.red;
								pixels[x][winHeight - y].pgreen = newP.green;
								pixels[x][winHeight - y].pblue = newP.blue;
								pixels[x][winHeight - y].red = newP.red;
								pixels[x][winHeight - y].green = newP.green;
								pixels[x][winHeight - y].blue = newP.blue;
								//printf("hafs = (%f, %f, %f)\n", pixels[x][winHeight - y].pred, pixels[x][winHeight - y].pgreen, pixels[x][winHeight - y].pblue);
								glEnd(); // ends gl loop
							}
						}
						else {
							glBegin(GL_POINTS); //begins gl loop
							glColor3f(newBrush[i][j].sred, newBrush[i][j].sgreen, newBrush[i][j].sblue);
							glVertex2i(x, winHeight - y);
							pixels[x][winHeight - y].pred = newBrush[i][j].sred;
							pixels[x][winHeight - y].pgreen = newBrush[i][j].sgreen;
							pixels[x][winHeight - y].pblue = newBrush[i][j].sblue;
							pixels[x][winHeight - y].red = newBrush[i][j].sred;
							pixels[x][winHeight - y].green = newBrush[i][j].sgreen;
							pixels[x][winHeight - y].blue = newBrush[i][j].sblue;
							pixels[x][winHeight - y].pvolume++;
							//printf("sada = (%f, %f, %f)\n", pixels[x][winHeight - y].pred, pixels[x][winHeight - y].pgreen, pixels[x][winHeight - y].pblue);
							glEnd(); // ends gl loop
						}
						if (newBrush[i][j].volume <= 0) {
							newBrush[i][j].volume = 1;
							newBrush[i][j].sred = newBrush[i][j].rred;
							newBrush[i][j].sgreen = newBrush[i][j].rgreen;
							newBrush[i][j].sblue = newBrush[i][j].rblue;
							newBrush[i][j].rvolume -= 1;
							if (newBrush[i][j].rvolume <= 0) {
								newBrush[i][j].sred = -1;
								newBrush[i][j].sgreen = -1;
								newBrush[i][j].sblue = -1;
							}
						}
					}
					pixels[x][winHeight - y].pline = true;
				}
			}
			else {
				if ((x >= 0 && y >= 0) && (y < winHeight - brushWidth) && x < winWidth) {
					glBegin(GL_POINTS);
					glColor3f(newBrush[i][j].sred, newBrush[i][j].sgreen, newBrush[i][j].sblue);
					glVertex2i(x, winHeight - y);
					pixels[x][winHeight - y].red = newBrush[i][j].sred;
					pixels[x][winHeight - y].green = newBrush[i][j].sgreen;
					pixels[x][winHeight - y].blue = newBrush[i][j].sblue;
					glEnd(); // ends gl loop
					pixels[x][winHeight - y].setLine(true);
				}
			}
			pixels[x][winHeight - y].drawn = true;
		}
		else {
			pixels[x][winHeight - y].red = -1;
			pixels[x][winHeight - y].green = -1;
			pixels[x][winHeight - y].blue = -1;
			pixels[x][winHeight - y].dred = -1;
			pixels[x][winHeight - y].dgreen = -1;
			pixels[x][winHeight - y].dblue = -1;
			pixels[x][winHeight - y].pred = -1;
			pixels[x][winHeight - y].pgreen = -1;
			pixels[x][winHeight - y].pblue = -1;
			pixels[x][winHeight - y].volume = -1;
			pixels[x][winHeight - y].pvolume = -1;
			pixels[x][winHeight - y].alpha = -1;
			pixels[x][winHeight - y].drawn = false;
			glColor3f(backgroundC.red, backgroundC.green, backgroundC.blue);
			glBegin(GL_POINTS);
			glVertex2i(x, winHeight - y);
			glEnd();
		}
	}
	else {
		if ((x >= 0 && y >= 0) && (y < winHeight - brushWidth) && x < winWidth) {
			Pixel p = pixels[x][winHeight - y];
			newBrush[i][j].rred = pixels[x][winHeight - y].red;
			newBrush[i][j].rgreen = pixels[x][winHeight - y].green;
			newBrush[i][j].rblue = pixels[x][winHeight - y].blue;
			newBrush[i][j].rvolume = 50;
			newBrush[i][j].sred = pixels[x][winHeight - y].red;
			newBrush[i][j].sgreen = pixels[x][winHeight - y].green;
			newBrush[i][j].sblue = pixels[x][winHeight - y].blue;
			newBrush[i][j].volume = 1;
		}
	}
}

/**
* Sets the color of the x and y coordinate.
*
* @param x
*			The x coordinate.
*
* @param y
*			The y coordinate.
*/
void drawPixel(GLint x, GLint y) {
	if (!drawn) {
		for (int pX = x - newBrushWidth / 2, i = 0; pX <= x + newBrushWidth / 2 + 1; pX++, i++) {
			for (int pY = winHeight - y - newBrushHeight / 2, j = 0; pY <= winHeight - y + newBrushHeight / 2 + 1; pY++, j++) {
				if ((pX >= 0 && pY >= 0) && (pY < winHeight) && pX < winWidth) {
					if (i < newBrush.size() && j < newBrush[i].size()) {
						if (newBrush[i][j].s == 1) {
							if (pixels[pX][pY].selected) {
								if (!single) {
									//glScissor(pX, pY - 1, 1, 1);
									//glEnable(GL_SCISSOR_TEST);
									//glClear(GL_DEPTH_BUFFER_BIT);
									oldDrawPixel(pX, winHeight - pY, i, j);
									//glDisable(GL_SCISSOR_TEST);
								}
								else if (single) {
									//glScissor(pX, pY - 1, 1, 1);
									//glEnable(GL_SCISSOR_TEST);
									//glClear(GL_DEPTH_BUFFER_BIT);
									oldDrawPixel(pX, winHeight - pY, centerX, centerY);
									//glDisable(GL_SCISSOR_TEST);
								}
							}
						}
					}
				}
			}
		}
		/*glColor3f(1, 1, 1);
		glEnable(GL_BLEND); glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glBindTexture(GL_TEXTURE_2D, newBrushTexture);
		glEnable(GL_TEXTURE_2D);
		glBegin(GL_QUADS);
		glTexCoord2f(1, 1);
		glVertex2i(x - newBrushWidth / 2, winHeight - y - newBrushHeight / 2);
		glTexCoord2f(0, 1);
		glVertex2i((x + newBrushWidth / 2 + 1), winHeight - y - newBrushHeight / 2);
		glTexCoord2f(0, 0);
		glVertex2i((x + newBrushWidth / 2 + 1), winHeight - y + newBrushHeight / 2 + 1);
		glTexCoord2f(1, 0);
		glVertex2i(x - newBrushWidth / 2, (winHeight - y + newBrushHeight / 2 + 1));
		glEnd();
		glBindTexture(GL_TEXTURE_2D, 0);
		glDisable(GL_TEXTURE_2D);
		glDisable(GL_BLEND);*/
		if (er == 2000) {
			glBindTexture(GL_TEXTURE_2D, brushTexture);
			glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_UNSIGNED_BYTE, brushTextureData);
			int save_result = SOIL_save_image("brush.bmp", SOIL_SAVE_TYPE_BMP, 20, 20, 4, brushTextureData);
			newBrushTexture = SOIL_load_OGL_texture("alphaBrush.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, 0);
		}
		//drawn = true;
	}
	//canvasTextureData = malloc(winWidth * winHeight * 4);
	//glReadPixels(0, 0, winWidth, winHeight, GL_RGBA, GL_UNSIGNED_BYTE, canvasTextureData);
	//save_result = SOIL_save_image("canvas.bmp", SOIL_SAVE_TYPE_BMP, winWidth, winHeight, 4, (unsigned char*)canvasTextureData);
	//printf("%d", save_result);
	//saveBMP(myString, 10, 10, brushTextureData);
	//myString = "canvas.bmp";
	//saveBMP(myString, winWidth, winHeight, (BYTE*)canvasTextureData);
}

/**
* Uses the midpoint algorithm to draw lines on the screen
*
* @param x0
*			The first x coordinate.
* @param y0
*			The first y coordinate.
* @param x1
*			The end x coordinate.
* @param y1
*			The end y coordinate.
* @param spec
*			Special case identifier. 0 means normal case. 1 means -y case. 3 means y = x case. 4 means -y = x case.
*/
void drawLineMidpoint(GLint x0, GLint y0, GLint x1, GLint y1, GLint spec) {
	GLint dy = abs(y1 - y0);
	GLint dx = abs(x1 - x0);
	GLint ds = 2 * dy - dx, x = 0, y = 0;
	GLint incE = 2 * dy;
	GLint incNE = 2 * (dy - dx);
	//chooses the start point
	if (x0 > x1) {
		x = x1;
		y = y1;
		x1 = x0;
	}
	else {
		x = x0;
		y = y0;
	}
	//chooses what special case to execute
	// normal
	if (spec == 0) {//slope > 0 && slope < 1
		drawPixel(x, y);
		for (; x < x1; x++) {
			if (ds <= 0)
				ds += incE;
			else {
				ds += incNE;
				y++;
			}
			drawPixel(x, y);
		}
	}
	// -y
	else if (spec == 1) {//slope < 0 && slope > -1
		drawPixel(x, y);
		for (; x < x1; x++) {
			if (ds <= 0)
				ds += incE;
			else {
				ds += incNE;
				y--;
			}
			drawPixel(x, y);
		}
	}
	// -y = x
	else if (spec == 3) {//slope < -1
		drawPixel(y, x);
		for (; x < x1; x++) {
			if (ds <= 0)
				ds += incE;
			else {
				ds += incNE;
				y--;
			}
			drawPixel(y, x);
		}
	}
	// y = x
	else if (spec == 4) {//slope > 1
		drawPixel(y, x);
		for (; x < x1; x++) {
			if (ds <= 0)
				ds += incE;
			else {
				ds += incNE;
				y++;
			}
			drawPixel(y, x);
		}
	}
}

/**
* Draw line function.
*
* @param x0
*			The first x coordinate.
* @param y0
*			The first y coordinate.
* @param x1
*			The end x coordinate.
* @param y1
*			The end y coordinate.
*/
void drawLine(GLint x0, GLint y0, GLint x1, GLint y1) {
	if (y1 >= winHeight) {
		y1 = winHeight - 1;
	}
	else if (y1 < 0) {
		y1 = 0;
	}
	if (y0 >= winHeight) {
		y0 = winHeight - 1;
	}
	else if (y0 < 0) {
		y0 = 0;
	}
	if (x1 >= winWidth) {
		x1 = winWidth - 1;
	}
	else if (x1 < 0) {
		x1 = 0;
	}
	if (x0 >= winWidth) {
		x0 = winWidth - 1;
	}
	else if (x0 < 0) {
		x0 = 0;
	}
	if (y1 > y0) {
		//postive slope
		if (x1 > x0) {
			if (y1 - y0 > x1 - x0) {
				drawLineMidpoint(y0, x0, y1, x1, 4);
			}
			else if (y1 - y0 <= x1 - x0) {
				drawLineMidpoint(x0, y0, x1, y1, 0);
			}
		}
		//negative slope
		else if (x1 < x0) {
			if (abs(y1 - y0) <= abs(x1 - x0)) {
				drawLineMidpoint(x0, y0, x1, y1, 1);
			}
			else if (y1 - y0 > x1 - x0) {
				drawLineMidpoint(y0, x0, y1, x1, 3);
			}

		}
		//vertical line
		else {
			for (int i = y0; i != y1; i++) {
				drawPixel(x0, i);
			}
		}
	}
	else if (y1 < y0) {
		//negative slope
		if (x1 > x0) {
			if (abs(y1 - y0) > abs(x1 - x0)) {
				drawLineMidpoint(y0, x0, y1, x1, 3);
			}
			else if (y1 - y0 <= x1 - x0) {
				drawLineMidpoint(x0, y0, x1, y1, 1);
			}
		}
		//postive slope
		else if (x1 < x0) {
			if (y1 - y0 > x1 - x0) {
				drawLineMidpoint(x0, y0, x1, y1, 0);
			}
			else if (y1 - y0 <= x1 - x0) {
				drawLineMidpoint(y0, x0, y1, x1, 4);
			}
		}
		//vertical line
		else {
			for (int i = y1; i != y0; i++) {
				drawPixel(x0, i);
			}
		}
	}
	//horizontal line
	else {
		if (x1 > x0) {
			for (int i = x0; i != x1; i++) {
				drawPixel(i, y0);
			}
		}
		else if (x1 < x0) {
			for (int i = x1; i != x0; i++) {
				drawPixel(i, y0);
			}
		}
		else {
			drawPixel(x0, y0);
		}
	}
}

double findAngle(Point p0, Point p1, Point c) {
	double p0c = sqrt(pow(c.x - p0.x, 2) + pow(c.y - p0.y, 2));
	double p1c = sqrt(pow(c.x - p1.x, 2) + pow(c.y - p1.y, 2));
	double p0p1 = sqrt(pow(p1.x - p0.x, 2) + pow(p1.y - p0.y, 2));
	return acos((p1c * p1c + p0c * p0c - p0p1 * p0p1) / (2 * p1c * p0c));
}

Point rotatePoint(Point pivot, float theta, Point r) {
	float sinTheta = sin(theta);
	float cosTheta = cos(theta);
	float nx = r.x - pivot.x;
	float ny = r.y - pivot.y;
	float newx = nx * cosTheta - ny * sinTheta;
	float newy = nx * sinTheta + ny * cosTheta;
	return Point(newx + pivot.x, newy + pivot.y);
}

/**
* The display function for the canvas window.
*/
void displayCanvas() {
	glBindFramebuffer(GL_FRAMEBUFFER, fbo);
	glViewport(0, 0, winWidth, winHeight);
	if (redoBackground) {
		glColor3f(backgroundC.red, backgroundC.green, backgroundC.blue);
		for (int i = 0; i < winWidth; i++) {
			for (int j = 0; j < winHeight; j++) {
				if (!pixels[i][j].drawn) {
					glBegin(GL_POINTS);
					glVertex2i(i, j);
					glEnd();
				}
			}
		}
		redoBackground = false;
		first = false;
	}
	if (load) {
		glClear(GL_DEPTH_BUFFER_BIT);
		for (int x = 0; x < winWidth; x++) {
			for (int y = 0; y < winHeight; y++) {
				if (pixels[x][y].drawn) {
					//printf("%f\t%f\t%f\n", pixels[x][y].red, pixels[x][y].green, pixels[x][y].blue);
					glColor3f(pixels[x][y].red, pixels[x][y].green, pixels[x][y].blue);
					glBegin(GL_POINTS);
					glVertex2i(x, y);
					glEnd();
				}
			}
		}
		load = false;
	}
	if (undoSelect) {
		for (int x = 0; x < winWidth; x++) {
			for (int y = 0; y < winHeight; y++) {
				if (pixels[x][winHeight - y].isSelected) {
					pixels[x][winHeight - y].red = -1;
					pixels[x][winHeight - y].green = -1;
					pixels[x][winHeight - y].blue = -1;
					pixels[x][winHeight - y].dred = -1;
					pixels[x][winHeight - y].dgreen = -1;
					pixels[x][winHeight - y].dblue = -1;
					pixels[x][winHeight - y].pred = -1;
					pixels[x][winHeight - y].pgreen = -1;
					pixels[x][winHeight - y].pblue = -1;
					pixels[x][winHeight - y].volume = -1;
					pixels[x][winHeight - y].pvolume = -1;
					pixels[x][winHeight - y].alpha = -1;
					pixels[x][winHeight - y].drawn = false;
					glBegin(GL_POINTS); //begins gl loop
					glColor3f(backgroundC.red, backgroundC.green, backgroundC.blue);
					glVertex2i(x, winHeight - y);
					glEnd();
				}
			}
		}
		undoSelect = false;
	}
	glClear(GL_DEPTH_BUFFER_BIT);
	if (!first && firstBrush) {
		glClearColor(238.0 / 255.0, 231.0 / 255.0, 69.0 / 255.0, 1);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		/*glColor3f(backgroundC.red, backgroundC.green, backgroundC.blue);
		glBegin(GL_QUADS); {
			glVertex2i(0, winHeight);
			glVertex2i(0, 0);
			glVertex2i(winWidth, 0);
			glVertex2i(winWidth, winHeight);
		} glEnd();*/
		firstBrush = false;
	}
	glScissor(0, 0, winWidth, brushWidth + 6);
	glEnable(GL_SCISSOR_TEST);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glColor3f(newBrush[centerX][centerY].sred, 
		newBrush[centerX][centerY].sgreen, 
		newBrush[centerX][centerY].sblue);
	glBegin(GL_QUADS); {
		glVertex2i(0, brushWidth);
		glVertex2i(0, 0);
		glVertex2i(winWidth, 0);
		glVertex2i(winWidth, brushWidth);
		glColor3f(0, 0, 0);
		glVertex2i(0, brushWidth);
		glVertex2i(0, brushWidth + 6);
		glVertex2i(winWidth, brushWidth + 6);
		glVertex2i(winWidth, brushWidth);
	} glEnd();
	glDisable(GL_SCISSOR_TEST);
	glColor3f(0, 0, 0);
	displayWells();
	first = true;
	if (isDrawing && !selection && !colorSelection && !colorPicker && !crossbar) {
		if (prevXMouse == -1) {
			drawPixel(currXMouse, currYMouse);
		}
		else {
			drawLine(prevXMouse, prevYMouse, currXMouse, currYMouse);
		}
		/*if (prevXMouse == -1) {
			if (currYMouse - 1 > 0) {
				if (currXMouse - 1 > 0) {
					drawPixel(currXMouse - 1, currYMouse - 1);
				}
				if (currXMouse + 1 < winWidth) {
					drawPixel(currXMouse + 1, currYMouse - 1);
					drawPixel(currXMouse, currYMouse - 1);
				}
			}
			if (currYMouse + 1 < winHeight) {
				if (currXMouse - 1 > 0) {
					drawPixel(currXMouse - 1, currYMouse + 1);
					drawPixel(currXMouse - 1, currYMouse);
				}
				if (currXMouse + 1 < winWidth) {
					drawPixel(currXMouse + 1, currYMouse + 1);
					drawPixel(currXMouse + 1, currYMouse);
					drawPixel(currXMouse, currYMouse + 1);
					drawPixel(currXMouse, currYMouse);
				}
			}
		}*/
		/*else {
			if (prevXMouse < currXMouse) {
				if (prevYMouse < currYMouse) {
					drawLine(prevXMouse - 1, prevYMouse - 1, currXMouse - 1, currYMouse - 1);
					drawLine(prevXMouse - 1, prevYMouse, currXMouse - 1, currYMouse);
					drawLine(prevXMouse - 1, prevYMouse + 1, currXMouse - 1, currYMouse + 1);
					drawLine(prevXMouse, prevYMouse + 1, currXMouse, currYMouse + 1);
					drawLine(prevXMouse + 1, prevYMouse + 1, currXMouse + 1, currYMouse + 1);
				}
				else if (prevYMouse > currYMouse) {
					drawLine(prevXMouse - 1, prevYMouse - 1, currXMouse - 1, currYMouse - 1);
					drawLine(prevXMouse, prevYMouse - 1, currXMouse, currYMouse - 1);
					drawLine(prevXMouse + 1, prevYMouse - 1, currXMouse + 1, currYMouse - 1);
					drawLine(prevXMouse + 1, prevYMouse, currXMouse + 1, currYMouse);
					drawLine(prevXMouse + 1, prevYMouse + 1, currXMouse + 1, currYMouse + 1);
				}
				else {
					drawLine(prevXMouse + 1, prevYMouse - 1, currXMouse + 1, currYMouse - 1);
					drawLine(prevXMouse + 1, prevYMouse, currXMouse + 1, currYMouse);
					drawLine(prevXMouse + 1, prevYMouse + 1, currXMouse + 1, currYMouse + 1);
				}
			}
			else if (prevXMouse > currXMouse) {
				if (prevYMouse < currYMouse) {
					drawLine(prevXMouse + 1, prevYMouse - 1, currXMouse + 1, currYMouse - 1);
					drawLine(prevXMouse + 1, prevYMouse, currXMouse + 1, currYMouse);
					drawLine(prevXMouse - 1, prevYMouse + 1, currXMouse - 1, currYMouse + 1);
					drawLine(prevXMouse, prevYMouse + 1, currXMouse, currYMouse + 1);
					drawLine(prevXMouse + 1, prevYMouse + 1, currXMouse + 1, currYMouse + 1);
				}
				else if (prevYMouse > currYMouse) {
					drawLine(prevXMouse - 1, prevYMouse - 1, currXMouse - 1, currYMouse - 1);
					drawLine(prevXMouse - 1, prevYMouse, currXMouse - 1, currYMouse);
					drawLine(prevXMouse - 1, prevYMouse + 1, currXMouse - 1, currYMouse + 1);
					drawLine(prevXMouse, prevYMouse - 1, currXMouse, currYMouse - 1);
					drawLine(prevXMouse + 1, prevYMouse - 1, currXMouse + 1, currYMouse - 1);
				}
				else {
					drawLine(prevXMouse - 1, prevYMouse - 1, currXMouse - 1, currYMouse - 1);
					drawLine(prevXMouse - 1, prevYMouse, currXMouse - 1, currYMouse);
					drawLine(prevXMouse - 1, prevYMouse + 1, currXMouse - 1, currYMouse + 1);
				}
			}
			else {
				if (prevYMouse < currYMouse) {
					drawLine(prevXMouse - 1, prevYMouse + 1, currXMouse - 1, currYMouse + 1);
					drawLine(prevXMouse, prevYMouse + 1, currXMouse, currYMouse + 1);
					drawLine(prevXMouse + 1, prevYMouse + 1, currXMouse + 1, currYMouse + 1);
				}
				else if (prevYMouse > currYMouse) {
					drawLine(prevXMouse - 1, prevYMouse - 1, currXMouse - 1, currYMouse - 1);
					drawLine(prevXMouse, prevYMouse - 1, currXMouse, currYMouse - 1);
					drawLine(prevXMouse + 1, prevYMouse - 1, currXMouse + 1, currYMouse - 1);
				}
				else {
					drawPixel(currXMouse, currYMouse - 1);
					drawPixel(currXMouse, currYMouse);
					drawPixel(currXMouse, currYMouse + 1);
					drawPixel(currXMouse - 1, currYMouse - 1);
					drawPixel(currXMouse - 1, currYMouse);
					drawPixel(currXMouse - 1, currYMouse + 1);
					drawPixel(currXMouse + 1, currYMouse - 1);
					drawPixel(currXMouse + 1, currYMouse);
					drawPixel(currXMouse + 1, currYMouse + 1);
				}
			}
		}*/
		//subCanvasTexture();
		resetLineCheck(currXMouse, currYMouse);
	}
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(0, 0, winWidth, winHeight);
	//glDisable(GL_DEPTH_TEST);
	glClearColor(1.0, 1.0, 1.0, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(shader);
	glBindVertexArray(quadVAO);
	//glBindTexture(GL_TEXTURE_2D, canvasBackgroundTexture);	// Use the color attachment texture as the texture of the quad plane
	//glDrawArrays(GL_TRIANGLES, 0, 6);
	glBindTexture(GL_TEXTURE_2D, tcanvasTexture);	// Use the color attachment texture as the texture of the quad plane
	glDrawArrays(GL_TRIANGLES, 0, 6);
	//glEnable(GL_DEPTH_TEST);
	glBindVertexArray(0);
	glUseProgram(0);
	glPushMatrix();
	glColor3f(0, 0, 0);
	glBegin(GL_QUADS); {
		glVertex2i(barX, winHeight);
		glVertex2i(barX + 5, winHeight);
		glVertex2i(barX + 5, 0);
		glVertex2i(barX, 0);
	} glEnd();
	//glShadeModel(GL_FLAT);
	/*if (theta == 0) {
		polygon[0].x = currXMouse - 2;
		polygon[1].x = currXMouse + 2;
		polygon[2].x = currXMouse + 32;
		polygon[3].x = currXMouse + 30;
		polygon[0].y = winHeight - currYMouse + 2;
		polygon[1].y = winHeight - currYMouse - 2;
		polygon[2].y = winHeight - currYMouse + 38;
		polygon[3].y = winHeight - currYMouse + 40;
	}*/
	//rotate(Point(polygon[0].x, polygon[0].y), findAngle(Point(polygon[2].x, polygon[2].y),
	//	Point(currXMouse, currYMouse),Point(polygon[0].x, polygon[0].y)));
	//rotate(Point(polygon[0].x, polygon[0].y), theta);
	//transform();
	//setIdentity(composite);
	//printf("theta2 = %f\ttheta3 = %f\n", angle2, angle3);
	if (!selection && !colorSelection && !colorPicker && !crossbar) {
		glBegin(GL_TRIANGLES);
		glVertex2i(polygon[0].x, polygon[0].y);
		glVertex2i(polygon[2].x, polygon[2].y);
		glVertex2i(polygon[3].x, polygon[3].y);
		glVertex2i(polygon2[0].x, polygon2[0].y);
		glVertex2i(polygon2[1].x, polygon2[1].y);
		glVertex2i(polygon2[2].x, polygon2[2].y);
		glVertex2i(polygon3[0].x, polygon3[0].y);
		glVertex2i(polygon3[1].x, polygon3[1].y);
		glVertex2i(polygon3[2].x, polygon3[2].y);
		glEnd();
	}
	if (boxes.size() != 0) {
		for (int j = 0; j < boxes.size(); j++) {
			for (int i = 0; i < 10; i++) {
				if (i % 2 == 0) {
					glColor3f(0, 0, 0);
				}
				else {
					glColor3f(0.5, 0.5, 0.5);
				}
				glBegin(GL_QUADS);
				glVertex2i(-(i - 10) * boxes[j].one.x / 10 + i * boxes[j].two.x / 10, winHeight - boxes[j].one.y);
				glVertex2i(-(i - 10) * boxes[j].one.x / 10 + i * boxes[j].two.x / 10, winHeight - boxes[j].one.y + 5);
				glVertex2i(-(i - 9) * boxes[j].one.x / 10 + (i + 1) * boxes[j].two.x / 10, winHeight - boxes[j].one.y + 5);
				glVertex2i(-(i - 9) * boxes[j].one.x / 10 + (i + 1) * boxes[j].two.x / 10, winHeight - boxes[j].one.y);
				glEnd();
			}
			for (int i = 0; i < 10; i++) {
				if (i % 2 == 1) {
					glColor3f(0, 0, 0);
				}
				else {
					glColor3f(0.5, 0.5, 0.5);
				}
				glBegin(GL_QUADS);
				glVertex2i(-(i - 10) * boxes[j].one.x / 10 + i * boxes[j].two.x / 10, winHeight - boxes[j].two.y);
				glVertex2i(-(i - 10) * boxes[j].one.x / 10 + i * boxes[j].two.x / 10, winHeight - boxes[j].two.y + 5);
				glVertex2i(-(i - 9) * boxes[j].one.x / 10 + (i + 1) * boxes[j].two.x / 10, winHeight - boxes[j].two.y + 5);
				glVertex2i(-(i - 9) * boxes[j].one.x / 10 + (i + 1) * boxes[j].two.x / 10, winHeight - boxes[j].two.y);
				glEnd();
			}
			for (int i = 0; i < 10; i++) {
				if (i % 2 == 0) {
					glColor3f(0, 0, 0);
				}
				else {
					glColor3f(0.5, 0.5, 0.5);
				}
				glBegin(GL_QUADS);
				glVertex2i(boxes[j].two.x, winHeight - (-(i - 10) * boxes[j].one.y / 10 + i * boxes[j].two.y / 10));
				glVertex2i(boxes[j].two.x + 5, winHeight - (-(i - 10) *boxes[j].one.y / 10 + i * boxes[j].two.y / 10));
				glVertex2i(boxes[j].two.x + 5, winHeight - (-(i - 9) * boxes[j].one.y / 10 + (i + 1) * boxes[j].two.y / 10));
				glVertex2i(boxes[j].two.x, winHeight - (-(i - 9) * boxes[j].one.y / 10 + (i + 1) * boxes[j].two.y / 10));
				glEnd();
			}
			for (int i = 0; i < 10; i++) {
				if (i % 2 == 1) {
					glColor3f(0, 0, 0);
				}
				else {
					glColor3f(0.5, 0.5, 0.5);
				}
				glBegin(GL_QUADS);
				glVertex2i(boxes[j].one.x, winHeight - (-(i - 10) * boxes[j].one.y / 10 + i * boxes[j].two.y / 10));
				glVertex2i(boxes[j].one.x + 5, winHeight - (-(i - 10) * boxes[j].one.y / 10 + i * boxes[j].two.y / 10));
				glVertex2i(boxes[j].one.x + 5, winHeight - (-(i - 9) * boxes[j].one.y / 10 + (i + 1) * boxes[j].two.y / 10));
				glVertex2i(boxes[j].one.x, winHeight - (-(i - 9) * boxes[j].one.y / 10 + (i + 1) * boxes[j].two.y / 10));
				glEnd();
			}
		}
	}
	if (select2.x != -1) {
		glutSetCursor(GLUT_CURSOR_WAIT);
		if (select1.x < select2.x) {
			if (select1.y < select2.y) {
				for (int x = select1.x; x < select2.x; x++) {
					for (int y = select1.y; y < select2.y; y++) {
						pixels[x][winHeight - y].selected = true;
						pixels[x][winHeight - y].isSelected = true;
					}
				}
			}
			else if (select1.y > select2.y) {
				for (int x = select1.x; x < select2.x; x++) {
					for (int y = select2.y; y < select1.y; y++) {
						pixels[x][winHeight - y].selected = true;
						pixels[x][winHeight - y].isSelected = true;
					}
				}
			}
		}
		else if (select1.x > select2.x) {
			if (select1.y < select2.y) {
				for (int x = select2.x; x < select1.x; x++) {
					for (int y = select1.y; y < select2.y; y++) {
						pixels[x][winHeight - y].selected = true;
						pixels[x][winHeight - y].isSelected = true;
					}
				}
			}
			else if (select1.y > select2.y) {
				for (int x = select2.x; x < select1.x; x++) {
					for (int y = select2.y; y < select1.y; y++) {
						pixels[x][winHeight - y].selected = true;
						pixels[x][winHeight - y].isSelected = true;
					}
				}
			}
		}
		select1 = Point(-1, -1);
		select2 = Point(-1, -1);
		glutSetCursor(GLUT_CURSOR_CROSSHAIR);
	}
	glBindTexture(GL_TEXTURE_2D, 0);
	glPopMatrix();
	glutSwapBuffers();
	//createCanvasTexture();
}

/**
* Display the paint wells.
*/
void displayWells() {
	if (!isCanvas || !first) {
		if ((currXMouse >= winWidth / 2 + winWidth / 4 - 2 && currXMouse <= winWidth / 2 + winWidth / 4 + 22) || !first) {
			for (int x = winWidth / 2 + winWidth / 4 - 2; x < winWidth / 2 + winWidth / 4; x++) {
				glColor3f(0.25, 0.25, 0.25);
				for (int y = 9; y < 41; y++) {
					glBegin(GL_POINTS);
					glVertex2i(x, winHeight - y);
					glEnd();
				}
				for (int y = 49; y < 81; y++) {
					glBegin(GL_POINTS);
					glVertex2i(x, winHeight - y);
					glEnd();
				}
				for (int y = 89; y < 121; y++) {
					glBegin(GL_POINTS);
					glVertex2i(x, winHeight - y);
					glEnd();
				}
				for (int y = 129; y < 159; y++) {
					glBegin(GL_POINTS);
					glVertex2i(x, winHeight - y);
					glEnd();
				}
				for (int y = 169; y < 201; y++) {
					glBegin(GL_POINTS);
					glVertex2i(x, winHeight - y);
					glEnd();
				}
				for (int y = 209; y < 241; y++) {
					glBegin(GL_POINTS);
					glVertex2i(x, winHeight - y);
					glEnd();
				}
				for (int y = 249; y < 281; y++) {
					glBegin(GL_POINTS);
					glVertex2i(x, winHeight - y);
					glEnd();
				}
				for (int y = 289; y < 321; y++) {
					glBegin(GL_POINTS);
					glVertex2i(x, winHeight - y);
					glEnd();
				}
				for (int y = 329; y < 361; y++) {
					glBegin(GL_POINTS);
					glVertex2i(x, winHeight - y);
					glEnd();
				}
				for (int y = 369; y < 401; y++) {
					glBegin(GL_POINTS);
					glVertex2i(x, winHeight - y);
					glEnd();
				}
				for (int y = 409; y < 441; y++) {
					glBegin(GL_POINTS);
					glVertex2i(x, winHeight - y);
					glEnd();
				}
				for (int y = 449; y < 481; y++) {
					glBegin(GL_POINTS);
					glVertex2i(x, winHeight - y);
					glEnd();
				}
				for (int y = 489; y < 521; y++) {
					glBegin(GL_POINTS);
					glVertex2i(x, winHeight - y);
					glEnd();
				}
				for (int y = 529; y < 561; y++) {
					glBegin(GL_POINTS);
					glVertex2i(x, winHeight - y);
					glEnd();
				}
				for (int y = 569; y < 601; y++) {
					glBegin(GL_POINTS);
					glVertex2i(x, winHeight - y);
					glEnd();
				}
			}
			for (int x = winWidth / 2 + winWidth / 4; x < winWidth / 2 + winWidth / 4 + 21; x++) {
				glColor3f(0.25, 0.25, 0.25);
				int y = 9;
				glBegin(GL_POINTS);
				glVertex2i(x, winHeight - y);
				glEnd();
				y = 40;
				glBegin(GL_POINTS);
				glVertex2i(x, winHeight - y);
				glEnd();
				y = 49;
				glBegin(GL_POINTS);
				glVertex2i(x, winHeight - y);
				glEnd();
				y = 80;
				glBegin(GL_POINTS);
				glVertex2i(x, winHeight - y);
				glEnd();
				y = 89;
				glBegin(GL_POINTS);
				glVertex2i(x, winHeight - y);
				glEnd();
				y = 120;
				glBegin(GL_POINTS);
				glVertex2i(x, winHeight - y);
				glEnd();
				y = 129;
				glBegin(GL_POINTS);
				glVertex2i(x, winHeight - y);
				glEnd();
				y = 159;
				glBegin(GL_POINTS);
				glVertex2i(x, winHeight - y);
				glEnd();
				y = 169;
				glBegin(GL_POINTS);
				glVertex2i(x, winHeight - y);
				glEnd();
				y = 200;
				glBegin(GL_POINTS);
				glVertex2i(x, winHeight - y);
				glEnd();
				y = 209;
				glBegin(GL_POINTS);
				glVertex2i(x, winHeight - y);
				glEnd();
				y = 240;
				glBegin(GL_POINTS);
				glVertex2i(x, winHeight - y);
				glEnd();
				y = 249;
				glBegin(GL_POINTS);
				glVertex2i(x, winHeight - y);
				glEnd();
				y = 280;
				glBegin(GL_POINTS);
				glVertex2i(x, winHeight - y);
				glEnd();
				y = 289;
				glBegin(GL_POINTS);
				glVertex2i(x, winHeight - y);
				glEnd();
				y = 320;
				glBegin(GL_POINTS);
				glVertex2i(x, winHeight - y);
				glEnd();
				y = 329;
				glBegin(GL_POINTS);
				glVertex2i(x, winHeight - y);
				glEnd();
				y = 360;
				glBegin(GL_POINTS);
				glVertex2i(x, winHeight - y);
				glEnd();
				y = 369;
				glBegin(GL_POINTS);
				glVertex2i(x, winHeight - y);
				glEnd();
				y = 400;
				glBegin(GL_POINTS);
				glVertex2i(x, winHeight - y);
				glEnd();
				y = 409;
				glBegin(GL_POINTS);
				glVertex2i(x, winHeight - y);
				glEnd();
				y = 440;
				glBegin(GL_POINTS);
				glVertex2i(x, winHeight - y);
				glEnd();
				y = 449;
				glBegin(GL_POINTS);
				glVertex2i(x, winHeight - y);
				glEnd();
				y = 480;
				glBegin(GL_POINTS);
				glVertex2i(x, winHeight - y);
				glEnd();
				y = 489;
				glBegin(GL_POINTS);
				glVertex2i(x, winHeight - y);
				glEnd();
				y = 520;
				glBegin(GL_POINTS);
				glVertex2i(x, winHeight - y);
				glEnd();
				y = 529;
				glBegin(GL_POINTS);
				glVertex2i(x, winHeight - y);
				glEnd();
				y = 560;
				glBegin(GL_POINTS);
				glVertex2i(x, winHeight - y);
				glEnd();
				y = 569;
				glBegin(GL_POINTS);
				glVertex2i(x, winHeight - y);
				glEnd();
				y = 600;
				glBegin(GL_POINTS);
				glVertex2i(x, winHeight - y);
				glEnd();
			}
			for (int x = winWidth / 2 + winWidth / 4 + 20; x < winWidth / 2 + winWidth / 4 + 22; x++) {
				glColor3f(0.25, 0.25, 0.25);
				for (int y = 9; y < 41; y++) {
					glBegin(GL_POINTS);
					glVertex2i(x, winHeight - y);
					glEnd();
				}
				for (int y = 49; y < 81; y++) {
					glBegin(GL_POINTS);
					glVertex2i(x, winHeight - y);
					glEnd();
				}
				for (int y = 89; y < 121; y++) {
					glBegin(GL_POINTS);
					glVertex2i(x, winHeight - y);
					glEnd();
				}
				for (int y = 129; y < 159; y++) {
					glBegin(GL_POINTS);
					glVertex2i(x, winHeight - y);
					glEnd();
				}
				for (int y = 169; y < 201; y++) {
					glBegin(GL_POINTS);
					glVertex2i(x, winHeight - y);
					glEnd();
				}
				for (int y = 209; y < 241; y++) {
					glBegin(GL_POINTS);
					glVertex2i(x, winHeight - y);
					glEnd();
				}
				for (int y = 249; y < 281; y++) {
					glBegin(GL_POINTS);
					glVertex2i(x, winHeight - y);
					glEnd();
				}
				for (int y = 289; y < 321; y++) {
					glBegin(GL_POINTS);
					glVertex2i(x, winHeight - y);
					glEnd();
				}
				for (int y = 329; y < 361; y++) {
					glBegin(GL_POINTS);
					glVertex2i(x, winHeight - y);
					glEnd();
				}
				for (int y = 369; y < 401; y++) {
					glBegin(GL_POINTS);
					glVertex2i(x, winHeight - y);
					glEnd();
				}
				for (int y = 409; y < 441; y++) {
					glBegin(GL_POINTS);
					glVertex2i(x, winHeight - y);
					glEnd();
				}
				for (int y = 449; y < 481; y++) {
					glBegin(GL_POINTS);
					glVertex2i(x, winHeight - y);
					glEnd();
				}
				for (int y = 489; y < 521; y++) {
					glBegin(GL_POINTS);
					glVertex2i(x, winHeight - y);
					glEnd();
				}
				for (int y = 529; y < 561; y++) {
					glBegin(GL_POINTS);
					glVertex2i(x, winHeight - y);
					glEnd();
				}
				for (int y = 569; y < 601; y++) {
					glBegin(GL_POINTS);
					glVertex2i(x, winHeight - y);
					glEnd();
				}
			}
			for (int x = winWidth / 2 + winWidth / 4; x < winWidth / 2 + winWidth / 4 + 20; x++) {
				for (int y = 10; y < 40; y++) {
					drawPixelPaletteWell(x, y, 1, 0, 0);
				}
				for (int y = 50; y < 80; y++) {
					drawPixelPaletteWell(x, y, 0, 1, 0);
				}
				for (int y = 90; y < 120; y++) {
					drawPixelPaletteWell(x, y, 0, 0, 1);
				}
				for (int y = 130; y < 160; y++) {
					drawPixelPaletteWell(x, y, 0, 1, 1);
				}
				for (int y = 170; y < 200; y++) {
					drawPixelPaletteWell(x, y, 1, 1, 0);
				}
				for (int y = 210; y < 240; y++) {
					drawPixelPaletteWell(x, y, 1, 0, 1);
				}
				for (int y = 250; y < 280; y++) {
					drawPixelPaletteWell(x, y, 0, 0, 0.5);
				}
				for (int y = 290; y < 320; y++) {
					drawPixelPaletteWell(x, y, 0, 0.5, 0);
				}
				for (int y = 330; y < 360; y++) {
					drawPixelPaletteWell(x, y, 0.5, 0, 0);
				}
				for (int y = 370; y < 400; y++) {
					drawPixelPaletteWell(x, y, 0, 0.5, 0.5);
				}
				for (int y = 410; y < 440; y++) {
					drawPixelPaletteWell(x, y, 0.5, 0, 0.5);
				}
				for (int y = 450; y < 480; y++) {
					drawPixelPaletteWell(x, y, 0.5, 0.5, 0);
				}
				for (int y = 490; y < 520; y++) {
					drawPixelPaletteWell(x, y, 0.5, 0.5, 0.5);
				}
				for (int y = 530; y < 560; y++) {
					drawPixelPaletteWell(x, y, 0, 0, 0);
				}
				for (int y = 570; y < 600; y++) {
					drawPixelPaletteWell(x, y, 1, 1, 1);
				}
			}
		}
	}
}

/**
* Reads mouse input from the user.
*
* @param button
*			The mouse button clicked.
*
* @param action
*			The action of the mouse (UP or DOWN)
*
* @param xMouse
*			The x coordinate of the mouse click event
*
* @param yMouse
*			The y coordinate of the mouse click event.
*/
void mouseInput(GLint button, GLint action, GLint xMouse, GLint yMouse) {
	if (button == GLUT_LEFT_BUTTON) {
		if (action == GLUT_DOWN) {
			currXMouse = xMouse;
			currYMouse = yMouse;
			isDrawing = true;
			if (selection) {
				if (select1.x == -1) {
					select1.x = currXMouse;
					select1.y = currYMouse;
				}
				else if (select2.x == -1) {
					select2.x = currXMouse;
					select2.y = currYMouse;
					boxes.push_back(Box(select1, select2));
					glutPostRedisplay();
				}
			}
			else if (colorSelection) {
				glutSetCursor(GLUT_CURSOR_WAIT);
				for (int x = 0; x < winWidth; x++) {
					for (int y = 0; y < winHeight; y++) {
						if (pixels[currXMouse][winHeight - currYMouse].red * 255 <= pixels[x][y].red * 255 + 1 &&
							pixels[currXMouse][winHeight - currYMouse].green * 255 <= pixels[x][y].green * 255 + 1 &&
							pixels[currXMouse][winHeight - currYMouse].blue * 255 <= pixels[x][y].blue * 255 + 1 &&
							pixels[currXMouse][winHeight - currYMouse].red * 255 >= pixels[x][y].red * 255 - 1 &&
							pixels[currXMouse][winHeight - currYMouse].green * 255 >= pixels[x][y].green * 255 - 1 &&
							pixels[currXMouse][winHeight - currYMouse].blue * 255 >= pixels[x][y].blue * 255 - 1) 
						{
							pixels[x][y].selected = true;
							pixels[x][y].isSelected = true;
							//printf("Selected: %f, %f, %f\n", pixels[x][y].red * 255, pixels[x][y].green * 255, pixels[x][y].blue * 255);
						}
						else if (!pixels[x][y].isSelected) {
							//printf("%d, %d, %d\n", pixels[x][y].red * 255, pixels[x][y].green * 255, pixels[x][y].blue * 255);
							pixels[x][y].selected = false;
						}
					}
				}
				glutSetCursor(GLUT_CURSOR_RIGHT_ARROW);
			}
			else if (colorPicker) {
				if (!eraser) {
					if (fil) {
						int tempw = newBrushWidth;
						int temph = newBrushHeight;
						newBrushHeight = 6;
						newBrushWidth = 6;
						for (int i = 0; i < newBrushWidth; i++) {
							for (int j = 0; j < newBrushHeight; j++) {
								filRightDown[i][j].sred = pixels[currXMouse][winHeight - currYMouse].red;
								filRightDown[i][j].sgreen = pixels[currXMouse][winHeight - currYMouse].green;
								filRightDown[i][j].sblue = pixels[currXMouse][winHeight - currYMouse].blue;
								filRightDown[i][j].rred = pixels[currXMouse][winHeight - currYMouse].red;
								filRightDown[i][j].rgreen = pixels[currXMouse][winHeight - currYMouse].green;
								filRightDown[i][j].rblue = pixels[currXMouse][winHeight - currYMouse].blue;
								filRightDown[i][j].volume = 0;
								filRightDown[i][j].rvolume = 50;
							}
						}
						newBrushHeight = 6;
						newBrushWidth = 6;
						for (int i = 0; i < newBrushWidth; i++) {
							for (int j = 0; j < newBrushHeight; j++) {
								filLeftDown[i][j].sred = pixels[currXMouse][winHeight - currYMouse].red;
								filLeftDown[i][j].sgreen = pixels[currXMouse][winHeight - currYMouse].green;
								filLeftDown[i][j].sblue = pixels[currXMouse][winHeight - currYMouse].blue;
								filLeftDown[i][j].rred = pixels[currXMouse][winHeight - currYMouse].red;
								filLeftDown[i][j].rgreen = pixels[currXMouse][winHeight - currYMouse].green;
								filLeftDown[i][j].rblue = pixels[currXMouse][winHeight - currYMouse].blue;
								filLeftDown[i][j].volume = 0;
								filLeftDown[i][j].rvolume = 50;
							}
						}
						newBrushHeight = 5;
						newBrushWidth = 7;
						for (int i = 0; i < newBrushWidth; i++) {
							for (int j = 0; j < newBrushHeight; j++) {
								filLeft[i][j].sred = pixels[currXMouse][winHeight - currYMouse].red;
								filLeft[i][j].sgreen = pixels[currXMouse][winHeight - currYMouse].green;
								filLeft[i][j].sblue = pixels[currXMouse][winHeight - currYMouse].blue;
								filLeft[i][j].rred = pixels[currXMouse][winHeight - currYMouse].red;
								filLeft[i][j].rgreen = pixels[currXMouse][winHeight - currYMouse].green;
								filLeft[i][j].rblue = pixels[currXMouse][winHeight - currYMouse].blue;
								filLeft[i][j].volume = 0;
								filLeft[i][j].rvolume = 50;
							}
						}
						newBrushHeight = 7;
						newBrushWidth = 5;
						for (int i = 0; i < newBrushWidth; i++) {
							for (int j = 0; j < newBrushHeight; j++) {
								filUp[i][j].sred = pixels[currXMouse][winHeight - currYMouse].red;
								filUp[i][j].sgreen = pixels[currXMouse][winHeight - currYMouse].green;
								filUp[i][j].sblue = pixels[currXMouse][winHeight - currYMouse].blue;
								filUp[i][j].rred = pixels[currXMouse][winHeight - currYMouse].red;
								filUp[i][j].rgreen = pixels[currXMouse][winHeight - currYMouse].green;
								filUp[i][j].rblue = pixels[currXMouse][winHeight - currYMouse].blue;
								filUp[i][j].volume = 0;
								filUp[i][j].rvolume = 50;
							}
						}
						newBrushWidth = tempw;
						newBrushHeight = temph;
						for (int i = 0; i < newBrushWidth; i++) {
							for (int j = 0; j < newBrushHeight; j++) {
								newBrush[i][j].sred = pixels[currXMouse][winHeight - currYMouse].red;
								newBrush[i][j].sgreen = pixels[currXMouse][winHeight - currYMouse].green;
								newBrush[i][j].sblue = pixels[currXMouse][winHeight - currYMouse].blue;
								newBrush[i][j].rred = pixels[currXMouse][winHeight - currYMouse].red;
								newBrush[i][j].rgreen = pixels[currXMouse][winHeight - currYMouse].green;
								newBrush[i][j].rblue = pixels[currXMouse][winHeight - currYMouse].blue;
								newBrush[i][j].volume = 0;
								newBrush[i][j].rvolume = 50;
							}
						}
					}
					else if (flat) {
							int tempw = newBrushWidth;
							int temph = newBrushHeight;
							newBrushHeight = 7;
							newBrushWidth = 7;
							for (int i = 0; i < newBrushWidth; i++) {
								for (int j = 0; j < newBrushHeight; j++) {
									flaRightUp[i][j].sred = pixels[currXMouse][winHeight - currYMouse].red;
									flaRightUp[i][j].sgreen = pixels[currXMouse][winHeight - currYMouse].green;
									flaRightUp[i][j].sblue = pixels[currXMouse][winHeight - currYMouse].blue;
									flaRightUp[i][j].rred = pixels[currXMouse][winHeight - currYMouse].red;
									flaRightUp[i][j].rgreen = pixels[currXMouse][winHeight - currYMouse].green;
									flaRightUp[i][j].rblue = pixels[currXMouse][winHeight - currYMouse].blue;
									flaRightUp[i][j].volume = 0;
									flaRightUp[i][j].rvolume = 50;
								}
							}
							newBrushHeight = 7;
							newBrushWidth = 7;
							for (int i = 0; i < newBrushWidth; i++) {
								for (int j = 0; j < newBrushHeight; j++) {
									flaLeftUp[i][j].sred = pixels[currXMouse][winHeight - currYMouse].red;
									flaLeftUp[i][j].sgreen = pixels[currXMouse][winHeight - currYMouse].green;
									flaLeftUp[i][j].sblue = pixels[currXMouse][winHeight - currYMouse].blue;
									flaLeftUp[i][j].rred = pixels[currXMouse][winHeight - currYMouse].red;
									flaLeftUp[i][j].rgreen = pixels[currXMouse][winHeight - currYMouse].green;
									flaLeftUp[i][j].rblue = pixels[currXMouse][winHeight - currYMouse].blue;
									flaLeftUp[i][j].volume = 0;
									flaLeftUp[i][j].rvolume = 50;
								}
							}
							newBrushHeight = 9;
							newBrushWidth = 5;
							for (int i = 0; i < newBrushWidth; i++) {
								for (int j = 0; j < newBrushHeight; j++) {
									flaLeft[i][j].sred = pixels[currXMouse][winHeight - currYMouse].red;
									flaLeft[i][j].sgreen = pixels[currXMouse][winHeight - currYMouse].green;
									flaLeft[i][j].sblue = pixels[currXMouse][winHeight - currYMouse].blue;
									flaLeft[i][j].rred = pixels[currXMouse][winHeight - currYMouse].red;
									flaLeft[i][j].rgreen = pixels[currXMouse][winHeight - currYMouse].green;
									flaLeft[i][j].rblue = pixels[currXMouse][winHeight - currYMouse].blue;
									flaLeft[i][j].volume = 0;
									flaLeft[i][j].rvolume = 50;
								}
							}
							newBrushHeight = 5;
							newBrushWidth = 9;
							for (int i = 0; i < newBrushWidth; i++) {
								for (int j = 0; j < newBrushHeight; j++) {
									flaUp[i][j].sred = pixels[currXMouse][winHeight - currYMouse].red;
									flaUp[i][j].sgreen = pixels[currXMouse][winHeight - currYMouse].green;
									flaUp[i][j].sblue = pixels[currXMouse][winHeight - currYMouse].blue;
									flaUp[i][j].rred = pixels[currXMouse][winHeight - currYMouse].red;
									flaUp[i][j].rgreen = pixels[currXMouse][winHeight - currYMouse].green;
									flaUp[i][j].rblue = pixels[currXMouse][winHeight - currYMouse].blue;
									flaUp[i][j].volume = 0;
									flaUp[i][j].rvolume = 50;
								}
							}
							newBrushWidth = tempw;
							newBrushHeight = temph;
							for (int i = 0; i < newBrushWidth; i++) {
								for (int j = 0; j < newBrushHeight; j++) {
									newBrush[i][j].sred = pixels[currXMouse][winHeight - currYMouse].red;
									newBrush[i][j].sgreen = pixels[currXMouse][winHeight - currYMouse].green;
									newBrush[i][j].sblue = pixels[currXMouse][winHeight - currYMouse].blue;
									newBrush[i][j].rred = pixels[currXMouse][winHeight - currYMouse].red;
									newBrush[i][j].rgreen = pixels[currXMouse][winHeight - currYMouse].green;
									newBrush[i][j].rblue = pixels[currXMouse][winHeight - currYMouse].blue;
									newBrush[i][j].volume = 0;
									newBrush[i][j].rvolume = 50;
								}
							}
					}
					else {
						for (int i = 0; i < newBrushWidth; i++) {
							for (int j = 0; j < newBrushHeight; j++) {
								cirBrush[i][j].sred = pixels[currXMouse][winHeight - currYMouse].red;
								cirBrush[i][j].sgreen = pixels[currXMouse][winHeight - currYMouse].green;
								cirBrush[i][j].sblue = pixels[currXMouse][winHeight - currYMouse].blue;
								cirBrush[i][j].rred = pixels[currXMouse][winHeight - currYMouse].red;
								cirBrush[i][j].rgreen = pixels[currXMouse][winHeight - currYMouse].green;
								cirBrush[i][j].rblue = pixels[currXMouse][winHeight - currYMouse].blue;
								cirBrush[i][j].volume = 0;
								cirBrush[i][j].rvolume = 50;
							}
						}
						for (int i = 0; i < newBrushWidth; i++) {
							for (int j = 0; j < newBrushHeight; j++) {
								newBrush[i][j].sred = pixels[currXMouse][winHeight - currYMouse].red;
								newBrush[i][j].sgreen = pixels[currXMouse][winHeight - currYMouse].green;
								newBrush[i][j].sblue = pixels[currXMouse][winHeight - currYMouse].blue;
								newBrush[i][j].rred = pixels[currXMouse][winHeight - currYMouse].red;
								newBrush[i][j].rgreen = pixels[currXMouse][winHeight - currYMouse].green;
								newBrush[i][j].rblue = pixels[currXMouse][winHeight - currYMouse].blue;
								newBrush[i][j].volume = 0;
								newBrush[i][j].rvolume = 50;
							}
						}
					}
				}
			}
			else if (crossbar) {
				if (currXMouse < winWidth / 2 + winWidth / 4 - 10 && currXMouse > 10) {
					barX = currXMouse;
					glutPostRedisplay();
				}
			}
		}
		else if (action == GLUT_UP) {
			isDrawing = false;
			prevXMouse = -1;
			prevYMouse = -1;
			//brush.volume = 255;
			//red = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
			//green = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
			//blue = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
		}
	}
}

Point pointFromEndOfLine(Point start, Point end, double distance) {
	double x = end.x - start.x;
	double y = end.y - start.y;
	double z = sqrt(x * x + y * y);  //Pathagrean Theorum for Hypotenuse
	double ratio = distance / z;
	double deltaX = x * ratio;
	double deltaY = y * ratio;
	return Point(end.x - deltaX, end.y - deltaY);
}

void fillfilUpFromRightDown() {
	for (int i = 0; i < newBrushWidth; i++) {
		for (int j = 0; j < newBrushHeight; j++) {
			if (i == 0) {
				if (j == 2) {
					newBrush[i][j] = filRightDown[4][2];
				}
				else if (j == 3) {
					newBrush[i][j] = filRightDown[1][3];
				}
				else if (j == 4) {
					newBrush[i][j] = filRightDown[1][4];
				}
			}
			else if (i == 1) {
				if (j == 1) {
					newBrush[i][j] = filRightDown[4][1];
				}
				else if (j == 2) {
					newBrush[i][j] = filRightDown[1][2];
				}
				else if (j == 3) {
					newBrush[i][j] = filRightDown[2][3];
				}
				else if (j == 4) {
					newBrush[i][j] = filRightDown[2][4];
				}
				else if (j == 5) {
					newBrush[i][j] = filRightDown[0][4];
				}
			}
			else if (i == 2) {
				if (j == 0) {
					newBrush[i][j] = filRightDown[5][0];
				}
				else if (j == 1) {
					newBrush[i][j] = filRightDown[4][0];
				}
				else if (j == 2) {
					newBrush[i][j] = filRightDown[1][1];
				}
				else if (j == 3) {
					newBrush[i][j] = filRightDown[3][3];
				}
				else if (j == 4) {
					newBrush[i][j] = filRightDown[3][4];
				}
				else if (j == 5) {
					newBrush[i][j] = filRightDown[1][5];
				}
				else if (j == 6) {
					newBrush[i][j] = filRightDown[0][5];
				}
			}
			else if (i == 3) {
				if (j == 1) {
					newBrush[i][j] = filRightDown[5][1];
				}
				if (j == 2) {
					newBrush[i][j] = filRightDown[2][1];
				}
				else if (j == 3) {
					newBrush[i][j] = filRightDown[2][2];
				}
				else if (j == 4) {
					newBrush[i][j] = filRightDown[4][4];
				}
				else if (j == 5) {
					newBrush[i][j] = filRightDown[2][5];
				}
			}
			else if (i == 4) {
				if (j == 2) {
					newBrush[i][j] = filRightDown[3][1];
				}
				else if (j == 3) {
					newBrush[i][j] = filRightDown[3][2];
				}
				else if (j == 4) {
					newBrush[i][j] = filRightDown[4][3];
				}
			}
		}
	}
	boolfilRightDown = false;
	boolfilLeftDown = false;
	boolFilLeft = false;
	boolFilUp = true;
}

void fillfilRightDownFromUp() {
	for (int i = 0; i < newBrushWidth; i++) {
		for (int j = 0; j < newBrushHeight; j++) {
			if (i == 0) {
				if (j == 2) {
					filRightDown[4][2] = newBrush[i][j];
				}
				else if (j == 3) {
					filRightDown[1][3] = newBrush[i][j];
				}
				else if (j == 4) {
					filRightDown[1][4] = newBrush[i][j];
				}
			}
			else if (i == 1) {
				if (j == 1) {
					filRightDown[4][1] = newBrush[i][j];
				}
				else if (j == 2) {
					filRightDown[1][2] = newBrush[i][j];
				}
				else if (j == 3) {
					filRightDown[2][3] = newBrush[i][j];
				}
				else if (j == 4) {
					filRightDown[2][4] = newBrush[i][j];
				}
				else if (j == 5) {
					filRightDown[0][4] = newBrush[i][j];
				}
			}
			else if (i == 2) {
				if (j == 0) {
					filRightDown[5][0] = newBrush[i][j];
				}
				else if (j == 1) {
					filRightDown[4][0] = newBrush[i][j];
				}
				else if (j == 2) {
					filRightDown[1][1] = newBrush[i][j];
				}
				else if (j == 3) {
					filRightDown[3][3] = newBrush[i][j];
				}
				else if (j == 4) {
					filRightDown[3][4] = newBrush[i][j];
				}
				else if (j == 5) {
					filRightDown[1][5] = newBrush[i][j];
				}
				else if (j == 6) {
					filRightDown[0][5] = newBrush[i][j];
				}
			}
			else if (i == 3) {
				if (j == 1) {
					filRightDown[5][1] = newBrush[i][j];
				}
				if (j == 2) {
					filRightDown[2][1] = newBrush[i][j];
				}
				else if (j == 3) {
					filRightDown[2][2] = newBrush[i][j];
				}
				else if (j == 4) {
					filRightDown[4][4] = newBrush[i][j];
				}
				else if (j == 5) {
					filRightDown[2][5] = newBrush[i][j];
				}
			}
			else if (i == 4) {
				if (j == 2) {
					filRightDown[3][1] = newBrush[i][j];
				}
				else if (j == 3) {
					filRightDown[3][2] = newBrush[i][j];
				}
				else if (j == 4) {
					filRightDown[4][3] = newBrush[i][j];
				}
			}
		}
	}
	boolfilRightDown = true;
	boolfilLeftDown = false;
	boolFilLeft = false;
	boolFilUp = false;
}

void fillfilLeftFromUp() {
	for (int i = 0; i < newBrushWidth; i++) {
		for (int j = 0; j < newBrushHeight; j++) {
			filLeft[j][i] = newBrush[i][j];
		}
	}
	boolfilRightDown = false;
	boolfilLeftDown = false;
	boolFilLeft = true;
	boolFilUp = false;
}

void fillfilLeftDownFromUp() {
	for (int i = 0; i < newBrushWidth; i++) {
		for (int j = 0; j < newBrushHeight; j++) {
			if (i == 0) {
				if (j == 2) {
					filLeftDown[1][2] = newBrush[i][j];
				}
				else if (j == 3) {
					filLeftDown[4][3] = newBrush[i][j];
				}
				else if (j == 4) {
					filLeftDown[4][4] = newBrush[i][j];
				}
			}
			else if (i == 1) {
				if (j == 1) {
					filLeftDown[1][1] = newBrush[i][j];
				}
				else if (j == 2) {
					filLeftDown[4][2] = newBrush[i][j];
				}
				else if (j == 3) {
					filLeftDown[3][3] = newBrush[i][j];
				}
				else if (j == 4) {
					filLeftDown[3][4] = newBrush[i][j];
				}
				else if (j == 5) {
					filLeftDown[5][4] = newBrush[i][j];
				}
			}
			else if (i == 2) {
				if (j == 0) {
					filLeftDown[0][0] = newBrush[i][j];
				}
				else if (j == 1) {
					filLeftDown[1][0] = newBrush[i][j];
				}
				else if (j == 2) {
					filLeftDown[4][1] = newBrush[i][j];
				}
				else if (j == 3) {
					filLeftDown[2][3] = newBrush[i][j];
				}
				else if (j == 4) {
					filLeftDown[2][4] = newBrush[i][j];
				}
				else if (j == 5) {
					filLeftDown[4][5] = newBrush[i][j];
				}
				else if (j == 6) {
					filLeftDown[5][5] = newBrush[i][j];
				}
			}
			else if (i == 3) {
				if (j == 1) {
					filLeftDown[0][1] = newBrush[i][j];
				}
				if (j == 2) {
					filLeftDown[3][1] = newBrush[i][j];
				}
				else if (j == 3) {
					filLeftDown[3][2] = newBrush[i][j];
				}
				else if (j == 4) {
					filLeftDown[1][4] = newBrush[i][j];
				}
				else if (j == 5) {
					filLeftDown[3][5] = newBrush[i][j];
				}
			}
			else if (i == 4) {
				if (j == 2) {
					filLeftDown[2][1] = newBrush[i][j];
				}
				else if (j == 3) {
					filLeftDown[2][2] = newBrush[i][j];
				}
				else if (j == 4) {
					filLeftDown[1][3] = newBrush[i][j];
				}
			}
		}
	}
	boolfilRightDown = false;
	boolfilLeftDown = true;
	boolFilLeft = false;
	boolFilUp = false;
}

void fillfilUpFromLeftDown() {
	for (int i = 0; i < newBrushWidth; i++) {
		for (int j = 0; j < newBrushHeight; j++) {
			if (i == 0) {
				if (j == 2) {
					newBrush[i][j] = filLeftDown[1][2];
				}
				else if (j == 3) {
					newBrush[i][j] = filLeftDown[4][3];
				}
				else if (j == 4) {
					newBrush[i][j] = filLeftDown[4][4];
				}
			}
			else if (i == 1) {
				if (j == 1) {
					newBrush[i][j] = filLeftDown[1][1];
				}
				else if (j == 2) {
					newBrush[i][j] = filLeftDown[4][2];
				}
				else if (j == 3) {
					newBrush[i][j] = filLeftDown[3][3];
				}
				else if (j == 4) {
					newBrush[i][j] = filLeftDown[3][4];
				}
				else if (j == 5) {
					newBrush[i][j] = filLeftDown[5][4];
				}
			}
			else if (i == 2) {
				if (j == 0) {
					newBrush[i][j] = filLeftDown[0][0];
				}
				else if (j == 1) {
					newBrush[i][j] = filLeftDown[1][0];
				}
				else if (j == 2) {
					newBrush[i][j] = filLeftDown[4][1];
				}
				else if (j == 3) {
					newBrush[i][j] = filLeftDown[2][3];
				}
				else if (j == 4) {
					newBrush[i][j] = filLeftDown[2][4];
				}
				else if (j == 5) {
					newBrush[i][j] = filLeftDown[4][5];
				}
				else if (j == 6) {
					newBrush[i][j] = filLeftDown[5][5];
				}
			}
			else if (i == 3) {
				if (j == 1) {
					newBrush[i][j] = filLeftDown[0][1];
				}
				if (j == 2) {
					newBrush[i][j] = filLeftDown[3][1];
				}
				else if (j == 3) {
					newBrush[i][j] = filLeftDown[3][2];
				}
				else if (j == 4) {
					newBrush[i][j] = filLeftDown[1][4];
				}
				else if (j == 5) {
					newBrush[i][j] = filLeftDown[3][5];
				}
			}
			else if (i == 4) {
				if (j == 2) {
					newBrush[i][j] = filLeftDown[2][1];
				}
				else if (j == 3) {
					newBrush[i][j] = filLeftDown[2][2];
				}
				else if (j == 4) {
					newBrush[i][j] = filLeftDown[1][3];
				}
			}
		}
	}
	boolfilRightDown = false;
	boolfilLeftDown = false;
	boolFilLeft = false;
	boolFilUp = true;
}

void fillfilLeftFromLeftDown() {
	for (int j = 0; j < newBrushWidth; j++) {
		for (int i = 0; i < newBrushHeight; i++) {
			if (i == 0) {
				if (j == 2) {
					newBrush[j][i] = filLeftDown[1][2];
				}
				else if (j == 3) {
					newBrush[j][i] = filLeftDown[4][3];
				}
				else if (j == 4) {
					newBrush[j][i] = filLeftDown[4][4];
				}
			}
			else if (i == 1) {
				if (j == 1) {
					newBrush[j][i] = filLeftDown[1][1];
				}
				else if (j == 2) {
					newBrush[j][i] = filLeftDown[4][2];
				}
				else if (j == 3) {
					newBrush[j][i] = filLeftDown[3][3];
				}
				else if (j == 4) {
					newBrush[j][i] = filLeftDown[3][4];
				}
				else if (j == 5) {
					newBrush[j][i] = filLeftDown[5][4];
				}
			}
			else if (i == 2) {
				if (j == 0) {
					newBrush[j][i] = filLeftDown[0][0];
				}
				else if (j == 1) {
					newBrush[j][i] = filLeftDown[1][0];
				}
				else if (j == 2) {
					newBrush[j][i] = filLeftDown[4][1];
				}
				else if (j == 3) {
					newBrush[j][i] = filLeftDown[2][3];
				}
				else if (j == 4) {
					newBrush[j][i] = filLeftDown[2][4];
				}
				else if (j == 5) {
					newBrush[j][i] = filLeftDown[4][5];
				}
				else if (j == 6) {
					newBrush[j][i] = filLeftDown[5][5];
				}
			}
			else if (i == 3) {
				if (j == 1) {
					newBrush[j][i] = filLeftDown[0][1];
				}
				if (j == 2) {
					newBrush[j][i] = filLeftDown[3][1];
				}
				else if (j == 3) {
					newBrush[j][i] = filLeftDown[3][2];
				}
				else if (j == 4) {
					newBrush[j][i] = filLeftDown[1][4];
				}
				else if (j == 5) {
					newBrush[j][i] = filLeftDown[3][5];
				}
			}
			else if (i == 4) {
				if (j == 2) {
					newBrush[j][i] = filLeftDown[2][1];
				}
				else if (j == 3) {
					newBrush[j][i] = filLeftDown[2][2];
				}
				else if (j == 4) {
					newBrush[j][i] = filLeftDown[1][3];
				}
			}
		}
	}
	boolfilRightDown = false;
	boolfilLeftDown = false;
	boolFilLeft = true;
	boolFilUp = false;
}

void fillfilLeftFromRightDown() {
	for (int j = 0; j < newBrushWidth; j++) {
		for (int i = 0; i < newBrushHeight; i++) {
			if (i == 0) {
				if (j == 2) {
					newBrush[j][i] = filRightDown[4][2];
				}
				else if (j == 3) {
					newBrush[j][i] = filRightDown[1][3];
				}
				else if (j == 4) {
					newBrush[j][i] = filRightDown[1][4];
				}
			}
			else if (i == 1) {
				if (j == 1) {
					newBrush[j][i] = filRightDown[4][1];
				}
				else if (j == 2) {
					newBrush[j][i] = filRightDown[1][2];
				}
				else if (j == 3) {
					newBrush[j][i] = filRightDown[2][3];
				}
				else if (j == 4) {
					newBrush[j][i] = filRightDown[2][4];
				}
				else if (j == 5) {
					newBrush[j][i] = filRightDown[0][4];
				}
			}
			else if (i == 2) {
				if (j == 0) {
					newBrush[j][i] = filRightDown[5][0];
				}
				else if (j == 1) {
					newBrush[j][i] = filRightDown[4][0];
				}
				else if (j == 2) {
					newBrush[j][i] = filRightDown[1][1];
				}
				else if (j == 3) {
					newBrush[j][i] = filRightDown[3][3];
				}
				else if (j == 4) {
					newBrush[j][i] = filRightDown[3][4];
				}
				else if (j == 5) {
					newBrush[j][i] = filRightDown[1][5];
				}
				else if (j == 6) {
					newBrush[j][i] = filRightDown[0][5];
				}
			}
			else if (i == 3) {
				if (j == 1) {
					newBrush[j][i] = filRightDown[5][1];
				}
				if (j == 2) {
					newBrush[j][i] = filRightDown[2][1];
				}
				else if (j == 3) {
					newBrush[j][i] = filRightDown[2][2];
				}
				else if (j == 4) {
					newBrush[j][i] = filRightDown[4][4];
				}
				else if (j == 5) {
					newBrush[j][i] = filRightDown[2][5];
				}
			}
			else if (i == 4) {
				if (j == 2) {
					newBrush[j][i] = filRightDown[3][1];
				}
				else if (j == 3) {
					newBrush[j][i] = filRightDown[3][2];
				}
				else if (j == 4) {
					newBrush[j][i] = filRightDown[4][3];
				}
			}
		}
	}
	boolfilRightDown = false;
	boolfilLeftDown = false;
	boolFilLeft = true;
	boolFilUp = false;
}

void fillfilUpFromLeft() {
	for (int i = 0; i < newBrushWidth; i++) {
		for (int j = 0; j < newBrushHeight; j++) {
			filUp[j][i] = newBrush[i][j];
		}
	}
	boolfilRightDown = false;
	boolfilLeftDown = false;
	boolFilLeft = false;
	boolFilUp = true;
}

void fillfilRightDownFromLeft() {
	for (int j = 0; j < newBrushWidth; j++) {
		for (int i = 0; i < newBrushHeight; i++) {
			if (i == 0) {
				if (j == 2) {
					filRightDown[4][2] = newBrush[j][i];
				}
				else if (j == 3) {
					filRightDown[1][3] = newBrush[j][i];
				}
				else if (j == 4) {
					filRightDown[1][4] = newBrush[j][i];
				}
			}
			else if (i == 1) {
				if (j == 1) {
					filRightDown[4][1] = newBrush[j][i];
				}
				else if (j == 2) {
					filRightDown[1][2] = newBrush[j][i];
				}
				else if (j == 3) {
					filRightDown[2][3] = newBrush[j][i];
				}
				else if (j == 4) {
					filRightDown[2][4] = newBrush[j][i];
				}
				else if (j == 5) {
					filRightDown[0][4] = newBrush[j][i];
				}
			}
			else if (i == 2) {
				if (j == 0) {
					filRightDown[5][0] = newBrush[j][i];
				}
				else if (j == 1) {
					filRightDown[4][0] = newBrush[j][i];
				}
				else if (j == 2) {
					filRightDown[1][1] = newBrush[j][i];
				}
				else if (j == 3) {
					filRightDown[3][3] = newBrush[j][i];
				}
				else if (j == 4) {
					filRightDown[3][4] = newBrush[j][i];
				}
				else if (j == 5) {
					filRightDown[1][5] = newBrush[j][i];
				}
				else if (j == 6) {
					filRightDown[0][5] = newBrush[j][i];
				}
			}
			else if (i == 3) {
				if (j == 1) {
					filRightDown[5][1] = newBrush[j][i];
				}
				if (j == 2) {
					filRightDown[2][1] = newBrush[j][i];
				}
				else if (j == 3) {
					filRightDown[2][2] = newBrush[j][i];
				}
				else if (j == 4) {
					filRightDown[4][4] = newBrush[j][i];
				}
				else if (j == 5) {
					filRightDown[2][5] = newBrush[j][i];
				}
			}
			else if (i == 4) {
				if (j == 2) {
					filRightDown[3][1] = newBrush[j][i];
				}
				else if (j == 3) {
					filRightDown[3][2] = newBrush[j][i];
				}
				else if (j == 4) {
					filRightDown[4][3] = newBrush[j][i];
				}
			}
		}
	}
	boolfilRightDown = true;
	boolfilLeftDown = false;
	boolFilLeft = false;
	boolFilUp = false;
}

void fillfilLeftDownFromLeft() {
	for (int j = 0; j < newBrushWidth; j++) {
		for (int i = 0; i < newBrushHeight; i++) {
			if (i == 0) {
				if (j == 2) {
					filLeftDown[1][2] = newBrush[j][i];
				}
				else if (j == 3) {
					filLeftDown[4][3] = newBrush[j][i];
				}
				else if (j == 4) {
					filLeftDown[4][4] = newBrush[j][i];
				}
			}
			else if (i == 1) {
				if (j == 1) {
					filLeftDown[1][1] = newBrush[j][i];
				}
				else if (j == 2) {
					filLeftDown[4][2] = newBrush[j][i];
				}
				else if (j == 3) {
					filLeftDown[3][3] = newBrush[j][i];
				}
				else if (j == 4) {
					filLeftDown[3][4] = newBrush[j][i];
				}
				else if (j == 5) {
					filLeftDown[5][4] = newBrush[j][i];
				}
			}
			else if (i == 2) {
				if (j == 0) {
					filLeftDown[0][0] = newBrush[j][i];
				}
				else if (j == 1) {
					filLeftDown[1][0] = newBrush[j][i];
				}
				else if (j == 2) {
					filLeftDown[4][1] = newBrush[j][i];
				}
				else if (j == 3) {
					filLeftDown[2][3] = newBrush[j][i];
				}
				else if (j == 4) {
					filLeftDown[2][4] = newBrush[j][i];
				}
				else if (j == 5) {
					filLeftDown[4][5] = newBrush[j][i];
				}
				else if (j == 6) {
					filLeftDown[5][5] = newBrush[j][i];
				}
			}
			else if (i == 3) {
				if (j == 1) {
					filLeftDown[0][1] = newBrush[j][i];
				}
				if (j == 2) {
					filLeftDown[3][1] = newBrush[j][i];
				}
				else if (j == 3) {
					filLeftDown[3][2] = newBrush[j][i];
				}
				else if (j == 4) {
					filLeftDown[1][4] = newBrush[j][i];
				}
				else if (j == 5) {
					filLeftDown[3][5] = newBrush[j][i];
				}
			}
			else if (i == 4) {
				if (j == 2) {
					filLeftDown[2][1] = newBrush[j][i];
				}
				else if (j == 3) {
					filLeftDown[2][2] = newBrush[j][i];
				}
				else if (j == 4) {
					filLeftDown[1][3] = newBrush[j][i];
				}
			}
		}
	}
	boolfilRightDown = false;
	boolfilLeftDown = true;
	boolFilLeft = false;
	boolFilUp = false;
}

void fillfilLeftDownFromRightDown() {
	for (int i = newBrushWidth - 1, k = 0; i >= 0; i--, k++) {
		for (int j = 0; j < newBrushHeight; j++) {
			filLeftDown[i][j] = newBrush[k][j];
		}
	}
	boolfilRightDown = false;
	boolfilLeftDown = true;
	boolFilLeft = false;
	boolFilUp = false;
}

void fillfilRightDownFromLeftDown() {
	for (int i = newBrushWidth - 1, k = 0; i >= 0; i--, k++) {
		for (int j = 0; j < newBrushHeight; j++) {
			filRightDown[k][j] = newBrush[i][j];
		}
	}
	boolfilRightDown = true;
	boolfilLeftDown = false;
	boolFilLeft = false;
	boolFilUp = false;
}


//correctt
void fillflaUpFromRightUp() {
	for (int i = 0; i < newBrushHeight; i++) {
		for (int j = 0; j < newBrushWidth; j++) {
			if (i == 0) {
				if (j == 3) {
					newBrush[j][i] = flaRightUp[3][1];
				}
				else if (j == 4) {
					newBrush[j][i] = flaRightUp[4][2];
				}
				else if (j == 5) {
					newBrush[j][i] = flaRightUp[5][3];
				}
			}
			else if (i == 1) {
				if (j == 1) {
					newBrush[j][i] = flaRightUp[1][0];
				}
				else if (j == 2) {
					newBrush[j][i] = flaRightUp[2][0];
				}
				else if (j == 3) {
					newBrush[j][i] = flaRightUp[3][2];
				}
				else if (j == 4) {
					newBrush[j][i] = flaRightUp[4][3];
				}
				else if (j == 5) {
					newBrush[j][i] = flaRightUp[5][4];
				}
				else if (j == 6) {
					newBrush[j][i] = flaRightUp[6][4];
				}
				else if (j == 7) {
					newBrush[j][i] = flaRightUp[6][5];
				}
			}
			else if (i == 2) {
				if (j == 0) {
					newBrush[j][i] = flaRightUp[0][0];
				}
				else if (j == 1) {
					newBrush[j][i] = flaRightUp[1][1];
				}
				else if (j == 2) {
					newBrush[j][i] = flaRightUp[1][2];
				}
				else if (j == 3) {
					newBrush[j][i] = flaRightUp[2][2];
				}
				else if (j == 4) {
					newBrush[j][i] = flaRightUp[3][3];
				}
				else if (j == 5) {
					newBrush[j][i] = flaRightUp[4][4];
				}
				else if (j == 6) {
					newBrush[j][i] = flaRightUp[2][1];
				}
				else if (j == 7) {
					newBrush[j][i] = flaRightUp[5][5];
				}
				else if (j == 8) {
					newBrush[j][i] = flaRightUp[6][6];
				}
			}
			else if (i == 3) {
				if (j == 1) {
					newBrush[j][i] = flaRightUp[0][1];
				}
				else if (j == 2) {
					newBrush[j][i] = flaRightUp[0][2];
				}
				else if (j == 3) {
					newBrush[j][i] = flaRightUp[2][3];
				}
				else if (j == 4) {
					newBrush[j][i] = flaRightUp[3][4];
				}
				else if (j == 5) {
					newBrush[j][i] = flaRightUp[4][5];
				}
				else if (j == 6) {
					newBrush[j][i] = flaRightUp[4][6];
				}
				else if (j == 7) {
					newBrush[j][i] = flaRightUp[5][6];
				}
			}
			else if (i == 4) {
				if (j == 3) {
					newBrush[j][i] = flaRightUp[1][3];
				}
				else if (j == 4) {
					newBrush[j][i] = flaRightUp[2][4];
				}
				else if (j == 5) {
					newBrush[j][i] = flaRightUp[3][5];
				}
			}
		}
	}
	boolflaRightUp = false;
	boolflaLeftUp = false;
	boolFlaLeft = false;
	boolFlaUp = true;
}

//correctt
void fillflaRightUpFromUp() {
	for (int i = 0; i < newBrushHeight; i++) {
		for (int j = 0; j < newBrushWidth; j++) {
			if (i == 0) {
				if (j == 3) {
					flaRightUp[3][1] = newBrush[j][i];
				}
				else if (j == 4) {
					flaRightUp[4][2] = newBrush[j][i];
				}
				else if (j == 5) {
					flaRightUp[5][3] = newBrush[j][i];
				}
			}
			else if (i == 1) {
				if (j == 1) {
					flaRightUp[1][0] = newBrush[j][i];
				}
				else if (j == 2) {
					flaRightUp[2][0] = newBrush[j][i];
				}
				else if (j == 3) {
					flaRightUp[3][2] = newBrush[j][i];
				}
				else if (j == 4) {
					flaRightUp[4][3] = newBrush[j][i];
				}
				else if (j == 5) {
					flaRightUp[5][4] = newBrush[j][i];
				}
				else if (j == 6) {
					flaRightUp[6][4] = newBrush[j][i];
				}
				else if (j == 7) {
					flaRightUp[6][5] = newBrush[j][i];
				}
			}
			else if (i == 2) {
				if (j == 0) {
					flaRightUp[0][0] = newBrush[j][i];
				}
				else if (j == 1) {
					flaRightUp[1][1] = newBrush[j][i];
				}
				else if (j == 2) {
					flaRightUp[1][2] = newBrush[j][i];
				}
				else if (j == 3) {
					flaRightUp[2][2] = newBrush[j][i];
				}
				else if (j == 4) {
					flaRightUp[3][3] = newBrush[j][i];
				}
				else if (j == 5) {
					flaRightUp[4][4] = newBrush[j][i];
				}
				else if (j == 6) {
					flaRightUp[2][1] = newBrush[j][i];
				}
				else if (j == 7) {
					flaRightUp[5][5] = newBrush[j][i];
				}
				else if (j == 8) {
					flaRightUp[6][6] = newBrush[j][i];
				}
			}
			else if (i == 3) {
				if (j == 1) {
					flaRightUp[0][1] = newBrush[j][i];
				}
				else if (j == 2) {
					flaRightUp[0][2] = newBrush[j][i];
				}
				else if (j == 3) {
					flaRightUp[2][3] = newBrush[j][i];
				}
				else if (j == 4) {
					flaRightUp[3][4] = newBrush[j][i];
				}
				else if (j == 5) {
					flaRightUp[4][5] = newBrush[j][i];
				}
				else if (j == 6) {
					flaRightUp[4][6] = newBrush[j][i];
				}
				else if (j == 7) {
					flaRightUp[5][6] = newBrush[j][i];
				}
			}
			else if (i == 4) {
				if (j == 3) {
					flaRightUp[1][3] = newBrush[j][i];
				}
				else if (j == 4) {
					flaRightUp[2][4] = newBrush[j][i];
				}
				else if (j == 5) {
					flaRightUp[3][5] = newBrush[j][i];
				}
			}
		}
	}
	boolflaRightUp = true;
	boolflaLeftUp = false;
	boolFlaLeft = false;
	boolFlaUp = false;
}


//correctt
void fillflaLeftFromUp() {
	for (int i = 0; i < newBrushWidth; i++) {
		for (int j = 0; j < newBrushHeight; j++) {
			flaLeft[j][i] = newBrush[i][j];
		}
	}
	boolflaRightUp = false;
	boolflaLeftUp = false;
	boolFlaLeft = true;
	boolFlaUp = false;
}


//correctt
void fillflaLeftUpFromUp() {
	for (int i = 0; i < newBrushHeight; i++) {
		for (int j = 0; j < newBrushWidth; j++) {
			if (i == 0) {
				if (j == 3) {
					flaLeftUp[3][1] = newBrush[j][i];
				}
				else if (j == 4) {
					flaLeftUp[2][2] = newBrush[j][i];
				}
				else if (j == 5) {
					flaLeftUp[1][3] = newBrush[j][i];
				}
			}
			else if (i == 1) {
				if (j == 1) {
					flaLeftUp[5][0] = newBrush[j][i];
				}
				else if (j == 2) {
					flaLeftUp[4][0] = newBrush[j][i];
				}
				else if (j == 3) {
					flaLeftUp[3][2] = newBrush[j][i];
				}
				else if (j == 4) {
					flaLeftUp[2][3] = newBrush[j][i];
				}
				else if (j == 5) {
					flaLeftUp[1][4] = newBrush[j][i];
				}
				else if (j == 6) {
					flaLeftUp[0][4] = newBrush[j][i];
				}
				else if (j == 7) {
					flaLeftUp[0][5] = newBrush[j][i];
				}
			}
			else if (i == 2) {
				if (j == 0) {
					flaLeftUp[6][0] = newBrush[j][i];
				}
				else if (j == 1) {
					flaLeftUp[5][1] = newBrush[j][i];
				}
				else if (j == 2) {
					flaLeftUp[5][2] = newBrush[j][i];
				}
				else if (j == 3) {
					flaLeftUp[4][2] = newBrush[j][i];
				}
				else if (j == 4) {
					flaLeftUp[3][3] = newBrush[j][i];
				}
				else if (j == 5) {
					flaLeftUp[2][4] = newBrush[j][i];
				}
				else if (j == 6) {
					flaLeftUp[4][1] = newBrush[j][i];
				}
				else if (j == 7) {
					flaLeftUp[1][5] = newBrush[j][i];
				}
				else if (j == 8) {
					flaLeftUp[0][6] = newBrush[j][i];
				}
			}
			else if (i == 3) {
				if (j == 1) {
					flaLeftUp[6][1] = newBrush[j][i];
				}
				else if (j == 2) {
					flaLeftUp[6][2] = newBrush[j][i];
				}
				else if (j == 3) {
					flaLeftUp[4][3] = newBrush[j][i];
				}
				else if (j == 4) {
					flaLeftUp[3][4] = newBrush[j][i];
				}
				else if (j == 5) {
					flaLeftUp[2][5] = newBrush[j][i];
				}
				else if (j == 6) {
					flaLeftUp[2][6] = newBrush[j][i];
				}
				else if (j == 7) {
					flaLeftUp[1][6] = newBrush[j][i];
				}
			}
			else if (i == 4) {
				if (j == 3) {
					flaLeftUp[5][3] = newBrush[j][i];
				}
				else if (j == 4) {
					flaLeftUp[4][4] = newBrush[j][i];
				}
				else if (j == 5) {
					flaLeftUp[3][5] = newBrush[j][i];
				}
			}
		}
	}
	boolflaRightUp = false;
	boolflaLeftUp = true;
	boolFlaLeft = false;
	boolFlaUp = false;
}

//correctt
void fillflaUpFromLeftUp() {
	for (int i = 0; i < newBrushHeight; i++) {
		for (int j = 0; j < newBrushWidth; j++) {
			if (i == 0) {
				if (j == 3) {
					newBrush[j][i] = flaLeftUp[3][1];
				}
				else if (j == 4) {
					newBrush[j][i] = flaLeftUp[2][2];
				}
				else if (j == 5) {
					newBrush[j][i] = flaLeftUp[1][3];
				}
			}
			else if (i == 1) {
				if (j == 1) {
					newBrush[j][i] = flaLeftUp[5][0];
				}
				else if (j == 2) {
					newBrush[j][i] = flaLeftUp[4][0];
				}
				else if (j == 3) {
					newBrush[j][i] = flaLeftUp[3][2];
				}
				else if (j == 4) {
					newBrush[j][i] = flaLeftUp[2][3];
				}
				else if (j == 5) {
					newBrush[j][i] = flaLeftUp[1][4];
				}
				else if (j == 6) {
					newBrush[j][i] = flaLeftUp[0][4];
				}
				else if (j == 7) {
					newBrush[j][i] = flaLeftUp[0][5];
				}
			}
			else if (i == 2) {
				if (j == 0) {
					newBrush[j][i] = flaLeftUp[6][0];
				}
				else if (j == 1) {
					newBrush[j][i] = flaLeftUp[5][1];
				}
				else if (j == 2) {
					newBrush[j][i] = flaLeftUp[5][2];
				}
				else if (j == 3) {
					newBrush[j][i] = flaLeftUp[4][2];
				}
				else if (j == 4) {
					newBrush[j][i] = flaLeftUp[3][3];
				}
				else if (j == 5) {
					newBrush[j][i] = flaLeftUp[2][4];
				}
				else if (j == 6) {
					newBrush[j][i] = flaLeftUp[4][1];
				}
				else if (j == 7) {
					newBrush[j][i] = flaLeftUp[1][5];
				}
				else if (j == 8) {
					newBrush[j][i] = flaLeftUp[0][6];
				}
			}
			else if (i == 3) {
				if (j == 1) {
					newBrush[j][i] = flaLeftUp[6][1];
				}
				else if (j == 2) {
					newBrush[j][i] = flaLeftUp[6][2];
				}
				else if (j == 3) {
					newBrush[j][i] = flaLeftUp[4][3];
				}
				else if (j == 4) {
					newBrush[j][i] = flaLeftUp[3][4];
				}
				else if (j == 5) {
					newBrush[j][i] = flaLeftUp[2][5];
				}
				else if (j == 6) {
					newBrush[j][i] = flaLeftUp[2][6];
				}
				else if (j == 7) {
					newBrush[j][i] = flaLeftUp[1][6];
				}
			}
			else if (i == 4) {
				if (j == 3) {
					newBrush[j][i] = flaLeftUp[5][3];
				}
				else if (j == 4) {
					newBrush[j][i] = flaLeftUp[4][4];
				}
				else if (j == 5) {
					newBrush[j][i] = flaLeftUp[3][5];
				}
			}
		}
	}
	boolflaRightUp = false;
	boolflaLeftUp = false;
	boolFlaLeft = false;
	boolFlaUp = true;
}

//correctt
void fillflaLeftFromLeftUp() {
	for (int j = 0; j < newBrushHeight; j++) {
		for (int i = 0; i < newBrushWidth; i++) {
			if (i == 0) {
				if (j == 3) {
					newBrush[i][j] = flaLeftUp[3][1];
				}
				else if (j == 4) {
					newBrush[i][j] = flaLeftUp[2][2];
				}
				else if (j == 5) {
					newBrush[i][j] = flaLeftUp[1][3];
				}
			}
			else if (i == 1) {
				if (j == 1) {
					newBrush[i][j] = flaLeftUp[5][0];
				}
				else if (j == 2) {
					newBrush[i][j] = flaLeftUp[4][0];
				}
				else if (j == 3) {
					newBrush[i][j] = flaLeftUp[3][2];
				}
				else if (j == 4) {
					newBrush[i][j] = flaLeftUp[2][3];
				}
				else if (j == 5) {
					newBrush[i][j] = flaLeftUp[1][4];
				}
				else if (j == 6) {
					newBrush[i][j] = flaLeftUp[0][4];
				}
				else if (j == 7) {
					newBrush[i][j] = flaLeftUp[0][5];
				}
			}
			else if (i == 2) {
				if (j == 0) {
					newBrush[i][j] = flaLeftUp[6][0];
				}
				else if (j == 1) {
					newBrush[i][j] = flaLeftUp[5][1];
				}
				else if (j == 2) {
					newBrush[i][j] = flaLeftUp[5][2];
				}
				else if (j == 3) {
					newBrush[i][j] = flaLeftUp[4][2];
				}
				else if (j == 4) {
					newBrush[i][j] = flaLeftUp[3][3];
				}
				else if (j == 5) {
					newBrush[i][j] = flaLeftUp[2][4];
				}
				else if (j == 6) {
					newBrush[i][j] = flaLeftUp[4][1];
				}
				else if (j == 7) {
					newBrush[i][j] = flaLeftUp[1][5];
				}
				else if (j == 8) {
					newBrush[i][j] = flaLeftUp[0][6];
				}
			}
			else if (i == 3) {
				if (j == 1) {
					newBrush[i][j] = flaLeftUp[6][1];
				}
				else if (j == 2) {
					newBrush[i][j] = flaLeftUp[6][2];
				}
				else if (j == 3) {
					newBrush[i][j] = flaLeftUp[4][3];
				}
				else if (j == 4) {
					newBrush[i][j] = flaLeftUp[3][4];
				}
				else if (j == 5) {
					newBrush[i][j] = flaLeftUp[2][5];
				}
				else if (j == 6) {
					newBrush[i][j] = flaLeftUp[2][6];
				}
				else if (j == 7) {
					newBrush[i][j] = flaLeftUp[1][6];
				}
			}
			else if (i == 4) {
				if (j == 3) {
					newBrush[i][j] = flaLeftUp[5][3];
				}
				else if (j == 4) {
					newBrush[i][j] = flaLeftUp[4][4];
				}
				else if (j == 5) {
					newBrush[i][j] = flaLeftUp[3][5];
				}
			}
		}
	}
	boolflaRightUp = false;
	boolflaLeftUp = false;
	boolFlaLeft = true;
	boolFlaUp = false;
}

//correctt
void fillflaLeftFromRightUp() {
	for (int j = 0; j < newBrushHeight; j++) {
		for (int i = 0; i < newBrushWidth; i++) {
			if (i == 0) {
				if (j == 3) {
					newBrush[i][j] = flaRightUp[3][1];
				}
				else if (j == 4) {
					newBrush[i][j] = flaRightUp[4][2];
				}
				else if (j == 5) {
					newBrush[i][j] = flaRightUp[5][3];
				}
			}
			else if (i == 1) {
				if (j == 1) {
					newBrush[i][j] = flaRightUp[1][0];
				}
				else if (j == 2) {
					newBrush[i][j] = flaRightUp[2][0];
				}
				else if (j == 3) {
					newBrush[i][j] = flaRightUp[3][2];
				}
				else if (j == 4) {
					newBrush[i][j] = flaRightUp[4][3];
				}
				else if (j == 5) {
					newBrush[i][j] = flaRightUp[5][4];
				}
				else if (j == 6) {
					newBrush[i][j] = flaRightUp[6][4];
				}
				else if (j == 7) {
					newBrush[i][j] = flaRightUp[6][5];
				}
			}
			else if (i == 2) {
				if (j == 0) {
					newBrush[i][j] = flaRightUp[0][0];
				}
				else if (j == 1) {
					newBrush[i][j] = flaRightUp[1][1];
				}
				else if (j == 2) {
					newBrush[i][j] = flaRightUp[1][2];
				}
				else if (j == 3) {
					newBrush[i][j] = flaRightUp[2][2];
				}
				else if (j == 4) {
					newBrush[i][j] = flaRightUp[3][3];
				}
				else if (j == 5) {
					newBrush[i][j] = flaRightUp[4][4];
				}
				else if (j == 6) {
					newBrush[i][j] = flaRightUp[2][1];
				}
				else if (j == 7) {
					newBrush[i][j] = flaRightUp[5][5];
				}
				else if (j == 8) {
					newBrush[i][j] = flaRightUp[6][6];
				}
			}
			else if (i == 3) {
				if (j == 1) {
					newBrush[i][j] = flaRightUp[0][1];
				}
				else if (j == 2) {
					newBrush[i][j] = flaRightUp[0][2];
				}
				else if (j == 3) {
					newBrush[i][j] = flaRightUp[2][3];
				}
				else if (j == 4) {
					newBrush[i][j] = flaRightUp[3][4];
				}
				else if (j == 5) {
					newBrush[i][j] = flaRightUp[4][5];
				}
				else if (j == 6) {
					newBrush[i][j] = flaRightUp[4][6];
				}
				else if (j == 7) {
					newBrush[i][j] = flaRightUp[5][6];
				}
			}
			else if (i == 4) {
				if (j == 3) {
					newBrush[i][j] = flaRightUp[1][3];
				}
				else if (j == 4) {
					newBrush[i][j] = flaRightUp[2][4];
				}
				else if (j == 5) {
					newBrush[i][j] = flaRightUp[3][5];
				}
			}
		}
	}
	boolflaRightUp = false;
	boolflaLeftUp = false;
	boolFlaLeft = true;
	boolFlaUp = false;
}


//correctt
void fillflaRightUpFromLeft() {
	for (int j = 0; j < newBrushHeight; j++) {
		for (int i = 0; i < newBrushWidth; i++) {
			if (i == 0) {
				if (j == 3) {
					flaRightUp[3][1] = newBrush[i][j];
				}
				else if (j == 4) {
					flaRightUp[4][2] = newBrush[i][j];
				}
				else if (j == 5) {
					flaRightUp[5][3] = newBrush[i][j];
				}
			}
			else if (i == 1) {
				if (j == 1) {
					flaRightUp[1][0] = newBrush[i][j];
				}
				else if (j == 2) {
					flaRightUp[2][0] = newBrush[i][j];
				}
				else if (j == 3) {
					flaRightUp[3][2] = newBrush[i][j];
				}
				else if (j == 4) {
					flaRightUp[4][3] = newBrush[i][j];
				}
				else if (j == 5) {
					flaRightUp[5][4] = newBrush[i][j];
				}
				else if (j == 6) {
					flaRightUp[6][4] = newBrush[i][j];
				}
				else if (j == 7) {
					flaRightUp[6][5] = newBrush[i][j];
				}
			}
			else if (i == 2) {
				if (j == 0) {
					flaRightUp[0][0] = newBrush[i][j];
				}
				else if (j == 1) {
					flaRightUp[1][1] = newBrush[i][j];
				}
				else if (j == 2) {
					flaRightUp[1][2] = newBrush[i][j];
				}
				else if (j == 3) {
					flaRightUp[2][2] = newBrush[i][j];
				}
				else if (j == 4) {
					flaRightUp[3][3] = newBrush[i][j];
				}
				else if (j == 5) {
					flaRightUp[4][4] = newBrush[i][j];
				}
				else if (j == 6) {
					flaRightUp[2][1] = newBrush[i][j];
				}
				else if (j == 7) {
					flaRightUp[5][5] = newBrush[i][j];
				}
				else if (j == 8) {
					flaRightUp[6][6] = newBrush[i][j];
				}
			}
			else if (i == 3) {
				if (j == 1) {
					flaRightUp[0][1] = newBrush[i][j];
				}
				else if (j == 2) {
					flaRightUp[0][2] = newBrush[i][j];
				}
				else if (j == 3) {
					flaRightUp[2][3] = newBrush[i][j];
				}
				else if (j == 4) {
					flaRightUp[3][4] = newBrush[i][j];
				}
				else if (j == 5) {
					flaRightUp[4][5] = newBrush[i][j];
				}
				else if (j == 6) {
					flaRightUp[4][6] = newBrush[i][j];
				}
				else if (j == 7) {
					flaRightUp[5][6] = newBrush[i][j];
				}
			}
			else if (i == 4) {
				if (j == 3) {
					flaRightUp[1][3] = newBrush[i][j];
				}
				else if (j == 4) {
					flaRightUp[2][4] = newBrush[i][j];
				}
				else if (j == 5) {
					flaRightUp[3][5] = newBrush[i][j];
				}
			}
		}
	}
	boolflaRightUp = true;
	boolflaLeftUp = false;
	boolFlaLeft = false;
	boolFlaUp = false;
}

//correctt
void fillflaUpFromLeft() {
	for (int i = 0; i < newBrushWidth; i++) {
		for (int j = 0; j < newBrushHeight; j++) {
			flaUp[j][i] = newBrush[i][j];
		}
	}
	boolflaRightUp = false;
	boolflaLeftUp = false;
	boolFlaLeft = false;
	boolFlaUp = true;
}

//correctt
void fillflaLeftUpFromLeft() {
	for (int j = 0; j < newBrushHeight; j++) {
		for (int i = 0; i < newBrushWidth; i++) {
			if (i == 0) {
				if (j == 3) {
					flaLeftUp[3][1] = newBrush[i][j];
				}
				else if (j == 4) {
					flaLeftUp[2][2] = newBrush[i][j];
				}
				else if (j == 5) {
					flaLeftUp[1][3] = newBrush[i][j];
				}
			}
			else if (i == 1) {
				if (j == 1) {
					flaLeftUp[5][0] = newBrush[i][j];
				}
				else if (j == 2) {
					flaLeftUp[4][0] = newBrush[i][j];
				}
				else if (j == 3) {
					flaLeftUp[3][2] = newBrush[i][j];
				}
				else if (j == 4) {
					flaLeftUp[2][3] = newBrush[i][j];
				}
				else if (j == 5) {
					flaLeftUp[1][4] = newBrush[i][j];
				}
				else if (j == 6) {
					flaLeftUp[0][4] = newBrush[i][j];
				}
				else if (j == 7) {
					flaLeftUp[0][5] = newBrush[i][j];
				}
			}
			else if (i == 2) {
				if (j == 0) {
					flaLeftUp[6][0] = newBrush[i][j];
				}
				else if (j == 1) {
					flaLeftUp[5][1] = newBrush[i][j];
				}
				else if (j == 2) {
					flaLeftUp[5][2] = newBrush[i][j];
				}
				else if (j == 3) {
					flaLeftUp[4][2] = newBrush[i][j];
				}
				else if (j == 4) {
					flaLeftUp[3][3] = newBrush[i][j];
				}
				else if (j == 5) {
					flaLeftUp[2][4] = newBrush[i][j];
				}
				else if (j == 6) {
					flaLeftUp[4][1] = newBrush[i][j];
				}
				else if (j == 7) {
					flaLeftUp[1][5] = newBrush[i][j];
				}
				else if (j == 8) {
					flaLeftUp[0][6] = newBrush[i][j];
				}
			}
			else if (i == 3) {
				if (j == 1) {
					flaLeftUp[6][1] = newBrush[i][j];
				}
				else if (j == 2) {
					flaLeftUp[6][2] = newBrush[i][j];
				}
				else if (j == 3) {
					flaLeftUp[4][3] = newBrush[i][j];
				}
				else if (j == 4) {
					flaLeftUp[3][4] = newBrush[i][j];
				}
				else if (j == 5) {
					flaLeftUp[2][5] = newBrush[i][j];
				}
				else if (j == 6) {
					flaLeftUp[2][6] = newBrush[i][j];
				}
				else if (j == 7) {
					flaLeftUp[1][6] = newBrush[i][j];
				}
			}
			else if (i == 4) {
				if (j == 3) {
					flaLeftUp[5][3] = newBrush[i][j];
				}
				else if (j == 4) {
					flaLeftUp[4][4] = newBrush[i][j];
				}
				else if (j == 5) {
					flaLeftUp[3][5] = newBrush[i][j];
				}
			}
		}
	}
	boolflaRightUp = false;
	boolflaLeftUp = true;
	boolFlaLeft = false;
	boolFlaUp = false;
}

void fillflaLeftUpFromRightUp() {
	for (int i = newBrushWidth - 1, k = 0; i >= 0; i--, k++) {
		for (int j = 0; j < newBrushHeight; j++) {
			flaLeftUp[k][j] = newBrush[i][j];
		}
	}
	boolflaRightUp = false;
	boolflaLeftUp = true;
	boolFlaLeft = false;
	boolFlaUp = false;
}

void fillflaRightUpFromLeftUp() {
	for (int i = newBrushWidth - 1, k = 0; i >= 0; i--, k++) {
		for (int j = 0; j < newBrushHeight; j++) {
			flaRightUp[i][j] = newBrush[k][j];
		}
	}
	boolflaRightUp = true;
	boolflaLeftUp = false;
	boolFlaLeft = false;
	boolFlaUp = false;
}

void polygonFlat() {
	polygon2[0].x = polygon[0].x;
	polygon2[0].y = polygon[0].y;
	polygon2[1].x = polygon[2].x;
	polygon2[1].y = polygon[2].y;
	double d = sqrt(pow(currXMouse - polygon2[1].x, 2) + pow(polygon2[0].y - polygon2[1].y, 2));
	double a = (6 * 6 - 40 * 40 + d * d) / (2 * d);
	double h = sqrt(6 * 6 - a * a);
	double nx = currXMouse + a * ((polygon2[1].x - currXMouse) / d);
	double ny = polygon2[0].y + a * ((polygon2[1].y - polygon2[0].y) / d);
	polygon2[2].x = nx + h * ((polygon2[1].y - polygon2[0].y) / d);
	polygon2[2].y = ny - h * ((polygon2[1].x - currXMouse) / d);
	polygon3[0].x = polygon[0].x;
	polygon3[0].y = polygon[0].y;
	polygon3[1].x = polygon[3].x;
	polygon3[1].y = polygon[3].y;
	d = sqrt(pow(currXMouse - polygon3[1].x, 2) + pow(polygon3[0].y - polygon3[1].y, 2));
	a = (6 * 6 - 40 * 40 + d * d) / (2 * d);
	h = sqrt(6 * 6 - a * a);
	nx = currXMouse + a * ((polygon3[1].x - currXMouse) / d);
	ny = polygon3[0].y + a * ((polygon3[1].y - polygon3[0].y) / d);
	polygon3[2].x = nx - h * ((polygon3[1].y - polygon3[0].y) / d);
	polygon3[2].y = ny + h * ((polygon3[1].x - currXMouse) / d);
}

void polygonFil() {
	polygon2[0].x = polygon[0].x;
	polygon2[0].y = polygon[0].y;
	polygon2[1].x = polygon[2].x;
	polygon2[1].y = polygon[2].y;
	double d = sqrt(pow(currXMouse - polygon2[1].x, 2) + pow(polygon2[0].y - polygon2[1].y, 2));
	double a = (4 * 4 - 40 * 40 + d * d) / (2 * d);
	double h = sqrt(4 * 4 - a * a);
	double nx = currXMouse + a * ((polygon2[1].x - currXMouse) / d);
	double ny = polygon2[0].y + a * ((polygon2[1].y - polygon2[0].y) / d);
	polygon2[2].x = nx + h * ((polygon2[1].y - polygon2[0].y) / d);
	polygon2[2].y = ny - h * ((polygon2[1].x - currXMouse) / d);
	polygon3[0].x = polygon[0].x;
	polygon3[0].y = polygon[0].y;
	polygon3[1].x = polygon[3].x;
	polygon3[1].y = polygon[3].y;
	d = sqrt(pow(currXMouse - polygon3[1].x, 2) + pow(polygon3[0].y - polygon3[1].y, 2));
	a = (4 * 4 - 40 * 40 + d * d) / (2 * d);
	h = sqrt(4 * 4 - a * a);
	nx = currXMouse + a * ((polygon3[1].x - currXMouse) / d);
	ny = polygon3[0].y + a * ((polygon3[1].y - polygon3[0].y) / d);
	polygon3[2].x = nx - h * ((polygon3[1].y - polygon3[0].y) / d);
	polygon3[2].y = ny + h * ((polygon3[1].x - currXMouse) / d);
}

void polygonRound() {
	polygon2[0].x = polygon[0].x;
	polygon2[0].y = polygon[0].y;
	polygon2[1].x = polygon[2].x;
	polygon2[1].y = polygon[2].y;
	double d = sqrt(pow(currXMouse - polygon2[1].x, 2) + pow(polygon2[0].y - polygon2[1].y, 2));
	double a = (1.5 * 1.5 - 40 * 40 + d * d) / (2 * d);
	double h = sqrt(1.5 * 1.5 - a * a);
	double nx = currXMouse + a * ((polygon2[1].x - currXMouse) / d);
	double ny = polygon2[0].y + a * ((polygon2[1].y - polygon2[0].y) / d);
	polygon2[2].x = nx + h * ((polygon2[1].y - polygon2[0].y) / d);
	polygon2[2].y = ny - h * ((polygon2[1].x - currXMouse) / d);
	polygon3[0].x = polygon[0].x;
	polygon3[0].y = polygon[0].y;
	polygon3[1].x = polygon[3].x;
	polygon3[1].y = polygon[3].y;
	d = sqrt(pow(currXMouse - polygon3[1].x, 2) + pow(polygon3[0].y - polygon3[1].y, 2));
	a = (1.5 * 1.5 - 40 * 40 + d * d) / (2 * d);
	h = sqrt(1.5 * 1.5 - a * a);
	nx = currXMouse + a * ((polygon3[1].x - currXMouse) / d);
	ny = polygon3[0].y + a * ((polygon3[1].y - polygon3[0].y) / d);
	polygon3[2].x = nx - h * ((polygon3[1].y - polygon3[0].y) / d);
	polygon3[2].y = ny + h * ((polygon3[1].x - currXMouse) / d);
}

void polygonEraser() {
	polygon2[0].x = polygon[0].x;
	polygon2[0].y = polygon[0].y;
	polygon2[1] = polygon[2];
	double d = sqrt(pow(currXMouse - polygon2[1].x, 2) + pow(polygon2[0].y - polygon2[1].y, 2));
	double a = (4 * 4 - 40 * 40 + d * d) / (2 * d);
	double h = sqrt(4 * 4 - a * a);
	double nx = currXMouse + a * ((polygon2[1].x - currXMouse) / d);
	double ny = polygon2[0].y + a * ((polygon2[1].y - polygon2[0].y) / d);
	polygon2[2].x = nx + h * ((polygon2[1].y - polygon2[0].y) / d);
	polygon2[2].y = ny - h * ((polygon2[1].x - currXMouse) / d);
	polygon3[0].x = polygon[0].x;
	polygon3[0].y = polygon[0].y;
	polygon3[1] = polygon2[1];
	polygon3[2] = polygon2[2];
}

void decideBrush(bool isMenu) {
	int tempX = polygon[0].x;
	int tempY = polygon[0].y;
	polygon[0].x = currXMouse;
	polygon[0].y = winHeight - currYMouse;
	polygon[3] = pointFromEndOfLine(Point(tempX, tempY), polygon[0], -40);
	double d = sqrt(pow(currXMouse - polygon[3].x, 2) + pow(polygon[0].y - polygon[3].y, 2));
	double a = (40 * 40 - 4 * 4 + d * d) / (2 * d);
	double h = sqrt(40 * 40 - a * a);
	double nx = currXMouse + a * ((polygon[3].x - currXMouse) / d);
	double ny = polygon[0].y + a * ((polygon[3].y - polygon[0].y) / d);
	polygon[2].x = nx + h * ((polygon[3].y - polygon[0].y) / d);
	polygon[2].y = ny - h * ((polygon[3].x - currXMouse) / d);
	if (fil) {
		polygonFil();
	} 
	else if (flat) {
		polygonFlat();
	}
	else if (eraser) {
		polygonEraser();
	}
	else {
		polygonRound();
	}
	if (!isMenu) {
		if (fil) {
			if (polygon[3].y <= polygon[0].y + 20 && polygon[3].y >= polygon[0].y - 20 &&
				((polygon[3].x < polygon[0].x - 35) || (polygon[3].x > polygon[0].x + 35))) {
				//filLeft
				if (boolfilRightDown) {
					newBrushWidth = 7;
					newBrushHeight = 5;
					newBrush = filLeft;
					fillfilLeftFromRightDown();
				}
				else if (boolfilLeftDown) {
					newBrushWidth = 7;
					newBrushHeight = 5;
					newBrush = filLeft;
					fillfilLeftFromLeftDown();
				}
				else if (boolFilUp) {
					fillfilLeftFromUp();
					newBrushWidth = 7;
					newBrushHeight = 5;
					newBrush = filLeft;
				}
				centerX = 3;
				centerY = 2;
				//printf("LEFT\n");
			}
			else if (polygon[3].x <= polygon[0].x + 20 && polygon[3].x >= polygon[0].x - 20 &&
				((polygon[3].y < polygon[0].y - 35) || (polygon[3].y > polygon[0].y + 35))) {
				//filUp
				if (boolfilRightDown) {
					newBrushWidth = 5;
					newBrushHeight = 7;
					newBrush = filUp;
					fillfilUpFromRightDown();
				}
				else if (boolfilLeftDown) {
					newBrushWidth = 5;
					newBrushHeight = 7;
					newBrush = filUp;
					fillfilUpFromLeftDown();
				}
				else if (boolFilLeft) {
					fillfilUpFromLeft();
					newBrushWidth = 5;
					newBrushHeight = 7;
					newBrush = filUp;
				}
				centerX = 2;
				centerY = 3;
				//printf("UP\n");
			}
			else if ((polygon[3].x > polygon[0].x && polygon[3].y > polygon[0].y) ||
				(polygon[3].x < polygon[0].x && polygon[3].y < polygon[0].y)) {
				//filRightDown
				if (boolFilUp) {
					fillfilLeftDownFromUp();
					newBrushWidth = 6;
					newBrushHeight = 6;
					newBrush = filLeftDown;
					//printf("HERE");
				}
				else if (boolfilRightDown) {
					fillfilLeftDownFromRightDown();
					newBrushWidth = 6;
					newBrushHeight = 6;
					newBrush = filLeftDown;
				}
				else if (boolFilLeft) {
					fillfilLeftDownFromLeft();
					newBrushWidth = 6;
					newBrushHeight = 6;
					newBrush = filLeftDown;
				}
				centerX = 3;
				centerY = 2;
				//printf("\nLEFTUP\n");
			}
			else if ((polygon[3].x < polygon[0].x &&  polygon[3].y > polygon[0].y) ||
				(polygon[3].x > polygon[0].x &&  polygon[3].y < polygon[0].y)) {
				//filLeftDown
				if (boolFilUp) {
					fillfilRightDownFromUp();
					newBrushWidth = 6;
					newBrushHeight = 6;
					newBrush = filRightDown;
					//printf("HERE");
				}
				else if (boolfilLeftDown) {
					fillfilRightDownFromLeftDown();
					newBrushWidth = 6;
					newBrushHeight = 6;
					newBrush = filRightDown;
				}
				else if (boolFilLeft) {
					fillfilRightDownFromLeft();
					newBrushWidth = 6;
					newBrushHeight = 6;
					newBrush = filRightDown;
				}
				centerX = 3;
				centerY = 2;
				//printf("RIGHTUP\n");
			}
		}
		else if (flat) {
			if (polygon[3].y < polygon[0].y + 20 && polygon[3].y > polygon[0].y - 20 &&
				((polygon[3].x < polygon[0].x - 35) || (polygon[3].x > polygon[0].x + 35))) {
				//filLeft
				if (boolflaRightUp) {
					newBrushWidth = 5;
					newBrushHeight = 9;
					newBrush = flaLeft;
					fillflaLeftFromRightUp();
					//printf("RIGHTUP");
				}
				else if (boolflaLeftUp) {
					newBrushWidth = 5;
					newBrushHeight = 9;
					newBrush = flaLeft;
					fillflaLeftFromLeftUp();
					//printf("LEFTUP");
				}
				else if (boolFlaUp) {
					fillflaLeftFromUp();
					newBrushWidth = 5;
					newBrushHeight = 9;
					newBrush = flaLeft;
					//printf("UP");
				}
				centerX = 2;
				centerY = 4;
				//printf("flaLEFT\n");
			}
			else if (polygon[3].x < polygon[0].x + 20 && polygon[3].x > polygon[0].x - 20 &&
				((polygon[3].y < polygon[0].y - 35) || (polygon[3].y > polygon[0].y + 35))) {
				//filUp
				if (boolflaRightUp) {
					newBrushWidth = 9;
					newBrushHeight = 5;
					newBrush = flaUp;
					fillflaUpFromRightUp();
					//printf("RIGHTUP");
				}
				else if (boolflaLeftUp) {
					newBrushWidth = 9;
					newBrushHeight = 5;
					newBrush = flaUp;
					fillflaUpFromLeftUp();
					//printf("LEFTUP");
				}
				else if (boolFlaLeft) {
					fillflaUpFromLeft();
					newBrushWidth = 9;
					newBrushHeight = 5;
					newBrush = flaUp;
					//printf("UP");
				}
				centerX = 4;
				centerY = 2;
				//printf("flaUP\n");
			}
			else if ((polygon[3].x > polygon[0].x && polygon[3].y > polygon[0].y) ||
				(polygon[3].x < polygon[0].x && polygon[3].y < polygon[0].y)) {
				//filRightDown
				if (boolFlaUp) {
					fillflaLeftUpFromUp();
					newBrushWidth = 7;
					newBrushHeight = 7;
					newBrush = flaLeftUp;
					//printf("UP");
				}
				else if (boolflaRightUp) {
					fillflaLeftUpFromRightUp();
					newBrushWidth = 7;
					newBrushHeight = 7;
					newBrush = flaLeftUp;
					//printf("RIGHTUP");
				}
				else if (boolFlaLeft) {
					fillflaLeftUpFromLeft();
					newBrushWidth = 7;
					newBrushHeight = 7;
					newBrush = flaLeftUp;
					//printf("LEFT");
				}
				centerX = 3;
				centerY = 3;
				//printf("flaLEFTUP\n");
			}
			else if ((polygon[3].x < polygon[0].x &&  polygon[3].y > polygon[0].y) ||
				(polygon[3].x > polygon[0].x &&  polygon[3].y < polygon[0].y)) {
				//filLeftDown
				if (boolFlaUp) {
					fillflaRightUpFromUp();
					newBrushWidth = 7;
					newBrushHeight = 7;
					newBrush = flaRightUp;
					//printf("UP");
				}
				else if (boolflaLeftUp) {
					fillflaRightUpFromLeftUp();
					newBrushWidth = 7;
					newBrushHeight = 7;
					newBrush = flaRightUp;
					//printf("LEFTUP");
				}
				else if (boolFlaLeft) {
					fillflaRightUpFromLeft();
					newBrushWidth = 7;
					newBrushHeight = 7;
					newBrush = flaRightUp;
					//printf("LEFT");
				}
				centerX = 3;
				centerY = 3;
				//printf("flaRIGHTUP\n");
			}
			/*for (int i = newBrushHeight - 1; i >= 0; i--) {
				printf("\n");
				for (int j = 0; j < newBrushWidth; j++) {
					if (newBrush[j][i].s) {
						printf("1");
					}
					else {
						printf("0");
					}
				}
			}
			printf("\n");*/
		}
	}
	//printf("\n");
}

/**
* Reads mouse motion from the user.
*
* @param xMouse
*			The x coordinate of the mouse click event
*
* @param yMouse
*			The y coordinate of the mouse click event.
*/
void mouseMotion(GLint xMouse, GLint yMouse) {
	if (isDrawing) {
		prevXMouse = currXMouse;
		prevYMouse = currYMouse;
		currXMouse = xMouse;
		currYMouse = yMouse;
		decideBrush(false);
		theta = 0;
		if (currXMouse > barX) {
			isCanvas = false;
		}
		else {
			isCanvas = true;
		}
		glutPostRedisplay();
	}
}

/**
* Reads the passive mouse motion from the user.
*
* @param xMouse
*			The x coordinate of the mouse click event
*
* @param yMouse
*			The y coordinate of the mouse click event.
*/
void passiveMouseMotion(GLint xMouse, GLint yMouse) {
	prevXMouse = currXMouse;
	prevYMouse = currYMouse;
	currXMouse = xMouse;
	currYMouse = yMouse;
	//translate(currXMouse - prevXMouse, (prevYMouse - currYMouse));
	//transform();
	//setIdentity(composite);
	int tempX = polygon[0].x;
	int tempY = polygon[0].y;
	polygon[0].x = currXMouse;
	polygon2[0].x = currXMouse;
	polygon3[0].x = currXMouse;
	if (polygon[3].x < tempX) {
		polygon[3].x = polygon[0].x + (polygon[3].x - tempX);
	}
	else {
		polygon[3].x = polygon[0].x + abs(polygon[3].x - tempX);
	}
	if (polygon[2].x < tempX) {
		polygon[2].x = polygon[0].x + (polygon[2].x - tempX);
	} 
	else {
		polygon[2].x = polygon[0].x + abs(polygon[2].x - tempX);
	}
	if (polygon3[1].x < tempX) {
		polygon3[1].x = polygon[0].x + (polygon3[1].x - tempX);
	}
	else {
		polygon3[1].x = polygon[0].x + abs(polygon3[1].x - tempX);
	}
	if (polygon2[1].x < tempX) {
		polygon2[1].x = polygon[0].x + (polygon2[1].x - tempX);
	}
	else {
		polygon2[1].x = polygon[0].x + abs(polygon2[1].x - tempX);
	}
	polygon[0].y = winHeight - currYMouse;
	polygon2[0].y = winHeight - currYMouse;
	polygon3[0].y = winHeight - currYMouse;
	if (polygon[3].y < tempY) {
		polygon[3].y = polygon[0].y + (polygon[3].y - tempY);
	}
	else {
		polygon[3].y = polygon[0].y + abs(polygon[3].y - tempY);
	}
	if (polygon[2].y < tempY) {
		polygon[2].y = polygon[0].y + (polygon[2].y - tempY);
	}
	else {
		polygon[2].y = polygon[0].y + abs(polygon[2].y - tempY);
	}
	if (polygon3[1].y < tempY) {
		polygon3[1].y = polygon[0].y + (polygon3[1].y - tempY);
	}
	else {
		polygon3[1].y = polygon[0].y + abs(polygon3[1].y - tempY);
	}
	if (polygon2[1].y < tempY) {
		polygon2[1].y = polygon[0].y + (polygon2[1].y - tempY);
	}
	else {
		polygon2[1].y = polygon[0].y + abs(polygon2[1].y - tempY);
	}
	if (polygon2[2].x < tempX) {
		polygon2[2].x = polygon[0].x + (polygon2[2].x - tempX);
	}
	else {
		polygon2[2].x = polygon[0].x + abs(polygon2[2].x - tempX);
	}
	if (polygon2[2].y < tempY) {
		polygon2[2].y = polygon[0].y + (polygon2[2].y - tempY);
	}
	else {
		polygon2[2].y = polygon[0].y + abs(polygon2[2].y - tempY);
	}
	if (polygon3[2].x >= tempX) {
		polygon3[2].x = polygon[0].x + abs(polygon3[2].x - tempX);
	}
	else {
		polygon3[2].x = polygon[0].x + (polygon3[2].x - tempX);
	}
	if (polygon3[2].y >= tempY) {
		polygon3[2].y = polygon[0].y + abs(polygon3[2].y - tempY);
	}
	else {
		polygon3[2].y = polygon[0].y + (polygon3[2].y - tempY);
	}
	glutPostRedisplay();
}

/**
* Initializes the pixel vector.
*/
void initArray() {
	for (int x = 0; x < winWidth; x++) {
		pixels.push_back(std::vector<Pixel>());
		for (int y = 0; y < winHeight + 20; y++) {
			pixels[x].push_back(Pixel(-1, -1, -1));
		}
	}
}

/**
* The reshape function.
*
* @param newWidth
*			The new width of the screen.
*
* @param newHeight
*			The new height of the screen.
*/
void reshape(GLint newWidth, GLint newHeight) {
	/*glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, (GLdouble)newWidth, 0.0, (GLdouble)newHeight, -1.0, 1.0);
	glMatrixMode(GL_MODELVIEW);
	glViewport(0, 0, newWidth, newHeight);
	glClearColor(1.0, 1.0, 1.0, 0.0);
	glClear(GL_COLOR_BUFFER_BIT);
	winWidth = newWidth;
	winHeight = newHeight;
	if (isCanvas) {
		backwinWidth = newWidth;
		backwinHeight = newHeight;
	}
	glFlush();*/
	//glutReshapeWindow(winWidth, winHeight);
	glMatrixMode(GL_PROJECTION);
	glFrustum(-1.0, 1.0, -1.0, 1.0, 2.0, 20.0);
	//glMatrixMode(GL_MODELVIEW);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	gluLookAt(ox, oy, oz, sx, sy, sz, tx, ty, tz);
}

/**
* Initializes the light.
*/
void init() {
	GLfloat mat_ambient[] = { 0.5, 0.5, 0.5, 1.0 };
	GLfloat mat_specular[] = { 1.0, 1.0, 1.0, 1.0 };
	GLfloat mat_shininess[] = { 50.0 };
	GLfloat light_position[] = { 1.0, 1.0, 1.0, 0.0 };
	GLfloat model_ambient[] = { 0.5, 0.5, 0.5, 1.0 };
	glClearColor(1, 1, 1, 0);
	//glMatrixMode(GL_MODELVIEW);
	glMaterialfv(GL_FRONT, GL_AMBIENT, mat_ambient);
	glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, model_ambient);
	glLoadIdentity();
	glOrtho(0.0, (GLdouble)winWidth, 0.0, (GLdouble)winHeight, -1.0, 1.0);
	glMatrixMode(GL_MODELVIEW);
	glViewport(0, 0, winWidth, winHeight);
	glClear(GL_COLOR_BUFFER_BIT);
	glClearColor(1.0, 1.0, 1.0, 0.0);
	//glEnable(GL_LIGHTING);
	//glEnable(GL_LIGHT0);
	glEnable(GL_DEPTH_TEST);
}

/**
* Draws a pixel with the specified color.
*
* @param x
*			The x coordinate of the pixel.
*
* @param y
*			The y coordinate of the pixel.
*
* @param r
*			The red part.
*
* @param g
*			The green part.
*
* @param b
*			The blue part.
*/
void drawPixelPaletteWell(GLint x, GLint y, GLfloat r, GLfloat g, GLfloat b) {
	if (x >= 0 && y >= 0) {
		glBegin(GL_POINTS); //begins gl loop
		glColor3f(r, g, b);
		glVertex2i(x, winHeight - y);
		//printf("%d", pixels[0].size());
		pixels[x][winHeight - y].pred = r;
		pixels[x][winHeight - y].pgreen = g;
		pixels[x][winHeight - y].pblue = b;
		pixels[x][winHeight - y].pvolume = 255;
		glEnd(); // ends gl loop
	}
}

void loadData(OPENFILENAME ofn) {
	std::string file = readFile(ofn.lpstrFile);
	int selector = 0;
	Pixel newP = Pixel(-1, -1, -1);
	int x = -1;
	int y = -1;
	if (replacement) {
		for (int x = 0; x < winWidth; x++) {
			for (int y = 0; y < winHeight; y++) {
				pixels[x][y].red = -1;
				pixels[x][y].green = -1;
				pixels[x][y].blue = -1;
				pixels[x][y].dred = -1;
				pixels[x][y].dgreen = -1;
				pixels[x][y].dblue = -1;
				pixels[x][y].pred = -1;
				pixels[x][y].pgreen = -1;
				pixels[x][y].pblue = -1;
				pixels[x][y].volume = -1;
				pixels[x][y].pvolume = -1;
				pixels[x][y].alpha = -1;
				pixels[x][y].drawn = false;
				pixels[x][y].selected = true;
				pixels[x][y].isSelected = false;
				pixels[x][y].line = false;
				pixels[x][y].pline = false;
			}
		}
	}
	for (int i = 0; i < file.size();) {
		int s = file.find_first_of('\n', i);
		char* data = (char*)malloc(10);
		//printf("%d\t%d\n", i, file.size());
		if (s != -1) {
			for (int j = i, k = 0; j < s; j++, k++) {
				data[k] = file[j];
			}
		}
		if (selector == 0) {
			sscanf_s(data, "%d", &x);
			selector++;
		}
		else if (selector == 1) {
			sscanf_s(data, "%d", &y);
			selector++;
		}
		else if (selector == 2) {
			int temp = 0;
			sscanf_s(data, "%d", &temp);
			newP.red = temp / 255.0;
			
			selector++;
		}
		else if (selector == 3) {
			int temp = 0;
			sscanf_s(data, "%d", &temp);
			newP.green = temp / 255.0; 
			//printf("load: %f\n", newP.green);
			selector++;
		}
		else if (selector == 4) {
			int temp = 0;
			sscanf_s(data, "%d", &temp);
			newP.blue = temp / 255.0;
			selector++;
		}
		else if (selector == 5) {
			int temp = 0;
			sscanf_s(data, "%d", &temp);
			newP.dred = temp / 255.0;
			selector++;
		}
		else if (selector == 6) {
			int temp = 0;
			sscanf_s(data, "%d", &temp);
			newP.dgreen = temp / 255.0;
			selector++;
		}
		else if (selector == 7) {
			int temp = 0;
			sscanf_s(data, "%d", &temp);
			newP.dblue = temp / 255.0;
			selector++;
		}
		else if (selector == 8) {
			int temp = 0;
			sscanf_s(data, "%d", &temp);
			newP.pred = temp / 255.0;
			selector++;
		}
		else if (selector == 9) {
			int temp = 0;
			sscanf_s(data, "%d", &temp);
			newP.pgreen = temp / 255.0;
			selector++;
		}
		else if (selector == 10) {
			int temp = 0;
			sscanf_s(data, "%d", &temp);
			newP.pblue = temp / 255.0;
			selector++;
		}
		else if (selector == 11) {
			int temp = 0;
			sscanf_s(data, "%d", &temp);
			newP.volume = temp / 255.0;
			selector++;
		}
		else if (selector == 12) {
			int temp = 0;
			sscanf_s(data, "%d", &temp);
			newP.pvolume = temp / 255.0;
			selector++;
		}
		else if (selector == 13) {
			int temp = 0;
			sscanf_s(data, "%d", &temp);
			newP.alpha = temp / 255.0;
			selector = 0;
			pixels[x][y].red = newP.red;
			pixels[x][y].green = newP.green;
			pixels[x][y].blue = newP.blue;
			pixels[x][y].dred = newP.dred;
			pixels[x][y].dgreen = newP.dgreen;
			pixels[x][y].dblue = newP.dblue;
			pixels[x][y].pred = newP.pred;
			pixels[x][y].pgreen = newP.pgreen;
			pixels[x][y].pblue = newP.pblue;
			pixels[x][y].volume = newP.volume;
			pixels[x][y].pvolume = newP.pvolume;
			pixels[x][y].alpha = newP.alpha;
			pixels[x][y].drawn = true;
			//printf("load: %f\t%f\t%f\n", pixels[x][y].red, pixels[x][y].green, pixels[x][y].blue);
			newP = Pixel(-1, -1, -1);
		}
		i = s + 1;
		free(data);
	}
	redoBackground = true;
	load = true;
	glutPostRedisplay();
}

void loadFile() {
	char filename[MAX_PATH];
	OPENFILENAME ofn;
	ZeroMemory(&filename, sizeof(filename));
	ZeroMemory(&ofn, sizeof(ofn));
	ofn.lStructSize = sizeof(ofn);
	ofn.hwndOwner = NULL;  // If you have a window to center over, put its HANDLE here
	ofn.lpstrFilter = "DAB Files\0*.dab\0Any File\0*.*\0";
	ofn.lpstrFile = filename;
	ofn.nMaxFile = MAX_PATH;
	ofn.lpstrTitle = "Select a File";
	ofn.Flags = OFN_DONTADDTORECENT | OFN_FILEMUSTEXIST;
	if (GetOpenFileNameA(&ofn)) {
		glutSetCursor(GLUT_CURSOR_WAIT);
		loadData(ofn);
		if (selection) {
			glutSetCursor(GLUT_CURSOR_CROSSHAIR);
		}
		else if (colorSelection) {
			glutSetCursor(GLUT_CURSOR_RIGHT_ARROW);
		}
		else if (colorPicker) {
			glutSetCursor(GLUT_CURSOR_HELP);
		}
		else if(crossbar) {
			glutSetCursor(GLUT_CURSOR_CROSSHAIR);
		}
		else {
			glutSetCursor(GLUT_CURSOR_RIGHT_ARROW);
		}
	}
}


void saveData(OPENFILENAME ofn) {
	std::string data = "";
	std::ofstream myfile;
	for (int i = 0; i < winWidth; i++) {
		for (int j = 0; j < winHeight; j++) {
			if (pixels[i][j].drawn) {
				data.append(std::to_string(i));
				data.append("\n");
				data.append(std::to_string(j));
				data.append("\n");
				data.append(std::to_string((int)floor(pixels[i][j].red * 255)));
				data.append("\n");
				data.append(std::to_string((int)floor(pixels[i][j].green * 255)));
				data.append("\n");
				data.append(std::to_string((int)floor(pixels[i][j].blue * 255)));
				data.append("\n");
				data.append(std::to_string((int)floor(pixels[i][j].dred * 255)));
				data.append("\n");
				data.append(std::to_string((int)floor(pixels[i][j].dgreen * 255)));
				data.append("\n");
				data.append(std::to_string((int)floor(pixels[i][j].dblue * 255)));
				data.append("\n");
				data.append(std::to_string((int)floor(pixels[i][j].pred * 255)));
				data.append("\n");
				data.append(std::to_string((int)floor(pixels[i][j].pgreen * 255)));
				data.append("\n");
				data.append(std::to_string((int)floor(pixels[i][j].pblue * 255)));
				data.append("\n");
				data.append(std::to_string((int)floor(pixels[i][j].volume * 255)));
				data.append("\n");
				data.append(std::to_string((int)floor(pixels[i][j].pvolume * 255)));
				data.append("\n");
				data.append(std::to_string((int)floor(pixels[i][j].alpha * 255)));
				data.append("\n");
			}
		}
	}
	myfile.open(ofn.lpstrFile);
	myfile << data;
	myfile.close();
}

void saveFile() {
	char filename[MAX_PATH];
	OPENFILENAME ofn;
	ZeroMemory(&filename, sizeof(filename));
	ZeroMemory(&ofn, sizeof(ofn));
	ofn.lStructSize = sizeof(ofn);
	ofn.hwndOwner = NULL;  // If you have a window to center over, put its HANDLE here
	if (!bmp) {
		ofn.lpstrFilter = "DAB Files\0*.dab\0Any File\0*.*\0";
	}
	else {
		ofn.lpstrFilter = "BMP Files\0*.bmp\0Any File\0*.*\0";
	}
	ofn.lpstrFile = filename;
	ofn.nMaxFile = MAX_PATH;
	ofn.lpstrTitle = "Select a directory to save file";
	ofn.Flags = OFN_EXPLORER | OFN_FILEMUSTEXIST;
	if (GetSaveFileNameA(&ofn)) {
		glutSetCursor(GLUT_CURSOR_WAIT);
		if (!bmp) {
			saveData(ofn);
		}
		else {
			if (full) {
				canvasTextureData = (unsigned char*)malloc(winWidth * winHeight * 4);
				glReadPixels(0, 0, winWidth, winHeight, GL_RGBA, GL_UNSIGNED_BYTE, canvasTextureData);
				SOIL_save_image(filename, SOIL_SAVE_TYPE_BMP, winWidth, winHeight, 4, (unsigned char*)canvasTextureData);
			}
			else {
				canvasTextureData = (unsigned char*)malloc(barX * (winHeight - brushWidth - 6) * 4);
				glReadPixels(0, brushWidth + 6, barX, winHeight, GL_RGBA, GL_UNSIGNED_BYTE, canvasTextureData);
				SOIL_save_image(filename, SOIL_SAVE_TYPE_BMP, barX, winHeight - brushWidth - 6, 4, (unsigned char*)canvasTextureData);
			}
		}
		if (selection) {
			glutSetCursor(GLUT_CURSOR_CROSSHAIR);
		}
		else if (colorSelection) {
			glutSetCursor(GLUT_CURSOR_RIGHT_ARROW);
		}
		else if (colorPicker) {
			glutSetCursor(GLUT_CURSOR_HELP);
		}
		else if (crossbar) {
			glutSetCursor(GLUT_CURSOR_CROSSHAIR);
		}
		else {
			glutSetCursor(GLUT_CURSOR_RIGHT_ARROW);
		}
	}
}

/**
* The menu function.
*
* @param num
*			The selection from the user.
*/
void menu(int num) {
	if (num == 0) {
		glutDestroyWindow(window);
		exit(0);
	}
	else if (num == 1) {
		redoBackground = true;
		//free(canvasTextureData);
		glScissor(0, 0, barX, winHeight);
		GLuint tempTex;
		glGenTextures(1, &tempTex);
		glBindTexture(GL_TEXTURE_2D, tempTex);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, barX, winHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glBindTexture(GL_TEXTURE_2D, 0);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tempTex, 0);
		glBindFramebuffer(GL_FRAMEBUFFER, fbo);
		//glBindTexture(GL_TEXTURE_2D, tcanvasTexture);
		//canvasTextureData = (unsigned char*)calloc((winWidth / 2.0) * winHeight * 3, sizeof(unsigned char));
		//glTexSubImage2D(GL_TEXTURE_2D, 0, winWidth / 2, 0, winWidth / 2, winHeight, GL_RGB, GL_UNSIGNED_BYTE, canvasTextureData);
		glEnable(GL_SCISSOR_TEST);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glClearColor(1, 1, 1, 1);
		glDisable(GL_SCISSOR_TEST);
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		prevXMouse = -1;
		prevYMouse = -1;
		glutSetCursor(GLUT_CURSOR_WAIT);
		for (int x = 0; x < barX; x++) {
			for (int y = 0; y < winHeight; y++) {
				pixels[x][y].red = -1;
				pixels[x][y].green = -1;
				pixels[x][y].blue = -1;
				pixels[x][y].dred = -1;
				pixels[x][y].dgreen = -1;
				pixels[x][y].dblue = -1;
				pixels[x][y].pred = -1;
				pixels[x][y].pgreen = -1;
				pixels[x][y].pblue = -1;
				pixels[x][y].volume = -1;
				pixels[x][y].pvolume = -1;
				pixels[x][y].alpha = -1;
				pixels[x][y].drawn = false;
				glColor3f(backgroundC.red, backgroundC.green, backgroundC.blue);
				glBegin(GL_POINTS);
				glVertex2i(x, winHeight - y);
				glEnd();
				glBegin(GL_POINTS); //begins gl loop
				glColor3f(1, 1, 1);
				glVertex2i(x, winHeight - y);
				glEnd();
			}
		}
		glFlush();
		glDeleteTextures(1, &tempTex);
		glutPostRedisplay();
		if (selection) {
			glutSetCursor(GLUT_CURSOR_CROSSHAIR);
		}
		else if (colorSelection) {
			glutSetCursor(GLUT_CURSOR_RIGHT_ARROW);
		}
		else if (colorPicker) {
			glutSetCursor(GLUT_CURSOR_HELP);
		}
		else if (crossbar) {
			glutSetCursor(GLUT_CURSOR_CROSSHAIR);
		}
		else {
			glutSetCursor(GLUT_CURSOR_RIGHT_ARROW);
		}
	}
	else if (num == 2) {
		redoBackground = true;
		//free(canvasTextureData);
		glScissor(barX, 0, winWidth, winHeight);
		GLuint tempTex;
		glGenTextures(1, &tempTex);
		glBindTexture(GL_TEXTURE_2D, tempTex);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, barX, winHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glBindTexture(GL_TEXTURE_2D, 0);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tempTex, 0);
		glBindFramebuffer(GL_FRAMEBUFFER, fbo);
		//glBindTexture(GL_TEXTURE_2D, tcanvasTexture);
		//canvasTextureData = (unsigned char*)calloc((winWidth / 2.0) * winHeight * 3, sizeof(unsigned char));
		//glTexSubImage2D(GL_TEXTURE_2D, 0, winWidth / 2, 0, winWidth / 2, winHeight, GL_RGB, GL_UNSIGNED_BYTE, canvasTextureData);
		glEnable(GL_SCISSOR_TEST);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glClearColor(1, 1, 1, 1);
		glDisable(GL_SCISSOR_TEST);
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		prevXMouse = -1;
		prevYMouse = -1;
		glutSetCursor(GLUT_CURSOR_WAIT);
		for (int x = barX; x < winWidth; x++) {
			for (int y = 0; y < winHeight; y++) {
				pixels[x][y].red = -1;
				pixels[x][y].green = -1;
				pixels[x][y].blue = -1;
				pixels[x][y].dred = -1;
				pixels[x][y].dgreen = -1;
				pixels[x][y].dblue = -1;
				pixels[x][y].pred = -1;
				pixels[x][y].pgreen = -1;
				pixels[x][y].pblue = -1;
				pixels[x][y].volume = -1;
				pixels[x][y].pvolume = -1;
				pixels[x][y].alpha = -1;
				pixels[x][y].drawn = false;
				glBegin(GL_POINTS); //begins gl loop
				glColor3f(1, 1, 1);
				glVertex2i(x, winHeight - y);
				glEnd();
			}
		}
		glFlush();
		glDeleteTextures(1, &tempTex);
		//glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tcanvasTexture, 0);
		//glDeleteFramebuffers(1, &fbo);
		//glDeleteRenderbuffers(1, &rbo);
		//glDeleteTextures(1, &tcanvasTexture);
		//createCanvasTexture();
		first = false;
		glutPostRedisplay();
		if (selection) {
			glutSetCursor(GLUT_CURSOR_CROSSHAIR);
		}
		else if (colorSelection) {
			glutSetCursor(GLUT_CURSOR_RIGHT_ARROW);
		}
		else if (colorPicker) {
			glutSetCursor(GLUT_CURSOR_HELP);
		}
		else if (crossbar) {
			glutSetCursor(GLUT_CURSOR_CROSSHAIR);
		}
		else {
			glutSetCursor(GLUT_CURSOR_RIGHT_ARROW);
		}
		/*
		if (isCanvas) {
			if (palette == -1) {
				initPalette();
				glutSetWindow(palette);
			}
			else {
				glutSetWindow(palette);
				initPalette();
			}
		}
		isCanvas = false;
		*/
	}
	else if (num == 3) {
		glutSetCursor(GLUT_CURSOR_RIGHT_ARROW);
		if (!selection && !colorSelection && !flat && !fil && !eraser && !colorPicker && !crossbar) {
			for (int i = 0; i < newBrushWidth; i++) {
				for (int j = 0; j < newBrushHeight; j++) {
					cirBrush[i][j] = newBrush[i][j];
				}
			}
		}
		newBrush = filLeft;
		newBrushWidth = 7;
		selection = false;
		colorSelection = false;
		newBrushHeight = 5;
		centerX = 3;
		centerY = 2;
		fil = true;
		flat = false;
		boolFilLeft = true;
		boolfilLeftDown = false;
		boolfilRightDown = false;
		boolFilUp = false;
		eraser = false;
		colorPicker = false;
		crossbar = false;
		polygonFil();
	}
	else if (num == 4) {
		glutSetCursor(GLUT_CURSOR_RIGHT_ARROW);
		/*if (!selection && !colorSelection && !flat && !fil && !eraser && !colorPicker) {
			for (int i = 0; i < newBrushWidth; i++) {
				for (int j = 0; j < newBrushHeight; j++) {
					cirBrush[i][j] = newBrush[i][j];
				}
			}
		}*/
		selection = false;
		colorSelection = false;
		newBrush = cirBrush;
		newBrushWidth = 5;
		newBrushHeight = 5;
		centerX = 2;
		centerY = 2;
		fil = false;
		flat = false;
		eraser = false;
		colorPicker = false;
		crossbar = false;
		polygonRound();
	}
	else if (num == 5) {
		glutSetCursor(GLUT_CURSOR_RIGHT_ARROW);
		if (!selection && !colorSelection && !flat && !fil && !eraser && !colorPicker && !crossbar) {
			for (int i = 0; i < newBrushWidth; i++) {
				for (int j = 0; j < newBrushHeight; j++) {
					cirBrush[i][j] = newBrush[i][j];
				}
			}
		}
		selection = false;
		colorSelection = false;
		newBrush = flaLeft;
		newBrushWidth = 5;
		newBrushHeight = 9;
		centerX = 2;
		centerY = 4;
		flat = true;
		fil = false;
		boolFlaLeft = true;
		boolflaLeftUp = false;
		boolflaRightUp = false;
		colorPicker = false;
		crossbar = false;
		boolFlaUp = false;
		eraser = false;
		polygonFlat();
	}
	else if (num == 6) {
		single = true;
	}
	else if (num == 7) {
		single = false;
	}
	else if (num == 8) {
		sketch = true;
	}
	else if (num == 9) {
		sketch = false;
	}
	else if (num == 10) {
		glutSetCursor(GLUT_CURSOR_RIGHT_ARROW);
		if (!selection && !colorSelection && !flat && !fil && !eraser && !colorPicker && !crossbar) {
			for (int i = 0; i < newBrushWidth; i++) {
				for (int j = 0; j < newBrushHeight; j++) {
					cirBrush[i][j] = newBrush[i][j];
				}
			}
		}
		colorSelection = false;
		eraser = true;
		selection = false;
		centerX = 2;
		centerY = 2;
		newBrush = eraBrush;
		newBrushWidth = 5;
		newBrushHeight = 5;
		fil = false;
		flat = false;
		colorPicker = false;
		crossbar = false;
		polygonEraser();
	}
	else if (num == 11) {
		CHOOSECOLOR cc;                 // common dialog box structure 
		static COLORREF acrCustClr[16]; // array of custom colors 
		HWND hwnd = ::CreateWindowA("STATIC","dummy",WS_DISABLED,0,0,100,100,NULL,NULL,NULL,NULL);
		static DWORD rgbCurrent;        // initial color selection
		ZeroMemory(&cc, sizeof(cc));
		rgbCurrent = RGB(newBrush[centerX][centerY].sred * 255, newBrush[centerX][centerY].sgreen * 255, newBrush[centerX][centerY].sblue * 255);
		cc.lStructSize = sizeof(cc);
		cc.hwndOwner = hwnd;
		cc.lpCustColors = (LPDWORD)acrCustClr;
		cc.rgbResult = rgbCurrent;
		cc.Flags = CC_FULLOPEN | CC_RGBINIT;
		if (ChooseColor(&cc) == TRUE) {
			rgbCurrent = cc.rgbResult;
			backgroundC.red = GetRValue(rgbCurrent) / 255.0;
			backgroundC.green = GetGValue(rgbCurrent) / 255.0;
			backgroundC.blue = GetBValue(rgbCurrent) / 255.0;
			redoBackground = true;
			glutPostRedisplay();
		}
	}
	else if (num == 12) {
		if (!selection && !colorSelection && !flat && !fil && !eraser && !colorPicker && !crossbar) {
			for (int i = 0; i < newBrushWidth; i++) {
				for (int j = 0; j < newBrushHeight; j++) {
					cirBrush[i][j] = newBrush[i][j];
				}
			}
		}
		selection = false;
		glutSetCursor(GLUT_CURSOR_WAIT);
		for (int x = 0; x < winWidth; x++) {
			for (int y = 0; y < winHeight; y++) {
				if (!pixels[x][y].isSelected) {
					pixels[x][y].selected = false;
				}
			}
		}
		glutSetCursor(GLUT_CURSOR_RIGHT_ARROW);
		colorSelection = true;
		crossbar = false;
		colorPicker = false;
	}
	else if (num == 13) {
		boxes.clear();
		glutSetCursor(GLUT_CURSOR_WAIT);
		for (int x = 0; x < winWidth; x++) {
			for (int y = 0; y < winHeight; y++) {
				pixels[x][y].selected = true;
				pixels[x][y].isSelected = false;
			}
		}
		if (selection) {
			glutSetCursor(GLUT_CURSOR_CROSSHAIR);
		}
		else if (colorSelection) {
			glutSetCursor(GLUT_CURSOR_RIGHT_ARROW);
		}
		else if (colorPicker) {
			glutSetCursor(GLUT_CURSOR_HELP);
		}
		else if (crossbar) {
			glutSetCursor(GLUT_CURSOR_CROSSHAIR);
		}
		else {
			glutSetCursor(GLUT_CURSOR_RIGHT_ARROW);
		}
	}
	else if (num == 14) {
		glutSetCursor(GLUT_CURSOR_RIGHT_ARROW);
		if (!selection && !colorSelection && !flat && !fil && !eraser && !colorPicker && !crossbar) {
			for (int i = 0; i < newBrushWidth; i++) {
				for (int j = 0; j < newBrushHeight; j++) {
					cirBrush[i][j] = newBrush[i][j];
				}
			}
		}
		selection = true;
		for (int x = 0; x < winWidth; x++) {
			for (int y = 0; y < winHeight; y++) {
				if (!pixels[x][y].isSelected) {
					pixels[x][y].selected = false;
				}
			}
		}
		colorSelection = false;
		glutSetCursor(GLUT_CURSOR_CROSSHAIR);
	}
	else if (num == 15) {
		if (!selection && !colorSelection && !flat && !fil && !eraser && !colorPicker && !crossbar) {
			for (int i = 0; i < newBrushWidth; i++) {
				for (int j = 0; j < newBrushHeight; j++) {
					cirBrush[i][j] = newBrush[i][j];
				}
			}
		}
		colorPicker = true;
		crossbar = false;
		colorSelection = false;
		selection = false;
		glutSetCursor(GLUT_CURSOR_HELP);
	}
	else if (num == 16) {
		CHOOSECOLOR cc;                 // common dialog box structure 
		static COLORREF acrCustClr[16]; // array of custom colors 
		HWND hwnd = ::CreateWindowA("STATIC", "dummy", WS_DISABLED, 0, 0, 100, 100, NULL, NULL, NULL, NULL);
		static DWORD rgbCurrent;        // initial color selection
		rgbCurrent = RGB(newBrush[centerX][centerY].sred * 255, newBrush[centerX][centerY].sgreen * 255, newBrush[centerX][centerY].sblue * 255);
		ZeroMemory(&cc, sizeof(cc));
		cc.lStructSize = sizeof(cc);
		cc.hwndOwner = hwnd;
		cc.lpCustColors = (LPDWORD)acrCustClr;
		cc.rgbResult = rgbCurrent;
		cc.Flags = CC_FULLOPEN | CC_RGBINIT;
		if (ChooseColor(&cc) == TRUE) {
			rgbCurrent = cc.rgbResult;
			if (!eraser) {
				if (fil) {
					int tempw = newBrushWidth;
					int temph = newBrushHeight;
					newBrushHeight = 6;
					newBrushWidth = 6;
					for (int i = 0; i < newBrushWidth; i++) {
						for (int j = 0; j < newBrushHeight; j++) {
							filRightDown[i][j].sred = GetRValue(rgbCurrent) / 255.0;
							filRightDown[i][j].sgreen = GetGValue(rgbCurrent) / 255.0;
							filRightDown[i][j].sblue = GetBValue(rgbCurrent) / 255.0;
							filRightDown[i][j].rred = GetRValue(rgbCurrent) / 255.0;
							filRightDown[i][j].rgreen = GetGValue(rgbCurrent) / 255.0;
							filRightDown[i][j].rblue = GetBValue(rgbCurrent) / 255.0;
							filRightDown[i][j].volume = 0;
							filRightDown[i][j].rvolume = 50;
						}
					}
					newBrushHeight = 6;
					newBrushWidth = 6;
					for (int i = 0; i < newBrushWidth; i++) {
						for (int j = 0; j < newBrushHeight; j++) {
							filLeftDown[i][j].sred = GetRValue(rgbCurrent) / 255.0;
							filLeftDown[i][j].sgreen = GetGValue(rgbCurrent) / 255.0;
							filLeftDown[i][j].sblue = GetBValue(rgbCurrent) / 255.0;
							filLeftDown[i][j].rred = GetRValue(rgbCurrent) / 255.0;
							filLeftDown[i][j].rgreen = GetGValue(rgbCurrent) / 255.0;
							filLeftDown[i][j].rblue = GetBValue(rgbCurrent) / 255.0;
							filLeftDown[i][j].volume = 0;
							filLeftDown[i][j].rvolume = 50;
						}
					}
					newBrushHeight = 5;
					newBrushWidth = 7;
					for (int i = 0; i < newBrushWidth; i++) {
						for (int j = 0; j < newBrushHeight; j++) {
							filLeft[i][j].sred = GetRValue(rgbCurrent) / 255.0;
							filLeft[i][j].sgreen = GetGValue(rgbCurrent) / 255.0;
							filLeft[i][j].sblue = GetBValue(rgbCurrent) / 255.0;
							filLeft[i][j].rred = GetRValue(rgbCurrent) / 255.0;
							filLeft[i][j].rgreen = GetGValue(rgbCurrent) / 255.0;
							filLeft[i][j].rblue = GetBValue(rgbCurrent) / 255.0;
							filLeft[i][j].volume = 0;
							filLeft[i][j].rvolume = 50;
						}
					}
					newBrushHeight = 7;
					newBrushWidth = 5;
					for (int i = 0; i < newBrushWidth; i++) {
						for (int j = 0; j < newBrushHeight; j++) {
							filUp[i][j].sred = GetRValue(rgbCurrent) / 255.0;
							filUp[i][j].sgreen = GetGValue(rgbCurrent) / 255.0;
							filUp[i][j].sblue = GetBValue(rgbCurrent) / 255.0;
							filUp[i][j].rred = GetRValue(rgbCurrent) / 255.0;
							filUp[i][j].rgreen = GetGValue(rgbCurrent) / 255.0;
							filUp[i][j].rblue = GetBValue(rgbCurrent) / 255.0;
							filUp[i][j].volume = 0;
							filUp[i][j].rvolume = 50;
						}
					}
					newBrushWidth = tempw;
					newBrushHeight = temph;
					for (int i = 0; i < newBrushWidth; i++) {
						for (int j = 0; j < newBrushHeight; j++) {
							newBrush[i][j].sred = GetRValue(rgbCurrent) / 255.0;
							newBrush[i][j].sgreen = GetGValue(rgbCurrent) / 255.0;
							newBrush[i][j].sblue = GetBValue(rgbCurrent) / 255.0;
							newBrush[i][j].rred = GetRValue(rgbCurrent) / 255.0;
							newBrush[i][j].rgreen = GetGValue(rgbCurrent) / 255.0;
							newBrush[i][j].rblue = GetBValue(rgbCurrent) / 255.0;
							newBrush[i][j].volume = 0;
							newBrush[i][j].rvolume = 50;
						}
					}
				}
				else if (flat) {
					int tempw = newBrushWidth;
					int temph = newBrushHeight;
					newBrushHeight = 7;
					newBrushWidth = 7;
					for (int i = 0; i < newBrushWidth; i++) {
						for (int j = 0; j < newBrushHeight; j++) {
							flaRightUp[i][j].sred = GetRValue(rgbCurrent) / 255.0;
							flaRightUp[i][j].sgreen = GetGValue(rgbCurrent) / 255.0;
							flaRightUp[i][j].sblue = GetBValue(rgbCurrent) / 255.0;
							flaRightUp[i][j].rred = GetRValue(rgbCurrent) / 255.0;
							flaRightUp[i][j].rgreen = GetGValue(rgbCurrent) / 255.0;
							flaRightUp[i][j].rblue = GetBValue(rgbCurrent) / 255.0;
							flaRightUp[i][j].volume = 0;
							flaRightUp[i][j].rvolume = 50;
						}
					}
					newBrushHeight = 7;
					newBrushWidth = 7;
					for (int i = 0; i < newBrushWidth; i++) {
						for (int j = 0; j < newBrushHeight; j++) {
							flaLeftUp[i][j].sred = GetRValue(rgbCurrent) / 255.0;
							flaLeftUp[i][j].sgreen = GetGValue(rgbCurrent) / 255.0;
							flaLeftUp[i][j].sblue = GetBValue(rgbCurrent) / 255.0;
							flaLeftUp[i][j].rred = GetRValue(rgbCurrent) / 255.0;
							flaLeftUp[i][j].rgreen = GetGValue(rgbCurrent) / 255.0;
							flaLeftUp[i][j].rblue = GetBValue(rgbCurrent) / 255.0;
							flaLeftUp[i][j].volume = 0;
							flaLeftUp[i][j].rvolume = 50;
						}
					}
					newBrushHeight = 9;
					newBrushWidth = 5;
					for (int i = 0; i < newBrushWidth; i++) {
						for (int j = 0; j < newBrushHeight; j++) {
							flaLeft[i][j].sred = GetRValue(rgbCurrent) / 255.0;
							flaLeft[i][j].sgreen = GetGValue(rgbCurrent) / 255.0;
							flaLeft[i][j].sblue = GetBValue(rgbCurrent) / 255.0;
							flaLeft[i][j].rred = GetRValue(rgbCurrent) / 255.0;
							flaLeft[i][j].rgreen = GetGValue(rgbCurrent) / 255.0;
							flaLeft[i][j].rblue = GetBValue(rgbCurrent) / 255.0;
							flaLeft[i][j].volume = 0;
							flaLeft[i][j].rvolume = 50;
						}
					}
					newBrushHeight = 5;
					newBrushWidth = 9;
					for (int i = 0; i < newBrushWidth; i++) {
						for (int j = 0; j < newBrushHeight; j++) {
							flaUp[i][j].sred = GetRValue(rgbCurrent) / 255.0;
							flaUp[i][j].sgreen = GetGValue(rgbCurrent) / 255.0;
							flaUp[i][j].sblue = GetBValue(rgbCurrent) / 255.0;
							flaUp[i][j].rred = GetRValue(rgbCurrent) / 255.0;
							flaUp[i][j].rgreen = GetGValue(rgbCurrent) / 255.0;
							flaUp[i][j].rblue = GetBValue(rgbCurrent) / 255.0;
							flaUp[i][j].volume = 0;
							flaUp[i][j].rvolume = 50;
						}
					}
					newBrushWidth = tempw;
					newBrushHeight = temph;
					for (int i = 0; i < newBrushWidth; i++) {
						for (int j = 0; j < newBrushHeight; j++) {
							newBrush[i][j].sred = GetRValue(rgbCurrent) / 255.0;
							newBrush[i][j].sgreen = GetGValue(rgbCurrent) / 255.0;
							newBrush[i][j].sblue = GetBValue(rgbCurrent) / 255.0;
							newBrush[i][j].rred = GetRValue(rgbCurrent) / 255.0;
							newBrush[i][j].rgreen = GetGValue(rgbCurrent) / 255.0;
							newBrush[i][j].rblue = GetBValue(rgbCurrent) / 255.0;
							newBrush[i][j].volume = 0;
							newBrush[i][j].rvolume = 50;
						}
					}
				}
				else {
					//printf("here\n");
					for (int i = 0; i < newBrushWidth; i++) {
						for (int j = 0; j < newBrushHeight; j++) {
							cirBrush[i][j].sred = GetRValue(rgbCurrent) / 255.0;
							cirBrush[i][j].sgreen = GetGValue(rgbCurrent) / 255.0;
							cirBrush[i][j].sblue = GetBValue(rgbCurrent) / 255.0;
							cirBrush[i][j].rred = GetRValue(rgbCurrent) / 255.0;
							cirBrush[i][j].rgreen = GetGValue(rgbCurrent) / 255.0;
							cirBrush[i][j].rblue = GetBValue(rgbCurrent) / 255.0;
							cirBrush[i][j].volume = 0;
							cirBrush[i][j].rvolume = 50;
							std::cout << std::to_string(cirBrush[i][j].sred) + "\n";
						}
					}
					for (int i = 0; i < newBrushWidth; i++) {
						for (int j = 0; j < newBrushHeight; j++) {
							newBrush[i][j].sred = GetRValue(rgbCurrent) / 255.0;
							newBrush[i][j].sgreen = GetGValue(rgbCurrent) / 255.0;
							newBrush[i][j].sblue = GetBValue(rgbCurrent) / 255.0;
							newBrush[i][j].rred = GetRValue(rgbCurrent) / 255.0;
							newBrush[i][j].rgreen = GetGValue(rgbCurrent) / 255.0;
							newBrush[i][j].rblue = GetBValue(rgbCurrent) /255.0;
							newBrush[i][j].volume = 0;
							newBrush[i][j].rvolume = 50;
						}
					}
				}
			}
			glutPostRedisplay();
		}
	}
	else if (num == 17) {
		undoSelect = true;
		glutPostRedisplay();
	}
	else if (num == 18) {
		replacement = true;
		loadFile();
	}
	else if (num == 19) {
		bmp = false;
		full = false;
		saveFile();
	}
	else if (num == 20) {
		replacement = false;
		loadFile();
	}
	else if (num == 21) {
		bmp = true;
		full = true;
		saveFile();
	}
	else if (num == 22) {
		bmp = true;
		full = false;
		saveFile();
	}
	else if (num == 23) {
		glutSetCursor(GLUT_CURSOR_CROSSHAIR);
		colorPicker = false;
		crossbar = true;
		colorSelection = false;
		selection = false;
	}
}

/**
* Initializes the menu.
*/
void initMenu() {
	brushMenu_id = glutCreateMenu(menu);
	glutAddMenuEntry("Filbert i", 3);
	glutAddMenuEntry("Round r", 4);
	glutAddMenuEntry("Flat f", 5);
	glutAddMenuEntry("Eraser e", 10);
	modeMenu_id = glutCreateMenu(menu);
	glutAddMenuEntry("Single-bristle b", 6);
	glutAddMenuEntry("Multi-bristle n", 7);
	glutAddMenuEntry("Disable color blending g", 8);
	glutAddMenuEntry("Enable color blending h", 9);
	toolMenu_id = glutCreateMenu(menu);
	glutAddMenuEntry("Color select m", 12);
	glutAddMenuEntry("Rectangle select y", 14);
	glutAddMenuEntry("Deselect j", 13);
	glutAddMenuEntry("Color picker k", 15);
	glutAddMenuEntry("Color chooser l", 16);
	glutAddMenuEntry("Change background t", 11);
	glutAddMenuEntry("Adjust crossbar o", 23);
	fileMenu_id = glutCreateMenu(menu);
	glutAddMenuEntry("Save .dab w", 19);
	glutAddMenuEntry("Save full to .bmp z", 21);
	glutAddMenuEntry("Save canvas to .bmp x", 22);
	glutAddMenuEntry("Load .dab with replacement u", 18);
	glutAddMenuEntry("Load .dab without replacement a", 20);
	clearMenu_id = glutCreateMenu(menu);
	glutAddMenuEntry("Clear canvas c", 1);
	glutAddMenuEntry("Clear palette p", 2);
	glutAddMenuEntry("Clear selected regions d", 17);
	mainMenu_id = glutCreateMenu(menu);
	glutAddSubMenu("Brush", brushMenu_id);
	glutAddSubMenu("Mode", modeMenu_id);
	glutAddSubMenu("Tool", toolMenu_id);
	glutAddSubMenu("File", fileMenu_id);
	glutAddSubMenu("Clear", clearMenu_id);
	glutAddMenuEntry("Quit q", 0);
	glutAttachMenu(GLUT_RIGHT_BUTTON);
}

/**
* The timer function for drying the canvas.
*/
void drying(int n) {
	float delta = 0.5;
	dryLock.lock();
	std::vector<int> dry;
	for (int i = dryingList.size() - 1; i > -1; i--) {
		int x = dryingList[i].x;
		int y = dryingList[i].y;
		float r = pixels[x][winHeight - y].red;
		float g = pixels[x][winHeight - y].green;
		float b = pixels[x][winHeight - y].blue;
		double Ot = opticaldepth(r, g, b);
		double alpha = std::min(pixels[x][winHeight - y].volume * Ot * -1, 1.0);
		float alphaPrime = alpha - delta;
		pixels[x][winHeight - y].dred = (alpha * pixels[x][winHeight - y].red + (1 - alpha) * pixels[x][winHeight - y].dred - alphaPrime * pixels[x][winHeight - y].red) / (1 - alphaPrime);
		pixels[x][winHeight - y].dgreen = (alpha * pixels[x][winHeight - y].green + (1 - alpha) * pixels[x][winHeight - y].dgreen - alphaPrime * pixels[x][winHeight - y].green) / (1 - alphaPrime);
		pixels[x][winHeight - y].dblue = (alpha * pixels[x][winHeight - y].blue + (1 - alpha) * pixels[x][winHeight - y].dblue - alphaPrime * pixels[x][winHeight - y].blue) / (1 - alphaPrime);
		pixels[x][winHeight - y].volume -= delta;
		if (pixels[x][winHeight - y].volume <= 0) {
			dry.push_back(i);
		}
	}
	for (int i = 0; i < dry.size(); i++) {
		int x = dryingList[dry[i]].x;
		int y = dryingList[dry[i]].y;
		pixels[x][winHeight - y].volume = 0;
		dryingList.erase(dryingList.begin() + dry[i]);
	}
	dryLock.unlock();
	glutTimerFunc(3, drying, 0);
}

void keyboardFunc(unsigned char key, int x, int y) {
	switch (key) {
		case 'q': menu(0); break;
		case 'c': menu(1); break;
		case 'p': menu(2); break;
		case 'i': menu(3); break;
		case 'r': menu(4); break;
		case 'f': menu(5); break;
		case 'b': menu(6); break;
		case 'n': menu(7); break;
		case 'g': menu(8); break;
		case 'e': menu(10); break;
		case 'h': menu(9); break;
		case 't': menu(11); break;
		case 'l': menu(15); break;
		case 'k': menu(16); break;
		case 'm': menu(12); break;
		case 'j': menu(13); break;
		case 'y': menu(14); break;
		case 'd': menu(17); break;
		case 'u': menu(18); break;
		case 'w': menu(19); break;
		case 'a': menu(20); break;
		case 'z': menu(21); break;
		case 'x': menu(22); break;
		case 'o': menu(23); break;
	}
}

int main(int argc, char **argv) {
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
	glutInitWindowPosition(0, 0); //beginning window position
	glutInitWindowSize(winWidth, winHeight); //window size
	window = glutCreateWindow("DAB");
	//glutFullScreen();
	//winWidth = glutGet(GLUT_SCREEN_WIDTH);
	//winHeight = glutGet(GLUT_SCREEN_HEIGHT);
	//glutSetCursor(GLUT_CURSOR_CROSSHAIR);
	glutSetCursor(GLUT_CURSOR_RIGHT_ARROW);
	GLenum err = glewInit();
	init();
	initArray();
	initMenu();
	polygon.push_back(Point(currXMouse, winHeight - currYMouse));
	polygon.push_back(Point(currXMouse + 2, winHeight - currYMouse - 2));
	polygon.push_back(Point(currXMouse + 32, winHeight - currYMouse + 38));
	polygon.push_back(Point(currXMouse + 30, winHeight - currYMouse + 40));
	polygon2.push_back(Point(currXMouse, winHeight - currYMouse));
	polygon2.push_back(Point(currXMouse, winHeight - currYMouse + 2));
	polygon2.push_back(Point(currXMouse + 30, winHeight - currYMouse + 40));
	polygon3.push_back(Point(currXMouse, winHeight - currYMouse));
	polygon3.push_back(Point(currXMouse + 32, winHeight - currYMouse + 38));
	polygon3.push_back(Point(currXMouse, winHeight - currYMouse - 2));
	createCanvasTexture();
	createBrushTexture();
	shader = LoadShader("shader.vert", "shader.frag");
	glutDisplayFunc(displayCanvas);
	glutMotionFunc(mouseMotion);
	glutMouseFunc(mouseInput);
	glutReshapeFunc(reshape);
	glutTimerFunc(5, drying, 0);
	glutKeyboardFunc(keyboardFunc);
	glutPassiveMotionFunc(passiveMouseMotion);
	glutMainLoop();
}